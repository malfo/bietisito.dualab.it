<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 * Date: 30/01/14
 * Time: 16.36
 * To change this template use File | Settings | File Templates.
 */

function import_immagine($arr_d,$arr_f,$pri=0){
    global $fold_upload;
    //var_dump($arr_f);
    if($arr_f["upfile"]["name"]!=""){
        //prendo l'estensione

        $ext=strtolower(substr($arr_f["upfile"]["name"],strrpos($arr_f["upfile"]["name"],'.')+1));

        $permessi=array("jpg","png","jpeg");

        if(in_array($ext,$permessi)){
            if(isset($arr_d['id_store_articoli'])){
                $nomefile="pri".str_pad($arr_d['id_store_articoli'],7,"0",STR_PAD_LEFT).".".$ext;
                $query = "UPDATE store_articoli SET immagine='$nomefile' WHERE id_store_articoli='".$arr_d['id_store_articoli']."'";
            }
            if(isset($arr_d['id_store_attributi_opzioni_articoli'])){
                $nomefile=str_pad($arr_d['id_store_attributi_opzioni_articoli'],7,"0",STR_PAD_LEFT).".".$ext;
                $query = "UPDATE store_attributi_opzioni_articoli SET immagine='$nomefile' WHERE id_store_attributi_opzioni_articoli='".$arr_d['id_store_attributi_opzioni_articoli']."'";
            }

            if(@is_uploaded_file($arr_f["upfile"]["tmp_name"])){
                //nomino il file con l'id

                $file_completo=$fold_upload."/articoli/".$nomefile;
                if(@move_uploaded_file($arr_f["upfile"]["tmp_name"], $file_completo)){

                    $sql=$query;
                    aggiorna($sql);
                    $avviso="Immagine caricata";
                }
                else{
                    $avviso="Impossibile spostare il file, controlla l'esistenza o i permessi della directory dove fare l'upload.";
                }
            }

        }
        else{
            $avviso="Formato di file non permesso,";
        }

    }

    $arr_ret=array("id"=>$arr_d['id_store_attributi_opzioni_articoli'],"avviso"=>$avviso);
    return $arr_ret;
}

function check_file_import($arr_f){
    if($arr_f["upfile"]["name"]!=""){
        //prendo l'estensione

        $nome_completo = $arr_f["upfile"]["name"];
        $explode_nome = explode(".",$nome_completo);
        $ext = $explode_nome[1];
        $permessi=array("txt");
        //echo $nome_completo."<br>";
        if(in_array($ext,$permessi)){


            $file = @fopen($arr_f["upfile"]["tmp_name"],"r") or exit("Unable to open file!");

            $row = fgets($file);
            //Elabora il file
            $nr_documento = rtrim(substr($row,0,9));
            $arr_documento['nr_documento|equal|A1']=$nr_documento;
            $arr_documento['attivo|equal|A1']=1;
            $documento = lista_record($arr_documento,array(20));

            $nr_record = mysql_num_rows($documento);
            if($nr_record==0){
                $avviso = "OK";
            }else{
                $avviso = "File già importato";
            }

            fclose($file);

        }else{
            $avviso="Formato di file non permesso.";
        }

    }else{
        $avviso="Seleziona un File";
    }

    return $avviso;
}
function import_fatture_ktm($arr_f,$import_articolo=0,$id_tipologia_documento=0){

    //var_dump($arr_f);
    if($arr_f["upfile"]["name"]!=""){
        //prendo l'estensione

        $nome_completo = $arr_f["upfile"]["name"];
        $explode_nome = explode(".",$nome_completo);
        $ext = $explode_nome[1];
        $permessi=array("txt");
        //echo $nome_completo."<br>";
        /*
         * Gestione Upload File
         */
        /*if(@is_uploaded_file($arr_f["upfile"]["tmp_name"])){
            //nomino il file con l'id

            $file_completo=$fold_upload."/fatture_ktm/".$nome_completo;
            if(@move_uploaded_file($arr_f["upfile"]["tmp_name"], $file_completo)){

            //Se upload ok leggo il file
            $id=1;
            $avviso="import eseguito con successo";
            }
            else{
                $id=0;
                $avviso="Impossibile spostare il file, controlla l'esistenza o i permessi della directory dove fare l'upload.";
            }
        }*/

        $file = @fopen($arr_f["upfile"]["tmp_name"],"r") or exit("Unable to open file!");
        $i=0;
        while(! feof($file))
        {

            $i=$i+1;

            if ($i==1){
                $row = fgets($file);
                //Elabora il file
                $nr_documento = rtrim(substr($row,0,9));
                $data_documento = substr($row,10,8);

                $date = DateTime::createFromFormat('dmY', $data_documento);
                $data_documento_format = $date->format('d-m-Y');

                /*
                 * Inserimento Documento
                 */
                //Anagrafica
                $arr_ric["flg_principale|equal|A1"]=1;
                $arr_ric["id_tipologia|equal|A1"]=5;
                $anagrafica=lista_record($arr_ric,array(10));
                $ris_anagrafica = mysql_fetch_assoc($anagrafica);

                //costruzione array inserimento
                $codice_numerazione = torna_info_da_id(12,21,"codice_numerazione");
                $insert['attivo']=1;
                $insert['stato']=1;
                $insert['id_tipologia_documento']=$id_tipologia_documento;
                $insert['codice_numerazione']=$codice_numerazione;
                $insert['nr_documento']=$nr_documento;
                $insert['data_documento']=$data_documento_format;
                $insert['id_anagrafica']=$ris_anagrafica['id_anagrafica'];
                $insert['ragione_sociale1']=$ris_anagrafica['ragione_sociale1'];
                $insert['ragione_sociale2']=$ris_anagrafica['ragione_sociale2'];
                $insert['indirizzo']=$ris_anagrafica['indirizzo'];
                $insert['citta']=$ris_anagrafica['citta'];
                $insert['provincia']=$ris_anagrafica['provincia'];
                $insert['cap']=$ris_anagrafica['cap'];

                $id_documento = inserisci_record($insert,20);
                $id=$id_documento;
                //echo $nr_documento."-".$data_documento."<br>";

            }
            if($i>1){
                $row = fgets($file);
                if(strlen($row)>0){
                    //Elabora il file
                    $nr_ordine = rtrim(substr($row,0,22));
                    $cod_articolo = rtrim(substr($row,23,20));
                    $nome_articolo = rtrim(substr($row,63,30));
                    $quantita = substr($row,93,7);
                    $prezzo_vendita_unitario = substr($row,114,10);
                    $sconto_totale = substr($row,124,10);
                    $prezzo_totale_scontato = substr($row,134,10);
                    $codice_doganale = rtrim(substr($row,144,10));
                    $nazione = substr($row,154,2);
                    $codice_motore=substr($row,156,11);
                    $numero_telaio=substr($row,177,20);
                    $codice_spedizione = rtrim(substr($row,200,8));

                    $prezzo_vendita_totale = $prezzo_vendita_unitario * $quantita;
                    $prezzo_scontato_unitario = $prezzo_totale_scontato / $quantita;
                    $sconto = (($prezzo_vendita_unitario-$prezzo_scontato_unitario)*100) / $prezzo_vendita_unitario;

                    $pattern = '/\s{2,}/';
                    $replacement = ' ';
                    $nome_articolo = preg_replace($pattern, $replacement, $nome_articolo);
                    //$nome_articolo = $arr_nome_articolo[0];
                    /*
                     * Inserimento Articoli Documento
                     */
                    $insert_art['ktm_nr_ordine']=$nr_ordine;
                    $insert_art['ktm_codice_articolo']=$cod_articolo;
                    $insert_art['ktm_nome_articolo']=$nome_articolo;
                    $insert_art['ktm_quantita']=$quantita;
                    $insert_art['ktm_prezzo_vendita']=$prezzo_vendita_unitario;
                    $insert_art['ktm_importo_sconto']=$sconto_totale;
                    $insert_art['ktm_prezzo_totale']=$prezzo_totale_scontato;
                    $insert_art['ktm_nazione']=$nazione;
                    $insert_art['ktm_codice_motore']=$codice_motore;
                    $insert_art['ktm_numero_telaio']=$numero_telaio;
                    $insert_art['ktm_codice_doganale']=$codice_doganale;
                    $insert_art['ktm_codice_spedizione']=$codice_spedizione;

                    $insert_art['id_documento']=$id_documento;
                    $insert_art['barcode_casaprod']=$cod_articolo;
                    $insert_art['id_store_casaprod']=1;
                    $insert_art['nome_articolo']=$nome_articolo;
                    $insert_art['numero_seriale']=$numero_telaio;
                    $insert_art['quantita']=$quantita;
                    $insert_art['prezzo_imponibile']=$prezzo_vendita_unitario;
                    $insert_art['sconto']=round($sconto,0);

                    inserisci_record($insert_art,22);
                    //Import Articolo Moto Da Fattura e Insert Movimento di Carico
                    /*if ($import_articolo==1){
                        $insert_store_articoli['id_store_sottocat']=400;
                        $insert_store_articoli['id_store_cat']=168;
                        $insert_store_articoli['id_store_casaprod']=1;
                        $insert_store_articoli['nome_articolo']=$nome_articolo;
                        $insert_store_articoli['codice_articolo']=$cod_articolo;
                        $insert_store_articoli['barcode_casaprod']=$cod_articolo;
                        $insert_store_articoli['numero_seriale']=$numero_telaio;
                        $insert_store_articoli['prezzo_vendita']=$prezzo_vendita_unitario;
                        $insert_store_articoli['valore_tax']=22;
                        $insert_store_articoli['flg_dati_moto']=1;
                        $insert_store_articoli['attivo']=1;
                        $id_articolo = inserisci_record($insert_store_articoli,4);

                        //Inserimento Movimento di Carico
                        $insert_movimento['id_store_articoli']=$id_articolo;
                        $insert_movimento['id_documento']=$id_documento;
                        $insert_movimento['tipo_operazione']="c";
                        $insert_movimento['quantita']=$quantita;
                        $insert_movimento['data_operazione']=data_phptomy(set_date_roma());
                        $insert_movimento['datatime_operazione']=set_date_time_roma();
                        inserisci_record($insert_movimento,6);

                }*/

                    //echo $codice_motore."-".$numero_telaio."<br>";
                }

            }
            if ($_FILES['upfile']['error'] === UPLOAD_ERR_OK){
                $id=$id_documento;
                $avviso="upload avvenuto con successo";
            }else{
                $id=0;
                $avviso="problemi nell'upload";
            }

        }

        fclose($file);


    }

    $arr_ret=array("id"=>$id,"avviso"=>$avviso);
    return $arr_ret;

}

function check_file_import_csv($arr_f){
    if($arr_f["upfile"]["name"]!=""){
        //prendo l'estensione

        $nome_completo = $arr_f["upfile"]["name"];
        $explode_nome = explode(".",$nome_completo);
        $ext = $explode_nome[1];
        $permessi=array("csv","txt");
        //echo $nome_completo."<br>";
        if(in_array($ext,$permessi)){

            $file = @fopen($arr_f["upfile"]["tmp_name"],"r") or exit("Unable to open file!");

            $size = $arr_f["upfile"]["size"];
            //Elabora il file


            if($size>0){
                $avviso = "OK";
            }else{
                $avviso = "Il file selezionato è Vuoto!";
            }

            fclose($file);

        }else{
            $avviso="Formato di file non permesso.";
        }

    }else{
        $avviso="Seleziona un File";
    }

    return $avviso;
}

function import_ordini_csv($arr_f,$id_carrello,$tipocsv){
    //$avviso = "";
    //var_dump($arr_f);
    if($arr_f["upfile"]["name"]!=""){

        $file = @fopen($arr_f["upfile"]["tmp_name"],"r") or exit("Unable to open file!");
        $i=0;
        while(! feof($file))
        {

            $i=$i+1;

            $row = fgets($file);
            //Import da Fatture KTM
            if ($tipocsv == 1){

                if($i>1){
                    $barcode = rtrim(substr($row,23,20));
                    $quantita = substr($row,93,7);


                    if($barcode){
                        //echo strlen($barcode)."<br>";
                        //echo $barcode. " - ". $quantita ."<br>";

                        $arr_ricerca['attivo|equal|A1']=1;
                        $arr_ricerca['barcode_casaprod|equal|A1']=$barcode;
                        $articolo = lista_record($arr_ricerca,array(4));
                        $ris_articolo = mysql_fetch_assoc($articolo);

                        //echo $ris_articolo['id_store_articoli']." - ".$ris_articolo['prezzo_vendita']."<br>";

                        $ret=inserisci_articolo_carrello($id_carrello,$ris_articolo['id_store_articoli'],0,$quantita,0);

                        //echo $ret['messaggio']."<br>";
                    }

                }

            }

            //Import da Ordini KTM
            if ($tipocsv == 2){
                //Elabora il file
                $arr_row = explode(";",$row);
                //echo str_replace('"','',$arr_row[1])."<br>";
                $barcode = str_replace('"','',$arr_row[1]);

                if($barcode){
                    $arr_ricerca['attivo|equal|A1']=1;
                    $arr_ricerca['barcode_casaprod|equal|A1']=$barcode;
                    $articolo = lista_record($arr_ricerca,array(4));
                    $ris_articolo = mysql_fetch_assoc($articolo);

                    //echo $ris_articolo['barcode_casaprod']." - ".$ris_articolo['prezzo_vendita']."<br>";

                    $ret=inserisci_articolo_carrello($id_carrello,$ris_articolo['id_store_articoli'],0,$arr_row[2],0);

                    //echo $ret['messaggio']."<br>";

                }
            }

            //Import da Ordini Interni
            if ($tipocsv == 3){
                //Elabora il file
                $arr_row = explode(";",$row);
                //echo str_replace('"','',$arr_row[1])."<br>";
                $barcode = str_replace('"','',$arr_row[1]);

                if($barcode){
                    $arr_ricerca['attivo|equal|A1']=1;
                    $arr_ricerca['barcode_casaprod|equal|A1']=$barcode;
                    $articolo = lista_record($arr_ricerca,array(4));
                    $ris_articolo = mysql_fetch_assoc($articolo);

                    //echo $ris_articolo['barcode_casaprod']." - ".$ris_articolo['prezzo_vendita']."<br>";

                    $ret=inserisci_articolo_carrello($id_carrello,$ris_articolo['id_store_articoli'],0,$arr_row[2],0);

                    //echo $ret['messaggio']."<br>";

                }
            }



            if ($_FILES['upfile']['error'] === UPLOAD_ERR_OK){
                $avviso="upload avvenuto con successo";
            }else{
                $avviso="problemi nell'upload";
            }
        }

        fclose($file);

        $arr_ret=array("id"=>$id_carrello,"avviso"=>$avviso);
        return $arr_ret;

    }

}
?>