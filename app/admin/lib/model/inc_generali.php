<?php

function inserisci_record($arr,$tipo){

    // l'array passato deve essere gia filtrato dai campi non necessari
    global $tipi;

    //Apro la connessione al db per la funzione mysql_real_escape_string()
    $db_connection = connection_db();
    //prendo l'array delle tabelle dal $tipo che passo nella funzione
    $sezione=$tipi[$tipo];

    //ciclo i campi da inserire
    foreach($arr as $key=>$value){
        $campi.=" $key,";
        //$key_split = split("_",$key);
        $key_split = explode("_",$key);
        //$key_split[0]."<br>";
        if ($key_split[0]=="data"){
            $valori.=" '".data_phptomy($value)."',";
        }else{
            $valori.=" '".mysql_real_escape_string($value)."',";
        }

    }
    // tolgo la virgola finale
    $campi=substr($campi,0,-1);
    $valori=substr($valori,0,-1);

    //inserisco
    $query="INSERT INTO ".$sezione[0]." ($campi) VALUES ($valori) ";
    //echo $query;
    $id=inserisci_conn_esternal($query,$db_connection);
    //echo $id;

    return $id;


}

function modifica_voce($arr){
    global $tipi;


    //explode dei dati['id']
    $dati= explode("|",$arr['id']);

    // $tipo - array tabella (da explode)
    $tipo=$dati[2];
    //campo da modificare  (da explode)
    $campotab=$dati[0];
    //identificativo del campo da modificare
    $id=$dati[1];
    //valore del campo da modificare
    $value=$arr['value'];

    //prendo l'array dei tipi (0=tabella,1=identificatore) vedi in cima alla pagina
    $sezione=$tipi[$tipo];

    //aggiorno il campo
    $query = "UPDATE ".$sezione[0]." SET $campotab = '".addslashes($value)."' WHERE ".$sezione[1]."='$id'";
    //echo $query;
    aggiorna($query);

    //riprendo il record per visualizzarlo
    $querysel = "SELECT $campotab FROM ".$sezione[0]." WHERE ".$sezione[1]."='$id'";
    $dati=Seleziona($querysel);
    $datiret = mysql_fetch_assoc($dati);
    return $datiret[$campotab];
}

function modifica_voce_return_affect($arr){
    global $tipi;


    //explode dei dati['id']
    $dati= explode("|",$arr['id']);

    // $tipo - array tabella (da explode)
    $tipo=$dati[2];
    //campo da modificare  (da explode)
    $campotab=$dati[0];
    //identificativo del campo da modificare
    $id=$dati[1];
    //valore del campo da modificare
    $value=$arr['value'];

    //prendo l'array dei tipi (0=tabella,1=identificatore) vedi in cima alla pagina
    $sezione=$tipi[$tipo];

    //aggiorno il campo
    $query = "UPDATE ".$sezione[0]." SET $campotab = '".addslashes($value)."' WHERE ".$sezione[1]."='$id'";
    //echo $query;
    $rec_affect = aggiorna($query);


    return $rec_affect;
}

function modifica_voce_drop($arr){
    global $tipi;


    //explode dei dati['id']
    $dati= explode("|",$arr['id']);
    // $tipo - array tabella (da explode)
    $tipo=$dati[2];
    //campo da modificare  (da explode)
    $campotab=$dati[0];
    //identificativo del campo da modificare
    $id=$dati[1];
    //valore del campo da modificare
    $value=$arr['value'];
    //
    $restituisci=$arr['restituisci'];
    $sezione=$tipi[$tipo];

    //aggiorno il campo
    $query = "UPDATE ".$sezione[0]." SET $campotab = '".addslashes($value)."' WHERE ".$sezione[1]."='$id'";
    aggiorna($query);

    //prendo l'identificativo (es: id_store_categorie)
    $ident = $tipi[$tipo][1];

    //preparo l'array di ricerca
    $arr_ricerca["$ident|equal|"]=$id;

    //prendo il valore con la funzione e l'array impostato a identificativo=$id)

    $record=lista_record($arr_ricerca,array($tipo-1,$tipo));
    $datiret = mysql_fetch_assoc($record);

    return $datiret[$restituisci];
}

function modifica_record($arr,$tipo,$identificativo){
    global $tipi;

    $sezione=$tipi[$tipo];

    //ciclo i campi da inserire
    foreach($arr as $key=>$value){
        //$campi.=" $key,";
        $key_split = explode("_",$key);
        //$key_split[0]."<br>";
        if ($key_split[0]=="data"){
            $valore=data_phptomy($value);
        }else{
            $valore=addslashes($value);
        }
        $up.=" $key='$valore',";
    }
    // tolgo la virgola finale
    //$campi=substr($campi,0,-1);
    //$valori=substr($valori,0,-1);
    $up=substr($up,0,-1);

    //inserisco
    $query="UPDATE ".$sezione[0]." SET $up WHERE ".$sezione[1]."='$identificativo'";
    //echo $query;
    $record_affect = aggiorna($query);

    return $record_affect;

}

function elimina_record($arr,$tipo){
    global $tipi;
    $id=$arr['id'];
    $sezione=$tipi[$tipo];


    $query = "UPDATE ".$sezione[0]." SET attivo = '0' WHERE ".$sezione[1]."='$id'";
    //echo $query;
    $ret=aggiorna($query);

    return $ret;

}

function elimina_record_def_id($arr,$tipo){
    global $tipi;
    $id=$arr['id'];
    $sezione=$tipi[$tipo];


    $query = "DELETE FROM ".$sezione[0]." WHERE ".$sezione[1]."='$id'";
    //echo $query;
    $numrighedel=rimuovi($query);

    return $numrighedel;

}

function elimina_record_def($arr,$tipo){
    global $tipi;
    $sezione=$tipi[$tipo];

    if(count($arr)>0){
        //var_dump($arr);
        foreach($arr as $key=>$value){
            $condizione="$key='$value'";
            if($value!=""){
                $and.=" AND $condizione";
            }
        }
    }


    $query = "DELETE FROM ".$sezione[0]." WHERE 1 $and";
    $numrighedel = rimuovi($query);
    return $numrighedel;

}

function prendi_record($arr,$tipo){
    global $tipi;

    $sezione=$tipi[$tipo];
    if(count($arr)>0){
        foreach($arr as $key=>$value){
            $and.=" AND $key='$value'";
        }
    }

    $query = "SELECT * FROM ".$sezione[0]." WHERE 1 $and   ORDER BY ordine  ";
    $dati=Seleziona($query);
    return $dati;

}

function lista_record($arr,$arr_tipo,$debug=0,$order="",$limit="",$group="",$add_select="",$tiposelect="",$tipojoin=""){
    global $tipi;
    /*
     * il primo array contiene le condizioni di ricerca
     * il secondo array contiene le tabelle coinvolte
     */


    //Ciclo le tabelle coinvolte nella select
    if(count($arr_tipo)>0){
        foreach($arr_tipo as $value){
            $num=$num+1;
            //se è la tabella principale (la prima)
            if($num==1){
                $nr_tabella=$value;
                $tab_princ=$tipi[$value][0]." as A$num";
                $campo_princ=$tipi[$value][1];

            }
            //altre tabelle (unite con una JOIN)
            else{
                $alias=$num-1;
                $tab=$tipi[$value][0];
                $tab_join.=$tipojoin." JOIN $tab as A$num  ON A$alias.".$campo_princ."=A$num.".$campo_princ." ";
                $campo_princ=$tipi[$value][1];
            }

        }
    }


    //elaboro  l'array di ricerca

    //echo "Conta arr: ".count($arr);
    if(count($arr)>0){
        //var_dump($arr);
        foreach($arr as $key=>$value){

            $stripcampo=explode("|",$key);
            
            $cond=$stripcampo[1];
            $tabella=$stripcampo[2];
            if($tabella!=""){
                $campo=$tabella.".".$stripcampo[0];
            }
            else{
                $campo=$stripcampo[0];
            }

            $key_split = explode("_",$stripcampo[0]);
            //$key_split[0]."<br>";
            if ($key_split[0]=="data" && $value!=""){

                $value=data_phptomy($value);

            }else{
                $value=addslashes($value);
            }
            //$value=addslashes($value);
            switch ($cond){
                case "equal":
                    $condizione="$campo='$value'";
                    break;
                case "notequal":
                    $condizione="$campo!='$value'";
                    break;
                case "like_left":
                    $condizione="$campo LIKE '%$value'";
                    break;
                case "like_right":
                    $condizione="$campo LIKE '$value%'";
                    break;
                case "like_both":
                    $condizione="$campo LIKE '%$value%'";
                    break;
                case "plus":
                    $condizione="$campo > '$value'";
                    break;
                case "min":
                    $condizione="$campo < '$value'";
                    break;
                case "plus_equal":
                    $condizione="$campo >= '$value'";
                    break;
                case "min_equal":
                    $condizione="$campo <= '$value'";
                    break;
                case "in":
                    $condizione="$campo in $value";
                    break;
                case "notin":
                    $condizione="$campo not in $value";
                    break;
            }
            if($value!=""){
                $and.=" AND $condizione";
            }
        }
    }
    if($order!=""){
        $order=" ORDER BY $order";
    }
    if($limit!=""){
        $limit=" LIMIT $limit";
    }
    if($group!=""){
        $group=" GROUP BY $group";
    }
    if($add_select!=""){
        $add_select=$add_select;
    }else{
        $add_select="*";
    }
    switch ($nr_tabella){
        case 1000:
            $query="SELECT `M`.`nome_macrocat` AS `nome_macrocat`,`C`.`nome_cat` AS `nome_cat`,`S`.`nome_sottocat` AS `nome_sottocat`,`A`.`id_store_articoli` AS `id_store_articoli`,`A`.`id_store_sottocat` AS `id_store_sottocat`,`A`.`id_store_cat` AS `id_store_cat`,`A`.`id_store_casaprod` AS `id_store_casaprod`,`A`.`codice_articolo` AS `codice_articolo`,`A`.`nome_articolo` AS `nome_articolo`,`A`.`nome_articolo_en` AS `nome_articolo_en`,`A`.`descrizione_articolo` AS `descrizione_articolo`,`A`.`descrizione_articolo_en` AS `descrizione_articolo_en`,`A`.`quantita_articolo` AS `quantita_articolo`,`A`.`modello_articolo` AS `modello_articolo`,`A`.`prezzo_vendita` AS `prezzo_vendita`,`A`.`sconto_fornitore` AS `sconto_fornitore`,`A`.`prezzo_fornitore` AS `prezzo_fornitore`,`A`.`ricarico_vendita` AS `ricarico_vendita`,`A`.`sconto_cliente` AS `sconto_cliente`,`A`.`valore_tax` AS `valore_tax`,`A`.`immagine_articolo` AS `immagine_articolo`,`A`.`url_articolo` AS `url_articolo`,`A`.`url_articolo_en` AS `url_articolo_en`,`A`.`barcode_casaprod` AS `barcode_casaprod`,`A`.`barcode_store` AS `barcode_store`,`A`.`id_store_articoli_stati` AS `id_store_articoli_stati`,`A`.`data_disponibile_dal` AS `data_disponibile_dal`,`A`.`data_disponibile_al` AS `data_disponibile_al`,`A`.`ordine_articolo` AS `ordine_articolo`,`A`.`flg_articolo_padre` AS `flg_articolo_padre`,`A`.`id_articolo_padre` AS `id_articolo_padre`,`A`.`nome_articolo_padre` AS `nome_articolo_padre`,`A`.`opzione_articolo_figlio` AS `opzione_articolo_figlio`,`A`.`data_creazione` AS `data_creazione`,`A`.`visibile` AS `visibile`,`A`.`attivo` AS `attivo`,`AST`.`descrizione_stato` AS `descrizione_stato`,`CP`.`descrizione_casaprod` AS `descrizione_casaprod`,`CP`.`immagine_casaprod` AS `immagine_casaprod`,`M`.`id_store_macrocat` AS `id_store_macrocat`,`A`.`unita_misura` AS `unita_misura`,`A`.`numero_seriale` AS `numero_seriale`,`A`.`flg_view_website` AS `flg_view_website`,`A`.`flg_view_ecommerce` AS `flg_view_ecommerce`
					FROM (((((`store_articoli` `A` left join `store_cat` `C` on((`C`.`id_store_cat` = `A`.`id_store_cat`))) left join `store_macrocat` `M` on((`M`.`id_store_macrocat` = `C`.`id_store_macrocat`))) left join `store_sottocat` `S` on((`S`.`id_store_sottocat` = `A`.`id_store_sottocat`))) left join `store_articoli_stati` `AST` on((`AST`.`id_store_articoli_stati` = `A`.`id_store_articoli_stati`))) left join `store_casaprod` `CP` on((`CP`.`id_store_casaprod` = `A`.`id_store_casaprod`))) 
					WHERE 1 $and ";
            break;
        default:
            $query = "SELECT $tiposelect $add_select FROM $tab_princ $tab_join WHERE 1 $and $group $order $limit";
            break;
    }

    if ($debug==1){echo $query."<br>";}
    //$myConnectDB = new connect_db($query);
    //$dati = $myConnectDB->seleziona($query);
    //echo $query;
    $dati=Seleziona($query);
    return $dati;
}

function rimpiazza($str_ric,$campo){
    if($str_ric!=""){
        $rimp="<span style=\"background-color:#F2FE63\">".$str_ric."</span>";
        $nuova=eregi_replace($str_ric,$rimp,$campo);
        return $nuova;
    }
    else{
        return $campo;
    }
}

function lista_province(){
    $query = "SELECT * FROM  generali_province ORDER BY siglaprovincia";
    $dati = Seleziona($query);

    return $dati;
}

function torna_info_da_id($id,$tipo,$camporet,$attivo=0){
    global $tipi;
    $sezione=$tipi[$tipo];
    $and = "";
    if($attivo==1){
        $and=" AND attivo=1";
    }
    /*if (empty($id)){
        $id = "''";
    }*/
    $query = "SELECT ".$camporet." FROM ".$sezione[0]." WHERE ".$sezione[1]."= $id $and";
    //echo $query;
    $dati = Seleziona($query);
    $datiret = mysql_fetch_assoc($dati);
    $split_camporet = explode(",",$camporet);
    //Se $camporet contiene pi� campi elaboro per tornare il dato corretto
    if ($split_camporet[1]!=""){
        $ris_ret = $datiret[$split_camporet[0]]." ".$datiret[$split_camporet[1]];
        return $ris_ret;
    }else{
        return $datiret[$camporet];
    }
}

function cerca_barcode_auto($q){

    $query = "SELECT * FROM  store_articoli WHERE attivo=1 AND barcode_casaprod LIKE '%".$q."%' ";
    //echo $query;
    $dati=Seleziona($query);
    if ($dati != FALSE){
        while($legami=mysql_fetch_assoc($dati)){

            $cmp=$legami['barcode_casaprod'];
            $cmp1=$legami['nome_articolo'];

            $str.=$cmp."|".$cmp1."\n";
            //$str.=$cmp4."|".$cmp5;

        }

    }
    return $str;

}

function set_flag_univoco($arr){
    //Setto il flag a 1 in tabella e resetto gli altri a 0

    global $tipi;

    //explode dei dati['id']
    $dati= explode("|",$arr['id']);

    // $tipo - array tabella (da explode)
    $tipo=$dati[2];
    //campo da modificare  (da explode)

    $campotab=$dati[0];
    //identificativo del campo da modificare
    $id=$dati[1];
    //valore del campo da modificare
    $value=$arr['value'];

    //prendo l'array dei tipi (0=tabella,1=identificatore) vedi in cima alla pagina
    $sezione=$tipi[$tipo];

    //aggiorno il campo
    $queryset = "UPDATE ".$sezione[0]." SET $campotab = '".addslashes($value)."' WHERE ".$sezione[1]."='$id'";
    echo $queryset;
    aggiorna($queryset);

    $queryreset = "UPDATE ".$sezione[0]." SET $campotab = '0' WHERE ".$sezione[1]."!='$id'";
    echo $queryreset;
    aggiorna($queryreset);

    //riprendo il record per visualizzarlo
    $querysel = "SELECT $campotab FROM ".$sezione[0]." WHERE ".$sezione[1]."='$id'";
    $dati=Seleziona($querysel);
    $datiret = mysql_fetch_assoc($dati);

    return $datiret[$campotab];
}

function set_flag_univoco_generali_immagini($arr){
    //Setto il flag a 1 in tabella e resetto gli altri a 0

    global $tipi;

    //explode dei dati['id']
    $dati= explode("|",$arr['id']);

    // $tipo - array tabella (da explode)
    $tipo=$dati[2];
    //campo da modificare  (da explode)

    $campotab=$dati[0];
    //identificativo del campo da modificare
    $id=$dati[1];
    //valore del campo da modificare
    $value=$arr['value'];


    //prendo l'array dei tipi (0=tabella,1=identificatore) vedi in cima alla pagina
    $sezione=$tipi[$tipo];

    //aggiorno il campo
    $queryset = "UPDATE ".$sezione[0]." SET $campotab = '".addslashes($value)."' WHERE ".$sezione[1]."='$id'";
    //echo $queryset;
    aggiorna($queryset);

    $query_select_sezione = "SELECT * FROM ".$sezione[0]." WHERE ".$sezione[1]."='$id'";
    $obj = seleziona($query_select_sezione);
    $ris = mysql_fetch_assoc($obj);

    $queryreset = "UPDATE ".$sezione[0]." SET $campotab = '0' WHERE ".$sezione[1]."!='$id' AND id='".$ris['id']."' AND sezione='".$ris['sezione']."'";
    //echo $queryreset;
    aggiorna($queryreset);

    //riprendo il record per visualizzarlo
    $querysel = "SELECT $campotab FROM ".$sezione[0]." WHERE ".$sezione[1]."='$id'";
    $dati=Seleziona($querysel);
    $datiret = mysql_fetch_assoc($dati);

    return $datiret[$campotab];
}

function set_flag_pubblico($arr){
    //Setto il flag a 1 in tabella e resetto gli altri a 0

    global $tipi;

    //explode dei dati['id']
    $dati= explode("|",$arr['id']);

    // $tipo - array tabella (da explode)
    $tipo=$dati[2];
    //campo da modificare  (da explode)

    $campotab=$dati[0];
    //identificativo del campo da modificare
    $id=$dati[1];
    //valore del campo da modificare
    $value=$arr['value'];

    //prendo l'array dei tipi (0=tabella,1=identificatore) vedi in cima alla pagina
    $sezione=$tipi[$tipo];

    //aggiorno il campo
    $queryset = "UPDATE ".$sezione[0]." SET $campotab = '".addslashes($value)."' WHERE ".$sezione[1]."='$id'";
    //echo $query;
    aggiorna($queryset);

    //riprendo il record per visualizzarlo
    $querysel = "SELECT $campotab FROM ".$sezione[0]." WHERE ".$sezione[1]."='$id'";
    $dati=Seleziona($querysel);
    $datiret = mysql_fetch_assoc($dati);

    return $datiret[$campotab];
}

function check_esistenza_campo($tipo,$camporet,$valorecamporet,$attivo=0){
    global $tipi;
    $sezione=$tipi[$tipo];
    $and = "";
    if($attivo==1){
        $and=" AND attivo=1";
    }
    $query = "SELECT ".$camporet." FROM ".$sezione[0]." WHERE ".$camporet."= $valorecamporet $and";
    //echo $query;
    $dati = Seleziona($query);
    $nrdatiret = mysql_num_rows($dati);

    return $nrdatiret;

}

function cerca_anagrafiche_auto($q,$tipologie=4){
    $q = addslashes($q);
    if($tipologie!=0){
        $and="AND id_tipologia='$tipologie'";
    }

    $query = "SELECT * FROM  anagrafiche WHERE attivo=1 AND (ragione_sociale1 LIKE '%".$q."%' OR CONCAT_WS(' ',ragione_sociale1,ragione_sociale2) LIKE '%".$q."%') $and";
    //echo $query;
    $dati=Seleziona($query);
    if ($dati != FALSE){
        while($legami=mysql_fetch_assoc($dati)){

            $cmp=$legami['id_anagrafica'];
            $cmp1=$legami['ragione_sociale1'];
            $cmp2=$legami['ragione_sociale2'];
            $cmp3=data_phptomy($legami['data_nascita']);
            $cmp4=$legami['e_mail'];
            $cmp5=$legami['cod_fiscale'];
            $str.=$cmp."|".$cmp1." ".$cmp2."|".$cmp3."|".$cmp4."|".$cmp5."\n";
            //$str.=$cmp4."|".$cmp5;

        }

    }
    return $str;

}

function avanzamento_nr_progressivo($tabella,$campo_max,$campo,$valore){

    $query="SELECT MAX($campo_max) AS ultimo_nr from $tabella
		WHERE $campo='$valore'";
    //echo ($query);
    $dati=Seleziona($query);
    $res=mysql_fetch_assoc($dati);

    //echo ();
    If ($res['ultimo_nr']==NULL){
        $num_ = 1;

    }else{
        $num_ = $res['ultimo_nr'] + 1;

    }

    return $num_;

}

function conta_record($nomecampo,$valorecampo,$sezione){
    $arr_ricerca['attivo|equal|A1']=1;
    $arr_ricerca[$nomecampo.'|equal|A1']=$valorecampo;
    $risultato=lista_record($arr_ricerca,array($sezione));
    $conta_risultato=mysql_num_rows($risultato);
    return $conta_risultato;

}

function set_date_time_roma(){
    $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
    return $dateTime->format("Y-m-d H:i:s");
}
function set_date_time_roma_it_format(){
    $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
    return $dateTime->format("d-m-Y H:i:s");
}
function set_date_time_roma_it_format_concat(){
    $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
    return $dateTime->format("d-m-Y_H:i:s");
}
function set_date_roma(){
    $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
    return $dateTime->format("Y-m-d");
}
function set_date_roma_it_format(){
    $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
    return $dateTime->format("d-m-Y");
}
//$arr=array("attivo"=>1,"descrizione"=>"bla");

//prendi_record($arr,1);


/*
*formattazione con due decimali e punto migliaia (formato 1.000,56)
*/
function decimal($somma){
    $formattato=number_format($somma, 2, ',', '.');
    return $formattato;
}

/*
*controlla il testo richiesto in un inserimento
*/
function testo_req($valore,$campo){
    $avviso="";
    if($valore==""){
        $avviso="<br>Il campo $campo &egrave; vuoto";
    }
    return $avviso;
}

function testo_req_zero($valore,$campo){
    $avviso="";
    if($valore=="" || $valore==0){
        $avviso="<br>Il campo $campo &egrave; vuoto";
    }
    return $avviso;
}

/*
Iverte la data da aaaa-mm-gg a gg-mm-aaaa e viceversa
*/
function data_phptomy($data){
    $data=preg_replace('\/','-',$data);
    $data=preg_replace("\.","-",$data);
    $arr=explode("-","$data");
    $nuova=$arr[2]."-".$arr[1]."-".$arr[0];
    return $nuova;
}

/*
#
# FUNZIONI VARIE (formati data valuta etc)
#
*/

/*
Funzione per filtrare i dati dai campi del form
*/
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
    $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

    switch ($theType) {
        case "text":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "long":
        case "int":
            $theValue = ($theValue != "") ? intval($theValue) : "NULL";
            break;
        case "double":
            $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
            break;
        case "date":
            $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
            break;
        case "defined":
            $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
            break;
    }
    return $theValue;
}

/*
 * redirect su pagina
 */
function redirectsu($pagina){
    header(sprintf("Location: %s", $pagina));

}


/*
 * Trasforma un array POST in array per inserire
 */

function trasforma_post($arr){

    $array_soliti=array("do","action","submit","Submit");

    foreach($arr as $key=>$value){
        if(substr($key,0,3)!="no_" && !in_array($key,$array_soliti) ){
            $return[$key]=$value;
        }
    }

    return $return;
}

function trasforma_post_ric($arr){

    //$array_soliti=array("do","action","submit","Submit");

    foreach($arr as $key=>$value){
        if(substr($key,0,4)=="ric_" ){
            $return[$key]=$value;
        }
    }

    return $return;
}
function logout(){
    session_start();
    // Desetta tutte le variabili di sessione.
    $_SESSION = array();
    // Infine distrugge la sessione.
    session_destroy();
    //session_destroy();
    //$pagina="index.php";
    //header(sprintf("Location: %s",$pagina));
}

function upload_immagine($arr_d,$arr_f,$pri=0){
    global $fold_images;

    //var_dump($arr_f);
    if($arr_f["upfile"]["name"]!=""){
        //prendo l'estensione

        $ext=strtolower(substr($arr_f["upfile"]["name"],strrpos($arr_f["upfile"]["name"],'.')+1));

        $permessi=array("jpg","png","jpeg");
        $nome_file = "";
        if(in_array($ext,$permessi)){

            if(@is_uploaded_file($arr_f["upfile"]["tmp_name"])){
                //nomino il file con l'id

                $insert['sezione']=$arr_d['sezione'];
                $insert['id']=$arr_d['id'];
                $insert['id_tipologia_file']=1;
                $id = inserisci_record($insert,62);

                $nome_file = $arr_d['sezione']."_".$arr_d['id']."_".$id.".".$ext;

                $file_completo=$fold_images."/".$nome_file;
                if(@move_uploaded_file($arr_f["upfile"]["tmp_name"], $file_completo)){

                    $edit['nome_file'] = $nome_file;
                    modifica_record($edit,62,$id);
                    $avviso="Immagine caricata";
                }
                else{
                    $avviso="Impossibile spostare il file, controlla l'esistenza o i permessi della directory dove fare l'upload.";
                }
            }

        }
        else{
            $avviso="Formato di file non permesso,";
        }

    }

    $arr_ret=array("id"=>$arr_d['id'],"avviso"=>$avviso);
    return $arr_ret;
}

/**
 * @param $valore
 * @param $campo
 * @param $tipocheck (1 = obbligatorio, 2 = Testo, 3 = Numerico, 4 = email)
 * @return string
 */
function check_form($valore,$campo,$tipocheck){
    $avviso="";
    switch ($tipocheck){
        case 1:
            if($valore==""){
                $avviso=ucfirst($campo)." Obbligatorio";
                $arr_avviso = array(
                    "error" => $campo."|".$avviso
                );
            }
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            //Controllo il Formato della Mail
            $mailvalidator= filter_var($valore, FILTER_VALIDATE_EMAIL);
            if(!$mailvalidator){
                $avviso="Formato ".ucfirst($campo)." non valido";
                $arr_avviso = array(
                    "error" => $campo."|".$avviso
                );
            }
            break;

    }

    return $arr_avviso;
}
?>