<?php
session_start();
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 16/04/15
 * Time: 09:26
 */
if( !$_SESSION['s_accesso'] && !$_SESSION['s_id_login'] ){

    $pagina="http://bietisito.dualab.it/app/admin2/index.php";
    header(sprintf("Location: %s",$pagina));
    exit;
}else{
    $pagina="http://bietisito.dualab.it/app/admin2/index.php?act=dashboard";
    header(sprintf("Location: %s",$pagina));
    exit;
}