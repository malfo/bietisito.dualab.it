<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 16/04/15
 * Time: 10:45
 */

/*
 * Crea un password random
 */
function createRandomPassword() {
    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime()*1000000);
    $i = 0;    $pass = '' ;
    while ($i <= 7) {
        $num = rand() % 33;
        $tmp = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }
    return $pass;
}


/*
 *
 * FUNZIONI PER IL LOGIN LOGOUT UTENTI
 *
 */

/*
 * Prende un utente
 */
function prendi_utente($campo1,$valore1,$campo2,$valore2){

    if($campo2!=""){$and=" AND $campo2='$valore2' ";}

    $query = "SELECT U.*,G.gruppo FROM  admin_utenti as U,admin_gruppi as G WHERE U.id_gruppo=G.id_gruppo AND $campo1='$valore1' $and AND U.attivo='1' AND U.password!='' ";
    //echo ($query);
    $ret=seleziona($query);
    // var_dump($ret);
    if($ret>0){
        $ris=mysql_fetch_assoc($ret);
        //var_dump($ris);
    }
    return $ris;
}


function esegui_login($username,$password){
    //vedo se esiste l'utente
    $ute=prendi_utente("U.username",$username,"U.password",$password);
    //var_dump($ute);
    //echo count($ute)."-".$ute['attivo'];

    if($ute['attivo']==1){

        $_SESSION['s_id_utente']=$ute['id_utente'];
        $_SESSION['s_nome_utente']=$ute['username'];
        $_SESSION['s_id_gruppo']=$ute['id_gruppo'];
        $_SESSION['s_id_anagrafica']=$ute['id_anagrafica'];
        $_SESSION['s_id_contatto']=$ute['id_contatto'];
        $_SESSION['s_id_agenzia']=$ute['id_agenzia'];
        $_SESSION['s_accesso']=1;


        //echo "Accesso: ".$_SESSION['s_accesso']."<br>";


        //inserisco in admin_login
        $insert ="INSERT INTO admin_login (id_utente,data_login,sessione) VALUES ('".$ute['id_utente']."','".date('Y-m-d H:i:s')."','".session_id()."')";
        //$insert ="INSERT INTO admin_login (id_utente,data_login,sessione) VALUES ('".$ute['id_utente']."',NOW(),'".session_id()."')";
        $id_login=inserisci($insert);

        $_SESSION['s_id_login']=$id_login;
        //echo "Login: ".$_SESSION['s_id_login']."<br>";
        //$sqlana="SELECT ragione_sociale1,ragione_sociale2 FROM anagrafiche WHERE id_anagrafica=".$ute['id_contatto'];
        $sqlana="SELECT ragione_sociale1,ragione_sociale2 FROM anagrafiche WHERE id_anagrafica=".$ute['id_anagrafica'];
        //echo($sqlana);
        $dta=seleziona($sqlana);
        $dt=mysql_fetch_assoc($dta);
        $_SESSION['s_nominativo']=$dt['ragione_sociale1']." ".$dt['ragione_sociale2'];
    }else{
        //session_destroy();
        //$avviso=1;
        $avviso_login="Username o password errati.";

        //$pagina="/index.php?avviso=1";
    }

    return $avviso_login;


}


function prendi_utente_idanagrafica($idanagrafica){


    $query = "SELECT U.*,G.gruppo FROM  admin_utenti as U,admin_gruppi as G WHERE U.id_gruppo=G.id_gruppo AND U.id_anagrafica='$idanagrafica' AND U.attivo='1' AND U.password!='' ";
    //echo ($query);
    $ret=seleziona($query);
    // var_dump($ret);
    if($ret>0){
        $ris=mysql_fetch_assoc($ret);
        //var_dump($ris);
    }
    return $ris;
}

function esegui_login_idanagrafica($id_anagrafica){
    //vedo se esiste l'utente
    $ute=prendi_utente_idanagrafica($id_anagrafica);
    //var_dump($ute);
    //echo count($ute)."-".$ute['attivo'];

    if($ute['attivo']==1){
        $_SESSION['s_id_utente']=$ute['id_utente'];
        $_SESSION['s_nome_utente']=$ute['username'];
        $_SESSION['s_id_gruppo']=$ute['id_gruppo'];
        $_SESSION['s_id_anagrafica']=$ute['id_anagrafica'];
        $_SESSION['s_id_contatto']=$ute['id_contatto'];
        $_SESSION['s_accesso']=1;




        //inserisco in admin_login
        $insert ="INSERT INTO admin_login (id_utente,data_login,sessione) VALUES ('".$ute['id_utente']."','".date('Y-m-d H:i:s')."','".session_id()."')";
        $id_login=inserisci($insert);

        $_SESSION['s_id_login']=$id_login;

        //$sqlana="SELECT ragione_sociale1,ragione_sociale2 FROM anagrafiche WHERE id_anagrafica=".$ute['id_contatto'];
        $sqlana="SELECT ragione_sociale1,ragione_sociale2 FROM anagrafiche WHERE id_anagrafica=".$ute['id_anagrafica'];
        //echo($sqlana);
        $dta=seleziona($sqlana);
        $dt=mysql_fetch_assoc($dta);
        $_SESSION['s_nominativo']=$dt['ragione_sociale1']." ".$dt['ragione_sociale2'];
        //echo $_SESSION['s_nominativo'];
    }else{
        $avviso=1;
        $avviso_login="Username o password errati.";
        //$pagina="/index.php?avviso=1";
    }

    return $avviso_login;


}

/*
 * Controlla lo stato di un login
 */

function stato_login(){
    $log="a";
    if($_SESSION['s_accesso']==1 && $_SESSION['s_id_login']>0){$log=1;}
    else {$log=0;}
    return $log;
};

/*
 * Controlla la validita della pass
 */
function controlla_pass(){
    $scad=0;
    $ute_sc=prendi_utente("U.id_utente",$_SESSION['s_id_utente'],"","");
    if($ute_sc['data_scadenza_pass']<=date('Y-m-d')){
        $scad=1;
    }
    return $scad;
}
