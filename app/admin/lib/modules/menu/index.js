$(document).ready(function(){

    //Inizializzazione
    var action = $("#action").val();
    var idtabella = $("#id_tabella").val();

    $("#forminserimento input").live("keypress", function(e) {
        if (e.keyCode == 13){

            return false;
        }
    });

    $(".cke_testo").ckeditor({
        toolbar: 'Basic',
        enterMode : CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });

    /*
     * Elimina Record
     */
    $(".del_voce").click(function(){
        id_record = $(this).attr('id').substr(3);
        $("#msg_alert").html("Vuoi Eliminare il Record?");
        $("#msg_alert").dialog({
            resizable: false,
            height:200,
            modal: true,
            title: "Attenzione",
            buttons: {
                "Si": function() {

                    $.ajax({
                        type: "POST",
                        url: "lib/controllers/gtw_"+action+".php",
                        data: "do=del&idtabella="+idtabella+"&id="+id_record,
                        success: function(msg){
                            //alert(msg);
                            if(msg>0){

                                window.location="index.php?act="+action+"&do=ric&ric[menu|equal|A1]=2";

                            }else{
                                $("#msg_alert").html("Problemi nell'eliminazione dell'id: "+id_record+". Contatta l'Amministratore!");
                                $("#msg_alert").dialog({
                                    resizable: false,
                                    height:200,
                                    width:400,
                                    modal: true,
                                    title: "Attenzione",
                                    buttons: {
                                        "Ok": function(){
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                            }

                        }
                    });

                    $( this ).dialog( "close" );

                },
                Cancel: function() {

                    $( this ).dialog( "close" );

                }
            }
        });
        return false;

    });


    /*
     * Inserisce o Modifica Articolo
     */
    function gestione_articolo(dati){
        var str1 = dati;
        //alert(str1);
        $.ajax({
            type: "POST",
            url: "lib/controllers/gtw_"+action+".php",
            data: str1,
            success: function(msg){
                var msg_split=msg.split("|");
                if (msg_split[0]=="OK"){
                    //alert('articoli.php?invia=1'+msg_split[1]);
                    window.location=msg_split[1];
                }else{
                    $("#msg_alert").html(msg_split[0]);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        });
    }
    $('#forminserimento').live('submit', function(event){

        var str = $("#forminserimento").serialize();
        var dati = str+"&no_idtabella="+idtabella;
        //alert (dati);
        gestione_articolo(dati);

        return false;
    });

    //Gestione Voce Menu
    $(".voce_padre").hide();
    if($("#menu").val()==1){
        $(".voce_padre").show();
    }
    $("#menu").change(function(){
        if($("#menu").val()==1){
            $(".voce_padre").show();
        }else{
            $("#id_menu_padre").val(0);
            $(".voce_padre").hide();
        }

    });

    //Gestione Associazione Gruppi Menu
    $("#add_gruppo").live('click', function (event){
        id_record = $("#id_art").val();
        id_gruppo = $("#elenco_gruppi").val();

        $.ajax({
            type: "POST",
            url: "lib/controllers/gtw_"+action+".php",
            data: "do=addgruppo&idmenu="+id_record+"&idgruppo="+id_gruppo,
            success: function(msg){
                if(msg=="OK"){
                    location.reload();
                }else{
                    $("#msg_alert").html(msg);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }



            }
        });


    });
    $(".eli_gruppo").live('click', function (event){
        id_record = $("#id_art").val();
        id_gruppo = $(this).attr('id').substr(3);

        $.ajax({
            type: "POST",
            url: "lib/controllers/gtw_"+action+".php",
            data: "do=delgruppo&idmenu="+id_record+"&idgruppo="+id_gruppo,
            success: function(msg){

                if(msg=="OK"){
                    location.reload();
                }else{
                    $("#msg_alert").html("Eliminazione Gruppo non riuscito!");
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }


            }
        });

    });
});
