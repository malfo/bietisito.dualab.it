$(document).ready(function(){

    //Inizializzazione
    var action = $("#action").val();
    var idtabella = $("#id_tabella").val();
    $("#forminserimento input").live("keypress", function(e) {
        if (e.keyCode == 13){

            return false;
        }
    });

    $(".cke_testo").ckeditor({
        toolbar: 'Basic',
        enterMode : CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });


    /*
     * Elimina Record
     */
    $(".del_voce").click(function(){
        id_record = $(this).attr('id').substr(3);
        $("#msg_alert").html("Vuoi Eliminare il Record?");
        $("#msg_alert").dialog({
            resizable: false,
            height:200,
            modal: true,
            title: "Attenzione",
            buttons: {
                "Si": function() {

                    $.ajax({
                        type: "POST",
                        url: "lib/controllers/gtw_"+action+".php",
                        data: "do=del&idtabella="+idtabella+"&id="+id_record,
                        success: function(msg){
                            //alert(msg);
                            if(msg>0){

                                window.location="index.php?act="+action+"&do=ric";

                            }else{
                                $("#msg_alert").html("Problemi nell'eliminazione dell'id: "+id_record+". Contatta l'Amministratore!");
                                $("#msg_alert").dialog({
                                    resizable: false,
                                    height:200,
                                    width:400,
                                    modal: true,
                                    title: "Attenzione",
                                    buttons: {
                                        "Ok": function(){
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                            }

                        }
                    });

                    $( this ).dialog( "close" );

                },
                Cancel: function() {

                    $( this ).dialog( "close" );

                }
            }
        });
        return false;

    });


    /*
     * Inserisce o Modifica Articolo
     */
    function gestione_articolo(dati){
        var str1 = dati;
        //alert(str1);
        $.ajax({
            type: "POST",
            url: "lib/controllers/gtw_"+action+".php",
            data: str1,
            success: function(msg){
                var msg_split=msg.split("|");
                if (msg_split[0]=="OK"){
                    //alert('articoli.php?invia=1'+msg_split[1]);
                    window.location=msg_split[1];
                }else{
                    $("#msg_alert").html(msg_split[0]);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        });
    }
    $('#forminserimento').live('submit', function(event){

        var str = $("#forminserimento").serialize();
        var dati = str+"&no_idtabella="+idtabella;
        //alert (dati);
        gestione_articolo(dati);

        return false;
    });

    function clean_field(){
        $(".clean_field").val("");
        $(".clean_field").html("");
    }


    /*
     * MEssaggio dialog
     */

    if($("#messaggio").html()!=""){
        $("#messaggio").dialog({
            resizable: false,
            height:200,
            modal: true,
            title: "Attenzione",
            buttons: {
                "Ok": function(){
                    $(this).dialog("close");
                }
            }
        });

    }
});
