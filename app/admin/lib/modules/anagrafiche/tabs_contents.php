<?php 
require_once('../acs_control.php');
include('../function/inc_generali.php');
include('../function/inc_anagrafiche.php');
$tab_attivo = $_REQUEST['tab_attivo'];
//var_dump($_REQUEST['test']);

//$lista=mysql_fetch_assoc($record);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $htmltitle;?> - Anagrafiche</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<?php echo $path_css;?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo $path_css_fold;?>jquery-ui-latest.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $path_css_fold;?>blocchi.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $path_css_fold;?>prettyPhoto.css" type="text/css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <script type="text/JavaScript" src="../js/jquery-latest.min.js"></script>
    <script type="text/JavaScript" src="../js/jquery-ui-personalized.min.js"></script>
    <script type="text/JavaScript" src="../js/jquery.jeditable.js"></script>
    <script type="text/JavaScript" src="../js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="../extra/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../extra/ckeditor/adapters/jquery.js"></script>
    <script type="text/JavaScript" src="tab.js"></script>
    <script type="text/JavaScript" src="index.js"></script>
</head>

<body >
<div id="contenitore">
<?php include('../template/basic/menu/menu.php'); ?>
<div id="corpopag">
	<div id="centrale">
		<!-- <div id="ricana">
		
		</div> -->

        <div id="corpo-tab">
		
			<?php include('tabs_menu.php');
            $arr_ricerca["attivo|equal|A1"]=1;
            //$arr_ricerca["id_anagrafica|equal|A1"]=$_REQUEST['id_anagrafica'];
			switch($tab_attivo){
					case "1":
                        /*
                         * Tab Modifica Anagrafica
                         */
                        $arr_ricerca_mod["id_anagrafica|equal|A1"]=$_REQUEST['id_anagrafica'];
                        $record_mod=lista_record($arr_ricerca_mod,array(10));
                        $ris_anagrafica=mysql_fetch_assoc($record_mod);

                        $arr_ricerca=array("attivo|equal|"=>1);
                        $tipologie=lista_record($arr_ricerca,array(11));

                        $province=lista_record($arr_ricerca,array(13),0,"siglaprovincia ASC");

						include('tab_dettaglio.php');
						break;
					case "2":
                        /*
                         * Tab Modifica Valori - Sconti, Iva
                         */
                        $arr_ricerca_mod["id_anagrafica|equal|A1"]=$_REQUEST['id_anagrafica'];
                        $record_mod=lista_record($arr_ricerca_mod,array(10));
                        $ris_anagrafica=mysql_fetch_assoc($record_mod);

                        $tax=lista_record($arr_ricerca,array(24));
                        include('tab_valori.php');
                        break;
                    case "3":
                        $sedi=lista_record($arr_ricerca,array(301));
                        include('tab_sedi.php');
                        break;
                    case "4":
                        $contatti=lista_record($arr_ricerca,array(302));
                        include('tab_contatti.php');
                        break;
                    case "5":
                        $arr_moto['id_anagrafica|equal|A1']=$_REQUEST['id_anagrafica'];
                        $moto=lista_record($arr_moto,array(303));
                        include('tab_moto.php');
                        break;
					
			}?>
		
		
		</div>
	
	</div>
</div>
</div>
<div id="messaggio" style="display:none"><?php echo $avviso;?></div>
<div id="msg_alert" style="display:none"></div>
</body>
</html>
<!-- <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><span id="testo-dialog"></span></p>
	<input type="hidden" id="idrecord" name="idrecord" /> -->