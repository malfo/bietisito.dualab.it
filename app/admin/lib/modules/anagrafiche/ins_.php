<div id="formins">
    <form name="forminserimento" id="forminserimento" method="post" action="">
        <table summary="">
        <caption>Inserimento Anagrafica</caption>
        <thead></thead>
        <tfoot> </tfoot>
        <tbody class="body_tab">
        <tr>
            <td colspan="3"><div align="center"><span class="style1">(*) </span>Campi obbligatori </div></td>
        </tr>
        <!--<tr class="aziendale">-->
        <tr>
            <td ><div align="left">Tipologia Anagrafica:</div></td>
            <td><div align="left">
                    <select name="id_tipologia" id="id_tipologia">
                        <option value="0" selected="selected">Seleziona..</option>
                        <?php
                        mysql_data_seek($tipologie,0);
                        while($ris=mysql_fetch_assoc($tipologie)) { ?>
                            <option value="<?php echo $ris['id_tipologia'];?>" <?php if($ris['id_tipologia']==$_REQUEST['ric']['id_tipologia|equal|A1']){echo "selected='selected'";}?>><?php echo $ris['tipologia'];?></option>
                        <?php }?>
                    </select>

                </div></td>
            <td >&nbsp;</td>
        </tr>

        <tr id="<?php if($conta_ana_principale==0){?>row_flg_principale<?php }?>" style="display: none;">
            <td><div align="left">Anagraficha Principale:</div></td>
            <td><div align="left">
                    <input name="flg_principale" type="checkbox" id="flg_principale" value="1" />

                </div></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="102"><div align="left">Titolo:</div></td>
            <td width="332" ><div align="left">
                    <input name="titolo_ana" type="text" id="titolo_ana" size="30" />
                </div></td>
            <td width="200">&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Rag. Soc. 1 (Nome):</div></td>
            <td><div align="left">
                    <input name="ragione_sociale1" type="text" id="ragione_sociale1" size="30" />
                    (*)</div></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="102" nowrap="nowrap"><div align="left">Rag. Soc. 2 (Cognome):</div></td>
            <td width="332"><div align="left">
                    <input name="ragione_sociale2" type="text" id="ragione_sociale2" size="30" />
                    (*)</div></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Codice Fiscale:</div></td>
            <td><div align="left">
                    <input name="cod_fiscale" type="text" id="cod_fiscale" size="30" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Partita Iva:</div></td>
            <td><div align="left">
                    <input name="partita_iva" type="text" id="partita_iva" size="30" class="check_piva" /><img id="ajax_loader" src="../images/ajax-loader.gif" alt="" >
                </div></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Data di Nascita (dd-mm-yyyy):</div></td>
            <td><div align="left">
                    <input name="data_nascita" type="text" id="data_nascita" size="12"  class="datepick add_date_picker"/>
                </div></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Luogo Nascita: </div></td>
            <td><div align="left">
                    <input name="luogo_nascita" type="text" id="luogo_nascita" size="30" />
                    <span class="style1"></span></div></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Provincia Nascita: </div></td>
            <td><div align="left">
                    <input name="provincia_nascita" type="text" id="provincia_nascita" size="3" />
                    <span class="style1"></span></div></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Indirizzo:</div></td>
            <td><div align="left">
                    <input name="indirizzo" type="text" id="indirizzo" size="50" />
                    <span class="style1"></span></div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Citta': </div></td>
            <td><div align="left">
                    <input name="citta" type="text" id="citta" size="30" />
                    <span class="style1"></span></div></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Provincia: </div></td>
            <td><div align="left">
                    <select name="provincia" id="provincia">
                        <option value="RM" selected="selected">RM</option>
                        <?php while( $ris = mysql_fetch_array($province)){?>
                            <option value="<?php echo $ris['siglaprovincia'];?>" ><?php echo $ris['siglaprovincia'];?></option>
                        <?php }?>
                    </select>
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">CAP:</div></td>
            <td><div align="left">
                    <input name="cap" type="text" id="cap" size="30" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Localita:</div></td>
            <td><div align="left">
                    <input name="localita" type="text" id="localita" size="50" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Paese:</div></td>
            <td><div align="left">
                    <input name="paese" type="text" id="paese" size="30" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Email:</div></td>
            <td><div align="left">
                    <input name="e_mail" type="text" id="e_mail" size="30" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Telefono1:</div></td>
            <td><div align="left">
                    <input name="telefono1" type="text" id="telefono1" size="30" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Telefono2:</div></td>
            <td><div align="left">
                    <input name="telefono2" type="text" id="telefono2" size="30" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Fax1:</div></td>
            <td><div align="left">
                    <input name="fax1" type="text" id="fax1" size="30" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Fax2:</div></td>
            <td><div align="left">
                    <input name="fax2" type="text" id="fax2" size="30" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Iban:</div></td>
            <td><div align="left">
                    <input name="iban" type="text" id="iban" size="30" />
                </div></td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td><div align="left">Associazione Tax: </div></td>
            <td><div align="left">
                    <select name="id_store_tax" id="id_store_tax">
                        <option value="0" selected="selected">Seleziona...</option>
                        <?php while( $ris_tax = mysql_fetch_array($tax)){?>
                            <option value="<?php echo $ris_tax['id_store_tax'];?>" ><?php echo $ris_tax['codice_tax'];?></option>
                        <?php }?>
                    </select>
                </div></td>
            <td >&nbsp;</td>
        </tr>

        <tr>
            <td colspan=4>
                <div align="left">
                    <input name="invia" type="submit" value="Invia" />
                    <input name="no_obj" type="hidden" value=anagrafica />
                    <input name="do" type="hidden" value="inserisci" />
                    <input name="no_id_anagrafica" id="no_id_anagrafica" type="hidden" />
                </div>
            </td>
        </tr>

        </tbody>
        </table>
    </form>
</div>