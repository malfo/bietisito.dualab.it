<?php
require_once('../acs_control.php');
include('../function/inc_generali.php');
?>
<?php
//var_dump($_REQUEST['test']);

$arr_ricerca=array("attivo|equal|"=>1);
$tipologie=lista_record($arr_ricerca,array(11));

$province=lista_record($arr_ricerca,array(13),0,"siglaprovincia ASC");
$tax=lista_record($arr_ricerca,array(24));

$arr_ricerca_tipo_oper["attivo|equal|A1"]=1;
$arr_ricerca_tipo_oper["id_tipologia_carrello|notequal|A1"]=6;
$tipologie_operazioni=lista_record($arr_ricerca_tipo_oper,array(16));

$arr_ricerca_principale["attivo|equal|A1"]=1;
$arr_ricerca_principale["flg_principale|equal|A1"]=1;
$ana_principale = lista_record($arr_ricerca_principale,array(10));
$conta_ana_principale = mysql_num_rows($ana_principale);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $htmltitle;?> - Anagrafiche</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="<?php echo $path_css;?>" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="<?php echo $path_css_fold;?>jquery-ui-latest.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $path_css_fold;?>jquery.autocomplete.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo $path_css_fold;?>prettyPhoto.css" type="text/css">
    <link rel="stylesheet" href="../js/fileupload/client/fileuploader.css" type="text/css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <script type="text/JavaScript" src="../js/jquery-latest.min.js"></script>
    <script type="text/JavaScript" src="../js/jquery-ui-personalized.min.js"></script>
    <script type="text/JavaScript" src="../js/jquery.jeditable.js"></script>
    <script type="text/javascript" src="../js/jquery.autocomplete.min.js"></script>
    <script src="../js/fileupload/client/fileuploader.js" type="text/javascript"></script>
    <script type="text/JavaScript" src="../js/jquery.prettyPhoto.js"></script>
    <script type="text/JavaScript" src="index.js"></script>
    <script type="text/javascript" src="../extra/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../extra/ckeditor/adapters/jquery.js"></script>
</head>

<body >
<div id="contenitore">
<?php include('../template/basic/menu/menu.php'); ?>
    <div id="corpopag">
        <div id="centrale">
            <?php
            switch($_REQUEST['invia']){
                case 1:
                    /*
                     * Ricerca ANAGRAFICHE
                     */

                    /*if ($_REQUEST['limit']>0){
                        $limit = $_REQUEST['limit'];
                    }*/
                    $limit = "30";

                    $arr_ricerca=$_REQUEST['ric'];

                    //aggiungo attivo=1 nel formato simile a quello del post
                    $arr_ricerca["attivo|equal|A1"]=1;
                    $record=lista_record($arr_ricerca,array(10),0,"A1.id_anagrafica DESC",$limit);

                    include('ric_.php');
                    ?>
                    <div id="" align="right"><a href="index.php?invia=ins"><i class="fa fa-plus-square fa-lg"></i>Nuova Anagrafica</a></div>
                    <?php
                    break;
                case "ins":
                    include('ins_.php');
                    break;
                default:
                    include('ric_.php');
                    ?>
                        <div id="" align="right"><a href="index.php?invia=ins"><i class="fa fa-plus-square fa-lg"></i>Nuovo Anagrafica</a></div>
                    <?php
            }

            ?>

            <div id="immagini" style="display: none;float:left;width:100%">
                <table width="100%">
                    <tr>
                        <td width="40%">
                            <div id="file-uploader">
                                <noscript>
                                    <p>Please enable JavaScript to use file uploader.</p>
                                    <!-- or put a simple form for upload here -->
                                </noscript>
                            </div>
                        </td>
                        <td>
                            <div id="imm_presenti"></div>
                        </td>
                    </tr>

                </table>

            </div>
            <?php
            if ($_REQUEST['invia']=="1"){
                include('list_.php');
            }
            ?>
        </div>
    </div>
</div>
<div id="messaggio" style="display:none"><?php echo $avviso;?></div>
<div id="msg_alert" style="display:none"></div>
</body>
</html>
<!-- <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><span id="testo-dialog"></span></p>
	<input type="hidden" id="idrecord" name="idrecord" /> -->