
$(document).ready(function(){

    /*
     *  Modifica campi
     */

    $(".edit_rec").editable('gtw_.php', {
        submitdata : {dorec:"mod_voce"},
        width:"70%",
        callback : function(value, settings) {

        }
    });
    $(".edit_rec_noreload").editable('gtw_.php', {
        submitdata : {dorec:"mod_voce"},
        width:"70%",
        callback : function(value, settings) {

        }
    });

    /*
     * Modifica Anagrafica
     */
    $(".mod_voce").click(function(){
        $(window).scrollTop(0);
        var id_record = $(this).attr('id').substr(3);
        //$("#examplet").tabs("load", 0);
        $.ajax({
            type: "POST",
            url: "gtw_.php",
            data: "dorec=prendi_record&id="+id_record+"&arric="+$('#arric').val(),
            success: function(msg){

                $("#formins").html(msg);
                $(".add_date_picker").datepicker({ dateFormat: 'dd-mm-yy', changeYear: true, yearRange: '2012:2015'});
            }
        });
        return false;

    });
    $(".del_ana").click(function(){
        var idsplit = $(this).attr('id').split("|");
        var id_record = idsplit[0];
        var arric = idsplit[1];
        //var href = $(this).attr("href");
        //var idtipodocumento = $("#id_tipologia_documento").val();
        //alert("art:"+idarticolo+"|car:"+idcarrello+"|flg:"+flgpadre+"|opz:"+idopzione+"|qta:"+qta);
        $("#msg_alert").html("Vuoi Eliminare il Record?");
        $("#msg_alert").dialog({
            resizable: false,
            height:200,
            modal: true,
            title: "Attenzione",
            buttons: {
                "Si": function() {
                    $.ajax({
                        type: "POST",
                        url: "gtw_.php",
                        data: "dorec=del_ana&id="+id_record+"&arric="+arric,
                        success: function(msg){

                            var msg_split=msg.split("^");
                            if (msg_split[0]=="OK"){
                                //alert('articoli.php?invia=1'+msg_split[1]);
                                //window.location='index.php?invia=1'+msg_split[1];
                                //alert(msg_split[1]);
                                window.location=msg_split[1];
                            }else{
                                $("#msg_alert").html("Problemi nell'eliminazione Anagrafica");
                                $("#msg_alert").dialog({
                                    resizable: false,
                                    height:200,
                                    width:400,
                                    modal: true,
                                    title: "Attenzione",
                                    buttons: {
                                        "Ok": function(){
                                            $(this).dialog("close");
                                        }
                                    }
                                });

                            }

                        }
                    });
                    return false;

                    //alert(href);

                    $( this ).dialog( "close" );

                },
                Cancel: function() {


                    $( this ).dialog( "close" );

                }
            }
        });
        return false;

    });

    function clean_field(){
        $(".clean_field").val("");
        $(".clean_field").html("");
    }


    $('#forminserimento').live('submit', function(event){

        var str = $("#forminserimento").serialize();
        //alert (str);
        gestione_anagrafica(str);

        return false;
    });

    /*
     * Inserisce o Modifica Anagrafica
     */
    function gestione_anagrafica(dati){
        var str1 = dati;
        //alert(str1);
        $.ajax({
            type: "POST",
            url: 'gtw_.php',
            data: str1,
            success: function(msg){
                var msg_split=msg.split("^");
                if (msg_split[0]=="OK"){
                    //alert('articoli.php?invia=1'+msg_split[1]);
                    //window.location='index.php?invia=1'+msg_split[1];
                    //alert(msg_split[1]);
                    window.location=msg_split[1];
                }else{
                    $("#msg_alert").html(msg_split[1]);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });

                }
            }
        });
    }
    //*************************//
    //Gestione Body Tabella Inserimento
    /*$(".body_tab").hide();
    $("#gest_body_tab").click(function(){


        if ($("#gest_body_tab").html()=="+"){
            $("#gest_body_tab").html("-");
            $(".body_tab").show();
            $(".add_date_picker").datepicker({ dateFormat: 'dd-mm-yy', changeYear: true, yearRange: '2012:2015'});
        }else{
            $("#gest_body_tab").html("+");
            $(".body_tab").hide();
            $("#forminserimento")[0].reset();
            $("#select_cat_ins").html("");
            $("#select_sottocat_ins").html("");
        }
        return false;
    });*/
    $("#canc_mod").live('click',function(){
        window.location='index.php?invia=1&ric[id_anagrafica|equal|A1]='+$("#no_id_anagrafica").val();
    });
    //*************************//

    /*.blur(function () {
     if ($(this).val() == "") {
     $(this).val($(this).attr("title"));
     }
     });*/
    /*
     * Gestione Sedi
     */
    $('#formins_sede').live('submit', function(event){

        var str = $("#formins_sede").serialize();
        nuova_sede(str);

        return false;
    });

    function nuova_sede(dati){
        var str1 = dati;
        id_record = $("#id_anagrafica").val();

        $.ajax({
            type: "POST",
            url: "gtw_.php",
            data: str1,
            success: function(msg){
                if(msg=="OK"){
                    //location.reload;
                    window.location='tabs_contents.php?id_anagrafica='+id_record+'&tab_attivo=3';
                }else if(msg=="KO"){
                    $("#msg_alert").html("Inserimento Sede non riuscito!Contattare l'amministratore");
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }else{
                    $("#msg_alert").html(msg);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        });

        return false;

    }

    /*
     * Fine Gestione Sedi
     */
    /*
     * Gestione Contatti
     */
    $('#formins_contatto').live('submit', function(event){

        var str = $("#formins_contatto").serialize();
        nuovo_contatto(str);

        return false;
    });
    function nuovo_contatto(dati){
        id_record = $("#id_anagrafica").val();
        var str1 = dati;
        //alert(str1);
        $.ajax({
            type: "POST",
            url: 'gtw_.php',
            data: str1,
            success: function(msg){
                if(msg=="OK"){
                    //location.reload;
                    window.location='tabs_contents.php?id_anagrafica='+id_record+'&tab_attivo=4';
                }else if(msg=="KO"){
                    $("#msg_alert").html("Inserimento Contatto non riuscito!Contattare l'amministratore");
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }else{
                    $("#msg_alert").html(msg);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        });
    }
    /*
     * Gestione Moto Anagrafica
     */
    $('#formins_moto').live('submit', function(event){

        var str = $("#formins_moto").serialize();
        nuova_moto(str);

        return false;
    });
    function nuova_moto(dati){
        id_record = $("#id_anagrafica").val();
        var str1 = dati;
        //alert(str1);
        $.ajax({
            type: "POST",
            url: 'gtw_.php',
            data: str1,
            success: function(msg){
                if(msg=="OK"){
                    //location.reload;
                    window.location='tabs_contents.php?id_anagrafica='+id_record+'&tab_attivo=5';
                }else if(msg=="KO"){
                    $("#msg_alert").html("Inserimento Moto non riuscito!Contattare l'amministratore");
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }else{
                    $("#msg_alert").html(msg);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        });
    }
    /*
     * Inserimento Operazioni Auto da Anagrafiche
     */
    $(".id_tipologia_carrello").live('change', function (event){
        var idanagrafica = $(this).attr('id').substr(3);
        if($('#top'+idanagrafica).val()==""){
            return false;
        }else{
            //alert("ok");
            //return false;
            var str = $("#form_operazioni"+idanagrafica).serialize();
            $.ajax({
                type: "POST",
                url: '../operazioni/gtw_.php',
                data: "dorec=ins_operazione&"+str+"&id_anagrafica="+idanagrafica,
                success: function(msg){
                    var msgsplit = msg.split("|");
                    if(msgsplit[0]=="OK"){
                        //location.reload;
                        window.location='../operazioni/tabs_contents.php?id_operazione='+msgsplit[1]+'&tab_attivo=1';

                    }else{
                        $("#msg_alert").html(msgsplit[1]);
                        $("#msg_alert").dialog({
                            resizable: false,
                            height:200,
                            width:400,
                            modal: true,
                            title: "Attenzione",
                            buttons: {
                                "Ok": function(){
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    $(".ins_operazione_officina").click(function(){
        var idsplit = $(this).attr('id').split("|");
        var idanagrafica = idsplit[0];
        var idanagraficamoto = idsplit[1];
        var idtipooperaizone = idsplit[2];
        $.ajax({
            type: "POST",
            url: '../operazioni/gtw_.php',
            data: "dorec=ins_operazione_auto&id_anagrafica="+idanagrafica+"&id_anagrafica_moto="+idanagraficamoto+"&id_tipologia_carrello="+idtipooperaizone,
            success: function(msg){
                var msgsplit = msg.split("|");
                if(msgsplit[0]=="OK"){
                    //location.reload;
                    window.location='../operazioni/tabs_contents.php?id_operazione='+msgsplit[1]+'&tab_attivo=1';

                }else{
                    $("#msg_alert").html(msgsplit[1]);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        });
    });

    /*
     * Controllo La presenza del partita iva
     */
    $("#ajax_loader").hide();
    $(".check_piva").focusout(function() {
        $("#ajax_loader").show();
        $.ajax({
            type: "POST",
            url: "gtw_.php",
            data: "dorec=check_partita_iva&partitaiva="+$("#partita_iva").val(),
            success: function(msg){
                //alert(id_record_sezione);

                if(msg=="OK"){

                    $("#ajax_loader").hide();
                }else{
                    $("#msg_alert").html("Partita Iva Presente");
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $("#partita_iva").val("");
                                $("#ajax_loader").hide();
                                $(this).dialog("close");
                            }
                        }
                    });
                }



            }
        });


    });
    /*
     * Fine Gestione Contatti
     */
    /*
     * Stampa Autocertificazione
     */
    $(".print_autocert").change(function(){
        var id;
        id = $(this).attr('id');
        if ($(this).val()==0){
            return false;
        }else{
            //alert(id);
            window.open("../stampa/stampa_ris_autocert.php?id_anagrafica="+id+"&tipo="+$(this).val(),"_blank");
        }
    });
    /*
     * Fine Stampa Autocertificazione
     */
    /*
     * MEssaggio dialog
     */

    if($("#messaggio").html()!=""){
        $("#gest_body_tab").html("-");
        $(".body_tab").show();
        $("#messaggio").dialog({
            resizable: false,
            height:200,
            modal: true,
            title: "Attenzione",
            buttons: {
                "Ok": function(){
                    $(this).dialog("close");
                }
            }
        });

    }
});