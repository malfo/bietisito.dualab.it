$(document).ready(function(){


    /*
     * Inizializzo gli oggetti
     */
    //CKEditor
    /*$(".cke_testo").ckeditor({
        toolbar: 'Full',
        enterMode : CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });

    //Calendar
    $(".add_date_picker").datepicker({ dateFormat: 'dd-mm-yy', changeYear: true, yearRange: '2012:2015'});


    $("#forminserimento input").live("keypress", function(e) {
        if (e.keyCode == 13){

            return false;
        }
    });*/

    /*
     *  Modifica campi
     */

    /*$(".edit_rec").editable('gtw_.php', {
        submitdata : {dorec:"mod_voce"},
        width:"70%",
        callback : function(value, settings) {
            location.reload();
        }
    });
    $(".edit_rec_noreload").editable('gtw_.php', {
        submitdata : {dorec:"mod_voce"},
        width:"70%",
        callback : function(value, settings) {

        }
    });*/


    /*
     * Gestione Radio Button Attributi
     */

    /*
     * Gestione del Click sull'elemento <li>
     * per il check sul radio
     */
    $('.li-attributi1').click( function(){

        $('input[name="id_attributo1"][value='+$(this).attr('id')+']').attr('checked', 'checked');
        $('input[name="id_attributo1"][value='+$(this).attr('id')+']').change();

    });
    $('.li-attributi2').live('click', function(){

        $('input[name="id_attributo2"][value='+$(this).attr('id')+']').attr('checked', 'checked');
        $('input[name="id_attributo2"][value='+$(this).attr('id')+']').change();

    });

    if($('#id_attributo1:checked').val()==0){
        //alert("ciao");
        $('input[name="id_attributo2"][value="0"]').attr('checked', 'checked');
    }
    /*
     * Metto in var il valore degli attributi iniziale
     */
    var idattributo1 = $('#id_attributo1_db').val();
    var idattributo2 = $('#id_attributo2_db').val();
    /*if($('input[type=radio][name=id_attributo1]:checked').val()==0){
        $("#idattributo2[value='0']").prop('checked','cheked');
    }*/
    var idattributo1_change = idattributo1;
    var idattributo2_change = idattributo2;
    if (idattributo1 == idattributo1_change && idattributo2 == idattributo2_change){
        $("#btn_attributi").attr('disabled', true);
    }
    //Se l'attributo 1 = 0 anche attributo 2 = 0
    if($('#id_attributo1:checked').val()==0){
        $('input[name="id_attributo2"][value="0"]').attr('checked', 'checked');
        $(".attributo2").attr('disabled', true);
        $('.li_attributi2').removeClass('li-attributi2');
    }else{
        $(".attributo2").removeAttr("disabled");
        $('.li_attributi2').addClass('li-attributi2');


    }
    $("input:radio[name=id_attributo1]").change( function(){
        idattributo1_change=$('#id_attributo1:checked').val();

        //Se l'attributo 1 = 0 anche attributo 2 = 0
        if($('#id_attributo1:checked').val()==0){
            $('input[name="id_attributo2"][value="0"]').attr('checked', 'checked');
            $(".attributo2").attr('disabled', true);
            $('.li_attributi2').removeClass('li-attributi2');
        }else{
            $(".attributo2").removeAttr("disabled");
            $('.li_attributi2').addClass('li-attributi2');

        }

        //Se gli attributi sono = a quelli già impostati disabilito il tasto "Salva"
        if (idattributo1 == idattributo1_change && idattributo2 == idattributo2_change){
            $("#btn_attributi").attr('disabled', true);
        }else{
            $("#btn_attributi").removeAttr("disabled");
        }


    });

    $("input:radio[name=id_attributo2]").change( function(){
        idattributo2_change=$('#id_attributo2:checked').val();

        //Se gli attributi sono = a quelli già impostati disabilito il tasto "Salva"
        if (idattributo1 == idattributo1_change && idattributo2 == idattributo2_change){
            $("#btn_attributi").attr('disabled', true);
        }else{
            $("#btn_attributi").removeAttr("disabled");
        }
    });


    /*
     * Fine Gestione Radio Button Attributi
     */
    $('#frm_attributi').live('submit', function(event){
        /*alert($('#id_attributo1:checked').val());
        alert(idattributo2_change);
        return false;*/
        // (idattributo1+"-"+idattributo1_change+"-"+idattributo2+"-"+idattributo2_change);
        var str = $("#frm_attributi").serialize();

        $("#msg_alert").html("L'azione eseguira' un reset di tutti gli attibuti.<br>Vuoi procedere?");
            $("#msg_alert").dialog({
                resizable: false,
                height:200,
                modal: true,
                title: "Attenzione",
                buttons: {
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    },

                    "Opzioni": function() {
                        gestione_attributi(str,"opz");
                        $( this ).dialog( "close" );

                    },
                    "Si": function() {
                        gestione_attributi(str,"atr");
                        $( this ).dialog( "close" );

                    }

                }
            });

        return false;

    });

    function gestione_attributi(str,azione){
        var dati = str;
        $.ajax({
            type: "POST",
            url: 'gtw_.php',
            data: dati,
            success: function(msg){

                if (msg=="OK"){
                    if (azione=="atr"){

                        window.location='tabs_contents.php?id_articolo='+$("#id_store_articoli").val()+'&tab_attivo=2';
                    }
                    if (azione=="opz"){

                        window.location='tabs_contents.php?id_articolo='+$("#id_store_articoli").val()+'&tab_attivo=3';
                    }
                }else{
                    $("#msg_alert").html("Associazione Attributi non riuscita!<br>Contatta il web master");
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        });


    }

    /*
     * Gestione Radio Button Opzioni
     */

    /*
     * Gestione del Click sull'elemento <li>
     * per il check sul radio
     */
    $('.li-opzioni1').click( function(){

        if ($(this).attr('id')=="all-opzioni1"){
            $('.no_checkopzioni1').attr('checked', 'checked');
            $('.no_checkopzioni1').change();
        }else{
            $('input[name="id_store_attributi_opzioni1"][value='+$(this).attr('id')+']').attr('checked', 'checked');
            $('input[name="id_store_attributi_opzioni1"][value='+$(this).attr('id')+']').change();
        }

    });
    //Inizializzo i campi Codice Ean/ Barcode e prezzo addizionale a disabled
    $("#barcode_opzione").attr('disabled', true);
    $("#prezzo_addizionale").attr('disabled', true);
    $("#btn_opzioni").attr('disabled', true);
    $("#btn_barcode_opzione").attr('disabled', true);
    $("#chk_genera_barcode").attr('disabled', true);
    //if($('#id_store_attributi_opzioni1:checked').val()==0 || !$("#id_store_attributi_opzioni1").is(':checked')){
    if($('#id_store_attributi_opzioni1:checked').val()>0){
        //alert("dddd");
        $(".opzione2").removeAttr('disabled');
        /*
         * Gestione del Click sull'elemento <li>
         * per il check sul radio
         */
        $('.li-opzioni2').click( function(){
            if ($(this).attr('id')=="all-opzioni2"){
                $('.no_checkopzioni2').attr('checked', 'checked');
                $('.no_checkopzioni2').change();
            }else{
                $('input[name="id_store_attributi_opzioni2"][value='+$(this).attr('id')+']').attr('checked', 'checked');
                $('input[name="id_store_attributi_opzioni2"][value='+$(this).attr('id')+']').change();
            }

        });
    }else{
        $(".opzione2").attr('disabled', true);

    }



    /*if($('input[type=radio][name=id_attributo1]:checked').val()==0){
     $("#idattributo2[value='0']").prop('checked','cheked');
     }*/


    //$("input:radio[name=id_store_attributi_opzioni1]").change( function(){
    $(".checkopzioni1").change( function(){
        if($('#id_store_attributi_opzioni1:checked').val()==0){

            $('input[name="id_store_attributi_opzioni2"][value="0"]').attr('checked', 'checked');
            $(".opzione2").attr('disabled', true);
            /*
             * Controllo se la coppia Nessuno : Nessuno
             * esiste nella lista Opzioni Articoli
             */
             check_opzioni($("#id_store_articoli").val(),0,0);
        }else{

            /*
             * Gestione del Click sull'elemento <li>
             * per il check sul radio
             */
            $('.li-opzioni2').click( function(){
                if ($(this).attr('id')=="all-opzioni2"){
                    $('.no_checkopzioni2').attr('checked', 'checked');
                    $('.no_checkopzioni2').change();
                }else{
                    $('input[name="id_store_attributi_opzioni2"][value='+$(this).attr('id')+']').attr('checked', 'checked');
                    $('input[name="id_store_attributi_opzioni2"][value='+$(this).attr('id')+']').change();
                }

            });


            $(".opzione2").removeAttr('disabled');


                /*
                 * Controllo se la coppia Nessuno : Nessuno
                 * esiste nella lista Opzioni Articoli
                 */
                check_opzioni($("#id_store_articoli").val(),$('#id_store_attributi_opzioni1:checked').val(),$('#id_store_attributi_opzioni2:checked').val());


        }

    });

    //$("input:radio[name=id_store_attributi_opzioni2]").change( function(){
    $(".checkopzioni2").change( function(){
        /*
         * Controllo se la coppia Nessuno : Nessuno
         * esiste nella lista Opzioni Articoli
         */
        check_opzioni($("#id_store_articoli").val(),$('#id_store_attributi_opzioni1:checked').val(),$('#id_store_attributi_opzioni2:checked').val());
    });
    $(".no_checkopzioni1").change( function(){
        $("#barcode_opzione").removeAttr('disabled');
        $("#barcode_opzione").focus();
        $("#prezzo_addizionale").removeAttr('disabled');
        $("#btn_opzioni").removeAttr('disabled');
        $("#btn_barcode_opzione").removeAttr('disabled');
        $(".row_opzione_articolo").css("background-color", "#F9F9F9");
        $(".opzione2").removeAttr('disabled');
    });
    $(".no_checkopzioni2").change( function(){
        $("#barcode_opzione").removeAttr('disabled');
        $("#barcode_opzione").focus();
        $("#prezzo_addizionale").removeAttr('disabled');
        $("#btn_opzioni").removeAttr('disabled');
        $("#btn_barcode_opzione").removeAttr('disabled');
        $(".row_opzione_articolo").css("background-color", "#F9F9F9");
    });

    /*
     * Fine Gestione Radio Button Opzioni
     */

    function check_opzioni(idarticolo, idopzione1, idopzione2){
        $.ajax({
            type: "POST",
            url: "gtw_.php",
            data: "azione=check_opzioni&idarticolo="+idarticolo+"&idopzione1="+idopzione1+"&idopzione2="+idopzione2,
            success: function(msg){
                var msg_split=msg.split("^");
                if (msg_split[0]=="OK"){
                    /*
                     * Se la combinazione è già esistente
                     * Disabilito i campi di Inserimento
                     * Evidenzio il record e abilito la modifica
                     */
                    $("#barcode_opzione").attr('disabled', true);
                    $("#prezzo_addizionale").attr('disabled', true);
                    $("#btn_opzioni").attr('disabled', true);
                    $("#btn_barcode_opzione").attr('disabled', true);
                    $("#chk_genera_barcode").attr('disabled', true);
                    $(".row_opzione_articolo").css("background-color", "#F9F9F9");
                    $("#row"+msg_split[1]).css("background-color", "#fbd850");
                    $("#lista_opzioni").contents().find("tr[id=row"+msg_split[1]+"]");
                    $('html, body').animate({
                        scrollTop: $("tr[id=row"+msg_split[1]+"]").offset().top
                    }, 1000);

                }
                if (msg_split[0]=="KO"){
                    /*
                     * Check Box genera Barcode
                     * Se checked disabilito il campo barcode_opzione
                     * e fisso il val del checkbox = 1 altrimenti val = 0
                     */
                    $("#chk_genera_barcode").removeAttr('disabled');
                    $("#chk_genera_barcode").click(function(){
                        if($(this).is(':checked')){
                            $("#chk_genera_barcode").val(1);
                            $("#barcode_opzione").attr('disabled', true);
                        }else{
                            $("#chk_genera_barcode").val(0);
                            $("#barcode_opzione").removeAttr('disabled');
                        }
                    });
                    /*
                     * Se la combinazione non è esistente
                     * Abilito i campi di Inserimento
                     */
                    $("#barcode_opzione").removeAttr('disabled');
                    $("#barcode_opzione").focus();
                    $("#prezzo_addizionale").removeAttr('disabled');
                    $("#btn_opzioni").removeAttr('disabled');
                    $("#btn_barcode_opzione").removeAttr('disabled');
                    $(".row_opzione_articolo").css("background-color", "#F9F9F9");

                }



            }
        });
    }

    $('#frm_opzioni').live('submit', function(event){
        /*alert($('#id_attributo1:checked').val());
         alert(idattributo2_change);
         return false;*/
        // (idattributo1+"-"+idattributo1_change+"-"+idattributo2+"-"+idattributo2_change);
        var str = $("#frm_opzioni").serialize()+"&no_genera_barcode="+$("#chk_genera_barcode").val();
        //alert(str);
        gestione_opzioni(str);

        return false;

    });
    function gestione_opzioni(str){
        var dati = str;
        $.ajax({
            type: "POST",
            url: 'gtw_.php',
            data: dati,
            success: function(msg){
                var msg_split=msg.split("^");
                if (msg_split[0]=="OK"){
                    var idopzione1 = $('#id_store_attributi_opzioni1:checked').val();
                    var idopzione2 = $('#id_store_attributi_opzioni2:checked').val();
                    window.location='tabs_contents.php?id_articolo='+$("#id_store_articoli").val()+'&tab_attivo=3&idopzione1='+idopzione1+'&idopzione2='+idopzione2;
                }
                if (msg_split[0]=="KO"){
                    $("#msg_alert").html(msg_split[1]+"<br>Vuoi Proseguire, generando un codice SKU?");
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Si": function() {
                                var dati2 = dati+"&no_confirm=OK";
                                gestione_opzioni(dati2);
                                $( this ).dialog( "close" );

                            },
                            "No": function() {
                                $( this ).dialog( "close" );
                            }
                        }
                    });
                }
            }
        });

    }

    $('#btn_carico_all').live('click', function(event){

        var str = $("#frm_elenco_opzioni").serialize()+"&id_store_articoli="+$("#id_store_articoli").val()+"&tipo_operazione=c";
        //alert(str);
        insert_movimenti_opzioni(str);

        return false;

    });
    function insert_movimenti_opzioni(str){
        var dati = str;
        $.ajax({
            type: "POST",
            url: 'gtw_.php',
            data: dati,
            success: function(msg){
                var msg_split=msg.split("^");
                if (msg_split[0]=="OK"){
                    window.location='tabs_contents.php?id_articolo='+$("#id_store_articoli").val()+'&tab_attivo=3';
                }
                if (msg_split[0]=="KO"){
                    $("#msg_alert").html(msg_split[1]);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        });


    }
    /*
     * Modifica
     */
    $(".mod_voce").click(function(){
        $(window).scrollTop(0);
        id_record = $(this).attr('id').substr(3);
        $("#immagini").hide();
        prendi_record_edit(id_record);

        return false;

    });
 
    /*
     *Codice Articolo Esterno (Associo un codice articolo alle anagrafiche)
     */
    $("#add_codice").live('click', function (event){
        id_articolo = $("#id_articolo").val();
        id_anagrafica = $("#id_anagrafica").val();
        codice_articolo = $("#codice_articolo_esterno").val();

        $.ajax({
            type: "POST",
            url: "gtw_.php",
            data: "dorec=associa_codice_esterno&idarticolo="+id_articolo+"&idanagrafica="+id_anagrafica+"&codicearticolo="+codice_articolo,
            success: function(msg){
                //alert(id_record_sezione);

                if(msg=="OK"){
                    //Collegamento al Tab di Riferimento
                    window.location='tabs_contents.php?invia=1&tab_attivo=1&id_articolo='+id_articolo;
                }else{
                    $("#msg_alert").html("Associazione non Riuscita. Contatta l'Amministratore!");
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }



            }
        });
    });

    /*
     * Elimina Record
     */
    $(".del_voce").click(function(){
        id_record = $(this).attr('id').substr(3);
        $("#msg_alert").html("Vuoi Eliminare il Record?");
        $("#msg_alert").dialog({
            resizable: false,
            height:200,
            modal: true,
            title: "Attenzione",
            buttons: {
                "Si": function() {

                    $.ajax({
                        type: "POST",
                        url: "gtw_.php",
                        data: "dorec=elimina&tipo=9&id="+id_record,
                        success: function(msg){
                            //alert(msg);
                            if(msg>0){

                                window.location='tabs_contents.php?id_articolo='+$("#id_store_articoli").val()+'&tab_attivo=3';

                            }else{
                                $("#msg_alert").html("Problemi nell'eliminazione dell'id: "+id_record+". Contatta l'Amministratore!");
                                $("#msg_alert").dialog({
                                    resizable: false,
                                    height:200,
                                    width:400,
                                    modal: true,
                                    title: "Attenzione",
                                    buttons: {
                                        "Ok": function(){
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                            }

                        }
                    });

                    $( this ).dialog( "close" );

                },
                Cancel: function() {

                    $( this ).dialog( "close" );

                }
            }
        });
        return false;

    });

    /*
     * Gestione Movimenti CARICO /SCARICO
     */

    $(".btn_carico").click(function(){

        var idarticolo = $("#id_store_articoli").val();
        /*
         * scrivo il tab attivo nell'id del button carico/scarico
         * Eseguo un split per recuperare i dati
         */
        var idbtn_split=$(this).attr('id').split("|");
        var tabattivo = idbtn_split[0];
        var idopzionearticolo = idbtn_split[1];
        var qta = $("#qta"+idopzionearticolo).val();
        var collezione = $("#col"+idopzionearticolo).val();
        var strdati = "&id_store_articoli="+idarticolo+"&id_store_attributi_opzioni_articoli="+idopzionearticolo+"&tipo_operazione=c&quantita="+qta+"&id_store_collezione="+collezione;

        add_movimento(strdati,tabattivo);
    });
    $(".btn_scarico").click(function(){
        var idarticolo = $("#id_store_articoli").val();
        /*
         * scrivo il tab attivo nell'id del button carico/scarico
         * Eseguo un split per recuperare i dati
         */
        var idbtn_split=$(this).attr('id').split("|");
        var tabattivo = idbtn_split[0];
        var idopzionearticolo = idbtn_split[1];
        var qta = $("#qta"+idopzionearticolo).val();
        var strdati = "&id_store_articoli="+idarticolo+"&id_store_attributi_opzioni_articoli="+idopzionearticolo+"&tipo_operazione=s&quantita="+qta;

        add_movimento(strdati,tabattivo);
    });

    function add_movimento(str,tabattivo){
        $.ajax({
            type: "POST",
            url: "gtw_.php",
            data: "do=ins_movimento"+str,
            success: function(msg){
                var msg_split=msg.split("^");
                if (msg_split[0]=="OK"){
                    //Collegamento al Tab di Riferimento
                    //window.location='tabs_contents.php?invia=1&tab_attivo='+tabattivo+'&id_articolo='+$("#id_store_articoli").val();
                    //alert(msg_split[1]);
                    $(".row_opzione_articolo").css("background-color", "#F9F9F9");
                    $("#row"+msg_split[2]).css("background-color", "#fbd850");
                    $("#qta"+msg_split[2]).val(1);
                    $("#dispo"+msg_split[2]).html("");
                    $("#dispo"+msg_split[2]).html(msg_split[1]);
                }
                if(msg_split[0]=="KO"){
                    $("#msg_alert").html(msg_split[1]);
                    $("#msg_alert").dialog({
                        resizable: false,
                        height:200,
                        width:400,
                        modal: true,
                        title: "Attenzione",
                        buttons: {
                            "Ok": function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }



            }
        });
    }
    /*
     * Controllo La presenza del codice articolo
     */
    /*$("#ajax_loader").hide();
    $(".check_barcode_opzione").focusout(function() {
        //alert($("#barcode_opzione").val());
        if($(this).val()!=""){
            $("#ajax_loader").show();
            $.ajax({
                type: "POST",
                url: "gtw_.php",
                data: "dorec=check_barcode_opzione&barcode_opzione="+$(this).val(),
                success: function(msg){
                    //alert(id_record_sezione);

                    if(msg=="OK"){

                        $("#ajax_loader").hide();
                    }else{
                        $("#msg_alert").html("Codice EAN/Barcode Presente");
                        $("#msg_alert").dialog({
                            resizable: false,
                            height:200,
                            width:400,
                            modal: true,
                            title: "Attenzione",
                            buttons: {
                                "Ok": function(){
                                    $("#barcode_opzione").val("");
                                    $("#ajax_loader").hide();
                                    $(this).dialog("close");
                                }

                            }
                        });
                    }



                }
            });

        }
    });*/
    /*
     *
     */

    /*
     * Gestione Stampa etichette
     * Salta l'apertura del PDF
     */

    $('.print_row').click(function () {
        print("../stampa/Dual-PieghevoleA4.pdf");
        /*var domain = location.protocol + "//" + location.host;
        var iframe = '<iframe src="../stampa/Dual-PieghevoleA4.pdf" id="print-iframe" name="print-iframe"></iframe>';
        $('body').append(iframe);
        window.frames["print-iframe"].focus();
        window.frames["print-iframe"].print();*/
    });

    //initiates print once content has been loaded into iframe
     function callPrint(iframeId) {
        var PDF = document.getElementById(iframeId);
        PDF.focus();
        PDF.contentWindow.print();
     }
     function print(url)
     {
        var _this = this,
            iframeId = 'print-iframe',
            $iframe = $('#print-iframe');
        $iframe.attr('src', url);

        $iframe.load(function() {
            _this.callPrint(iframeId);
        });
     }

    /*
     * Gestione Stampa Etichette
     */
     $("#chk_all_print").click(function(){
         if ($(this).attr('checked')){
             //alert("c");
             $(".stampa_etichette").attr('checked',true);
         }else{
             $(".stampa_etichette").attr('checked',false);
         }

     });
     $("#btn_stampa_etichette").click(function(){
         var str = $("#frm_elenco_opzioni").serialize();
         //alert(str);
         //console.log(str);
         window.open('../stampa/stampa_barcode.php?'+str,'_blank');
         return false;

     });

    /*
     * Gestione immagini
     */
        $("a[rel^='prettyPhoto']").prettyPhoto();

    /*
     * MEssaggio dialog
     */

    if($("#messaggio").html()!=""){
        $("#gest_body_tab").html("-");
        $(".body_tab").show();
        $("#messaggio").dialog({
            resizable: false,
            height:200,
            modal: true,
            title: "Attenzione",
            buttons: {
                "Ok": function(){
                    $(this).dialog("close");
                }
            }
        });

    }
});