<?php
/*
 * Per richieste php controllo l'azione proveniente dalla Rotta
 * In caso contrario, se la richiesta è ajax controlla la sessione attiva.
 * Nel caso di sessione non attiva rimando al Login
 */
if ($action){
    include_once("config/access.php");
}else{
    include_once("../../config/access_ajax.php");
    include_once("../../config/setup.php");
    include_once("../../config/db.php");
    include_once("../../config/mysql.php");
    include_once("../model/inc_generali.php");

}


//Menu Padre
$arr_ricerca_padre["attivo|equal|A1"]=1;
$arr_ricerca_padre["id_menu_padre|equal|A1"]=0;
$arr_ricerca_padre["menu|equal|A1"]=2;
$menupadre=lista_record($arr_ricerca_padre,array(501));


switch ($_REQUEST['do']){
    case "":
        //In caso di Inserimento do="";
        break;
    case "ric": //////////RICERCA
        $arr_ricerca=$_REQUEST['ric'];
        $arr_ricerca["attivo|equal|A1"]=1;
        $_SESSION['s_arric']=$arr_ricerca;
        $record=lista_record($arr_ricerca,array($idtabella),0,"ordine");
        $conta_record = mysql_num_rows($record);
        break;
    case "ins": //////////INSERIMENTO
        $insert=trasforma_post($_POST);

        $avviso.=testo_req_zero($_POST['menu'],"Tipologia");
        if($_POST['menu']==1){
            $avviso.=testo_req_zero($_POST['id_menu_padre'],"Menu Padre");
        }
        $avviso.=testo_req($_POST['titolo'],"Titolo");

        if($avviso==""){

            $insert['attivo']=1;
            $id_record=inserisci_record($insert,$_REQUEST['no_idtabella']);
            if($_POST['menu']==1){
                $dati_url = "&ric[menu|equal|A1]=1&ric[id_menu_padre|equal|A1]=".$_POST['id_menu_padre'];
            }else{
                $dati_url = "&ric[menu|equal|A1]=2";
            }

            $msg="OK|index.php?act=".$_REQUEST['no_obj']."&do=ric".$dati_url;

        }else{

            $msg=$avviso."|";
        }
        echo $msg;

        break;

    case "mod": //////////MODIFICA

        $insert=trasforma_post($_POST);

        $avviso.=testo_req_zero($_POST['menu'],"Tipologia");
        if($_POST['menu']==1){
            $avviso.=testo_req_zero($_POST['id_menu_padre'],"Menu Padre");
        }
        $avviso.=testo_req($_POST['titolo'],"Titolo");

        if($avviso==""){


            $id_record=modifica_record($insert,$_REQUEST['no_idtabella'],$_REQUEST['no_id']);
            $msg="OK|index.php?act=".$_REQUEST['no_obj']."&do=get&mode=tab&tab_attivo=1&id=".$_REQUEST['no_id'];

        }else{

            $msg=$avviso."|";
        }
        echo $msg;

        break;
    case "get": //////////GET RECORD

        $arr_ricerca["attivo|equal|A1"]=1;
        $arr_ricerca["id_menu|equal|A1"]=$id;
        $record=lista_record($arr_ricerca,array($idtabella));
        $conta_record = mysql_num_rows($record);
        $dati_= mysql_fetch_assoc($record);
        break;
    case "getp": //////////GET RECORD PERMESSI
        //Dati Menu
        $arr_datiarticolo["id_menu|equal|A1"]=$id;
        $record_datiarticolo = lista_record($arr_datiarticolo,array(501));
        $ris_datiarticolo=mysql_fetch_assoc($record_datiarticolo);

        //Gruppi Autorizzati
        $arr_gruppi_aut["id_menu|equal|A1"]=$id;
        $gruppi_aut = lista_record($arr_gruppi_aut,array(504));
        $conta_gruppi_aut = mysql_num_rows($gruppi_aut);

        //Gruppi
        $arr_gruppi["id_gruppo|notin|A1"]="(SELECT id_gruppo FROM admin_menu_gruppi WHERE id_menu=".$id.")";
        $arr_gruppi["attivo|equal|A1"]=1;
        $gruppi = lista_record($arr_gruppi,array(500));

        break;

    case "del": /////////// ELIMINA
        //$ret= elimina_record($_REQUEST,$_REQUEST['idtabella']);
        $ret= elimina_record_def_id($_REQUEST,$_REQUEST['idtabella']);
        //elimina le voci figlie
        elimina_record_def_id($_REQUEST,506);

        echo $ret;
        break;

    case "addgruppo": //////////AGGIUNGI GRUPPO
        $avviso.=testo_req_zero($_POST['idgruppo'],"Gruppo");
        if($avviso=="") {
            $insert['id_gruppo'] = $_REQUEST['idgruppo'];
            $insert['id_menu'] = $_REQUEST['idmenu'];
            $id_record = inserisci_record($insert, 502);
            //echo $id_record;
            if ($id_record > 0) {
                echo "OK";
            } else {
                echo "Inserimento Gruppo non riuscito!";
            }
        }else{
            echo $avviso;
        }

        break;
    case "delgruppo": //////////ELIMINA GRUPPO

        if($_REQUEST['idgruppo']>0){
            $sql="DELETE FROM admin_menu_gruppi WHERE id_menu='".$_REQUEST['idmenu']."' AND id_gruppo ='".$_REQUEST['idgruppo']."'";
            $nr_del=aggiorna($sql);
            if($nr_del>0){
                $msg="OK";
            }else{
                $msg="KO";
            }
            echo $msg;

        }

        break;
    default:
        // In caso di modifica manuale della variabile do
        // reset della variabile $action, distrugge la sessione e rimanda al Login
        if ($action){
            $action="";
        }else{
            session_destroy();
            include_once("../../config/access_ajax.php");
        }

}

?>