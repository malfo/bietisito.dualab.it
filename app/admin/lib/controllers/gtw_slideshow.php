<?php
/*
 * Per richieste php controllo l'azione proveniente dalla Rotta
 * In caso contrario, se la richiesta è ajax controlla la sessione attiva.
 * Nel caso di sessione non attiva rimando al Login
 */
if ($action){
    include_once("config/access.php");
}else{
    include_once("../../config/access_ajax.php");
    include_once("../../config/setup.php");
    include_once("../../config/db.php");
    include_once("../../config/mysql.php");
    include_once("../model/inc_generali.php");

}


switch ($_REQUEST['do']){
    case "":
        //In caso di Inserimento do="";
        break;
    case "ric": //////////RICERCA
        $arr_ricerca=$_REQUEST['ric'];
        $arr_ricerca["attivo|equal|A1"]=1;
        $_SESSION['s_arric']=$arr_ricerca;
        $record=lista_record($arr_ricerca,array($idtabella));
        $conta_record = mysql_num_rows($record);
        break;
    case "ins": //////////INSERIMENTO
        $insert=trasforma_post($_POST);

        $avviso.=testo_req($_POST['titolo'],"Titolo");
        //$avviso.=testo_req($_POST['descrizione'],"Descrizione");

        if($avviso==""){

            $insert['attivo']=1;
            $insert['data_inserimento']=data_phptomy(set_date_roma());
            $id_record=inserisci_record($insert,$_REQUEST['no_idtabella']);

            $msg="OK|index.php?act=".$_REQUEST['no_obj']."&do=ric";

        }else{

            $msg=$avviso."|";
        }
        echo $msg;

        break;

    case "mod": //////////MODIFICA

        $insert=trasforma_post($_POST);

        $avviso.=testo_req($_POST['titolo'],"Titolo");
        //$avviso.=testo_req($_POST['descrizione'],"Descrizione");

        if($avviso==""){


            $id_record=modifica_record($insert,$_REQUEST['no_idtabella'],$_REQUEST['no_id']);
            $msg="OK|index.php?act=".$_REQUEST['no_obj']."&do=get&mode=tab&tab_attivo=1&id=".$_REQUEST['no_id'];

        }else{

            $msg=$avviso."|";
        }
        echo $msg;

        break;
    case "get": //////////GET RECORD

        $arr_ricerca["attivo|equal|A1"]=1;
        $arr_ricerca["id_slideshow|equal|A1"]=$id;
        $record=lista_record($arr_ricerca,array($idtabella));
        $conta_record = mysql_num_rows($record);
        $dati_= mysql_fetch_assoc($record);
        break;

    case "del": /////////// ELIMINA
        $ret= elimina_record($_REQUEST,$_REQUEST['idtabella']);
        echo $ret;
        break;

    case "getimg": //////////GET RECORD IMMAGINI

        $arr_ricerca_imm["sezione|equal|A1"]=$idtabella;
        $arr_ricerca_imm["id|equal|A1"]=$id;
        $record_imm=lista_record($arr_ricerca_imm,array(62));
        $conta_record = mysql_num_rows($record_imm);


        break;
    case "upimg": //////////UPLOAD IMMAGINE

        $stato_imm=upload_immagine($_REQUEST,$_FILES);

        $pagina=$url_principale."index.php?act=".$_REQUEST['act']."&do=getimg&tab_attivo=img&mode=tab&id=".$_REQUEST['id'];
        //echo $pagina;
        header(sprintf("Location: %s",$pagina));
        exit;


        break;
    case "delimg": //////////ELIMINA IMMAGINE
        //echo $fold_images;
        //Elimina File
        unlink($fold_images.$_REQUEST['nomeimm']);

        //Elimina Record
        $ris=elimina_record_def_id($_REQUEST,62);

        if ($ris){
            $msg = "OK|index.php?act=".$_REQUEST['act']."&do=getimg&tab_attivo=img&mode=tab&id=".$_REQUEST['idrecord'];
        }else{
            $msg = "KO|Eliminazione Immagine non riuscita!";
        }

        echo $msg;

        break;
    default:
        // In caso di modifica manuale della variabile do
        // reset della variabile $action, distrugge la sessione e rimanda al Login
        if ($action){
            $action="";
        }else{
            session_destroy();
            include_once("../../config/access_ajax.php");
        }
}

?>