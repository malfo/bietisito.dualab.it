<?php
/*
 * Per richieste php controllo l'azione proveniente dalla Rotta
 * In caso contrario, se la richiesta è ajax controlla la sessione attiva.
 * Nel caso di sessione non attiva rimando al Login
 */
if ($action){
    include_once("config/access.php");
}else{
    include_once("../../config/access_ajax.php");
    include_once("../../config/db.php");
    include_once("../../config/mysql.php");
    include_once("../model/inc_generali.php");

}

switch ($_REQUEST['do']) {
    case "mod_voce": ////////// MODIFICA VOCE JEDITABLE
        $ret=modifica_voce($_REQUEST);
        echo $ret;
        break;

    case "mod_voce_drop": ////////// MODIFICA VOCE JEDITABLE
        $ret=modifica_voce_drop($_REQUEST);
        echo $ret;
        break;

    case "setflagpri": //////////SET FLAG Univoco

        //$ret=set_flag_univoco($_REQUEST);
        $ret=set_flag_univoco_generali_immagini($_REQUEST);
        echo $ret;

        break;
    case "setflagpub": //////////SET FLAG Pubblico

        $ret=set_flag_pubblico($_REQUEST);
        echo $ret;

        break;
}
?>