<?php
session_start();
require_once('config/setup.php');
require_once('config/access.php');

/*
 * Gestione del Login e Logout esterna alla funzione
 * per elaborare il redirect (La teniamo al di fuori della funziona per problemi di headers)
 */
switch ($_REQUEST['act']){
    case "login":

        require_once('config/db.php');
        require_once('config/mysql.php');
        require_once('lib/model/inc_login.php');
        $avviso=esegui_login($_REQUEST['username'],$_REQUEST['password']);
        //echo stato_login();
        //if(stato_login()==1){

        if($_SESSION['s_accesso']==1 && $_SESSION['s_id_login']>0){
            //echo "ok";
            $pagina="index.php?act=dashboard";
            header(sprintf("Location: %s",$pagina));
            exit;

        }else{
            //echo "ko";
            $pagina="index.php?act=loginko";
            header(sprintf("Location: %s",$pagina));
            exit;
        }

        break;

    case "logout":

        logout();
        $pagina="index.php";
        header(sprintf("Location: %s",$pagina));
        exit;

        break;
}

/**
 * root_contents() - Gestisce le richieste ed imposta l'elaborazione della risposta
 * @param $action
 * @param string $mode
 * @param string $dati
 */
function root_contents($action,$mode="",$dati=""){
    global $voci_pagine;
    global $path_link_menu,$fold_images,$link_images,$thumb,$url_principale;
    global $id,$tab_attivo;
    extract($dati);

    //var_dump($dati);
    switch ($action){
        case "":
            session_destroy();
            require_once("lib/modules/login/index.php");
            break;
        case "loginko":
            $avviso = "Username o Password Errati!";
            require_once("lib/modules/login/index.php");

            break;

        case "dashboard":
                //echo $action;
                require_once('template/basic/menu/menu.php');
                require_once("lib/modules/".$action."/index.php");

            break;
        case "chisiamo":
            $idtabella = 47;

            require_once('lib/controllers/gtw_'.$action.'.php');
            echo "<input type='hidden' id='id_tabella' value='$idtabella'/>";
            // Se il Valore della variabile $action viene resettato
            // distrugge la sessione e rimanda al login
            if($action){
                require_once('template/basic/menu/menu.php');

                if($mode==""){
                    require_once("lib/modules/".$action."/index.php");
                }else{

                    require_once("lib/modules/".$action."/tabs_contents.php");
                }
            }else{
                session_destroy();
                require_once("lib/modules/login/index.php");
            }
            break;
        case "motovendita":
            $idtabella = 43;

            require_once('lib/controllers/gtw_'.$action.'.php');
            echo "<input type='hidden' id='id_tabella' value='$idtabella'/>";
            // Se il Valore della variabile $action viene resettato
            // distrugge la sessione e rimanda al login
            if($action){
                require_once('template/basic/menu/menu.php');

                if($mode==""){
                    require_once("lib/modules/".$action."/index.php");
                }else{

                    require_once("lib/modules/".$action."/tabs_contents.php");
                }
            }else{
                session_destroy();
                require_once("lib/modules/login/index.php");
            }
            break;

        case "news":
            $idtabella = 40;

            require_once('lib/controllers/gtw_'.$action.'.php');
            echo "<input type='hidden' id='id_tabella' value='$idtabella'/>";
            // Se il Valore della variabile $action viene resettato
            // distrugge la sessione e rimanda al login
            if($action){
                require_once('template/basic/menu/menu.php');

                if($mode==""){
                    require_once("lib/modules/".$action."/index.php");
                }else{

                    require_once("lib/modules/".$action."/tabs_contents.php");
                }
            }else{
                session_destroy();
                require_once("lib/modules/login/index.php");
            }
            break;

        case "slideshow":
            $idtabella = 404;

            require_once('lib/controllers/gtw_'.$action.'.php');
            echo "<input type='hidden' id='id_tabella' value='$idtabella'/>";
            // Se il Valore della variabile $action viene resettato
            // distrugge la sessione e rimanda al login

            if($action){
                require_once('template/basic/menu/menu.php');

                if($mode==""){
                    require_once("lib/modules/".$action."/index.php");
                }else{

                    require_once("lib/modules/".$action."/tabs_contents.php");
                }
            }else{
                session_destroy();
                require_once("lib/modules/login/index.php");
            }
            break;

        case "offerte":
            $idtabella = 409;

            require_once('lib/controllers/gtw_'.$action.'.php');
            echo "<input type='hidden' id='id_tabella' value='$idtabella'/>";
            // Se il Valore della variabile $action viene resettato
            // distrugge la sessione e rimanda al login

            if($action){
                require_once('template/basic/menu/menu.php');

                if($mode==""){
                    require_once("lib/modules/".$action."/index.php");
                }else{

                    require_once("lib/modules/".$action."/tabs_contents.php");
                }
            }else{
                session_destroy();
                require_once("lib/modules/login/index.php");
            }
            break;


        case "menu":
            $idtabella = 501;

            require_once('lib/controllers/gtw_'.$action.'.php');
            echo "<input type='hidden' id='id_tabella' value='$idtabella'/>";
            // Se il Valore della variabile $action viene resettato
            // distrugge la sessione e rimanda al login
            if($action){
                require_once('template/basic/menu/menu.php');

                if($mode==""){
                    require_once("lib/modules/".$action."/index.php");
                }else{

                    require_once("lib/modules/".$action."/tabs_contents.php");
                }
            }else{
                session_destroy();
                require_once("lib/modules/login/index.php");
            }

            break;

        case "utenti":
            $idtabella = 503;

            //Gruppi
            $arr_ricerca_gruppi["attivo|equal|A1"]=1;
            //$arr_ricerca_gruppi["flg_principale|equal|A1"]=0;
            $gruppi=lista_record($arr_ricerca_gruppi,array(500));

            require_once('lib/controllers/gtw_'.$action.'.php');
            echo "<input type='hidden' id='id_tabella' value='$idtabella'/>";
            // Se il Valore della variabile $action viene resettato
            // distrugge la sessione e rimanda al login
            if($action){
                require_once('template/basic/menu/menu.php');

                if($mode==""){
                    require_once("lib/modules/".$action."/index.php");
                }else{

                    require_once("lib/modules/".$action."/tabs_contents.php");
                }
            }else{
                session_destroy();
                require_once("lib/modules/login/index.php");
            }

            break;
        default:
            session_destroy();
            require_once("lib/modules/login/index.php");

    }
}

/*if($_REQUEST['lang']==""){
    $language = $_SESSION['s_language'];
}else{
    $language = $_REQUEST['lang'];
}
session_language($language);*/
?>
