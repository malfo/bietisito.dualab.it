<?php
require_once('routes.php');
header('Content-type: text/html; charset=utf-8');
$act=$_REQUEST['act'];
$mode=$_REQUEST['mode'];
$dati=$_REQUEST;
//echo $mode;
//var_dump($dati);
//$_SESSION['s_sezione']=$sez;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>ECUO 1.2</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="<?php echo $path_css;?>" rel="stylesheet" type="text/css">
    <link href="<?php echo $path_css_menu1;?>" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="<?php echo $path_css_fold;?>jquery-ui-latest.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $path_css_fold;?>jquery.autocomplete.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $path_css_fold;?>prettyPhoto.css" type="text/css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

</head>

<body>


<?php
//echo $sez;
?>
<?php root_contents($act,$mode,$dati);?>
<input type="hidden" id="action" value="<?php echo $act; ?>"/>

</body>
<script type="text/JavaScript" src="<?php echo $path;?>/js/jquery-latest.min.js"></script>
<script type="text/JavaScript" src="<?php echo $path;?>/js/jquery-ui-personalized.min.js"></script>
<script type="text/JavaScript" src="<?php echo $path;?>/js/jquery.jeditable.js"></script>
<script type="text/javascript" src="<?php echo $path;?>/js/jquery.autocomplete.min.js"></script>
<script src="<?php echo $path;?>/js/fileupload/client/fileuploader.js" type="text/javascript"></script>
<script type="text/JavaScript" src="<?php echo $path;?>/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?php echo $path;?>/bundle/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $path;?>/bundle/ckeditor/adapters/jquery.js"></script>
<script type="text/JavaScript" src="<?php echo $path;?>/lib/modules/<?php echo $act;?>/index.js"></script>
<script type="text/JavaScript" src="<?php echo $path;?>/js/index_common.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#username").focus();

    });
</script>
</html>
