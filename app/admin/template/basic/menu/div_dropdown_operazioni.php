<script type="text/javascript">
    $(document).ready(function(){

        $("#select_carrelli").live('change', function (event){
            $("#form_carrelli").submit();

        });

    });
</script>
<style>
    div#div_select_carrelli {
        margin-left:72%;
        margin-top:3px;
        float:left;
        padding-right:3px;
    }
    div#div_open_operazione {
        float: left;
        margin-top: 3px;
    }
    div#div_open_operazione a {
        color: #777777;
        text-decoration: underline;
        font-size: 0.9em;
    }
    div#div_open_operazione a:hover {
        color: #F8A704;
        text-decoration: underline;
    }
</style>
<?php
//Per il gruppo user_interno filtro le operazioni "Officina"
if($_SESSION['s_id_gruppo']==3){
    $arr_ricerca_carrelli["id_tipologia_carrello|equal|A1"]=2;
}else{

}
$action = "../operazioni/tabs_contents.php?tab_attivo=1";
?>
<form name="form_carrelli" id="form_carrelli" method="post" action=<?php echo $action;?> >
    <?php

    //Prendo gli stati che determinano la chiusura del carrello
    $arr_stati_car["flg_close_car|equal|A1"]=1;
    $record_stati_car=lista_record($arr_stati_car,array(18));
    while($ris_stati_car=mysql_fetch_assoc($record_stati_car)){
        $stati_car[] = $ris_stati_car['id_stato_carrello'];
    }
    $implode_stati_car = implode(",",$stati_car);
    //Prendo i carrelli
    $arr_ricerca_carrelli["attivo|equal|A1"]=1;
    $arr_ricerca_carrelli["id_stato_carrello|notin|A1"]="($implode_stati_car)";


    //Per il gruppo user_esterno selezione solo carrelli dove � presente l'id_anagrafica dell'user
    if($_SESSION['s_id_gruppo']==4){
        $arr_ricerca_carrelli["id_anagrafica|equal|A1"]=$_SESSION['s_id_anagrafica'];
    }
    //Per l'utente Federico Al� seleziono solo i carrelli di tipo officina e Preventivi
    if($_SESSION['s_id_anagrafica']==63){
        $arr_ricerca_carrelli["id_tipologia_carrello|in|A1"]="(2,4)";
    }
    $record_carrelli=lista_record($arr_ricerca_carrelli,array(14),0,"id_tipologia_carrello, data_carrello");
    //echo $_SESSION['nome_carrello'];
    ?>
    <div id="div_select_carrelli">
        <select id="select_carrelli" name="id_operazione" style="width:205px">
            <option value="" selected="selected">Seleziona...</option>
            <?php
            while($ris_carrelli=mysql_fetch_assoc($record_carrelli)){
                //$arr_nome_carrello = split("_",$ris_carrelli["nome_carrello"]);
                $arr_articoli_carrello["id_carrello|equal|A1"]=$ris_carrelli["id_carrello"];
                $articoli_carrello=lista_record($arr_articoli_carrello,array(15));
                $conta_articoli_carrello=mysql_num_rows($articoli_carrello);
                $pref_tipo_operazione = torna_info_da_id($ris_carrelli["id_tipologia_carrello"],16,"prefisso");
                /*switch($ris_carrelli["id_tipologia_carrello"]){
                       case 1:
                           $pref_tipo_operazione="NGZ";
                           $data_carrello ="";
                           break;
                       case 2:
                           $pref_tipo_operazione="OFC";
                           $data_carrello ="";
                           break;
                       case 3:
                           $tipo_operazione="WEB";
                           $data_carrello ="";
                           break;
                       case 4:
                           $pref_tipo_operazione="PRV";
                           $data_carrello ="";
                           break;
                       case 5:
                           $pref_tipo_operazione="SLN";
                           $data_carrello ="";
                           break;
                       case 6:
                           $pref_tipo_operazione="RDK";
                           $data_carrello =": ".data_phptomy($ris_carrelli["data_carrello"]);
                           break;
                }*/

                ?>

                <option value="<?php echo $ris_carrelli["id_carrello"];?>" <?php if($ris_carrelli["id_carrello"]==$_SESSION['idcarrello']){?>selected="selected"<?php }?>><?php echo $pref_tipo_operazione." - NR.".$ris_carrelli["nr_carrello"]." - (".$conta_articoli_carrello.") - ".stripslashes($ris_carrelli["nome_carrello"]);?></option>
            <?php }?>
        </select>


    </div>
    <div id="div_open_operazione">
        <?php if(isset($_SESSION['idcarrello'])){ ?>
            <a href="../operazioni/tabs_contents.php?id_operazione=<?php echo $_SESSION['idcarrello'];?>&tab_attivo=1">
                <i class="fa fa-folder-open fa-2x" ></i>
            </a>
        <?php } ?>
    </div>
</form>