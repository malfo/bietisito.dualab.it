/**
 * @author roberto
 */
(function($) {  
   //plugin code will go here...  
   $.richiama = {  
     defaults: {  
      before_ric:function(){},
	  after_ric:function(){},
	  before_sub:function(){},
	  after_sub:function(){},
	  succ_ric:function(){}, //success nel richiamo
	  unsucc_ric:function(){},//unsuccess nel richiamo
	  id_contsubmit:"#formins",
	  id_submit:"#forminsert",
	  succ_sub:function(){},//success nell'invio
	  unsucc_sub:function(){},//unsuccess nell'invio
	  risp_ric:1,////tipo di risposta 1=html(),2=replaceWith(),nullo = OK o no
	  divrisp_ric:'',//div dove inserisco la risposta
      risp_sub:false,////tipo di risposta 1=html(),2=replaceWith(),nullo = OK o no
	  divrisp_sub:'',//div dove inserisco la risposta
	  gtw_ric:'',//gateway richiamo
	  gtw_sub:false,//gateway submit
	  str_ric:'',//stringa richiamo
	  str_sub:''//stringa submit
	  
	 }  
   };
   
      $.fn.extend({
  richiama:function(config) {
			
    var config = $.extend({}, $.richiama.defaults, config);

        

	richiamafun(config);

    return this;
  }
});
   
   function richiamafun(config){
   		//chiamata per prendere il record
		$.fn.gestoredati({
                    str: config.str_ric,
                    gtw: config.gtw_ric,
                    succ: function(){
						config.succ_ric();
						 $(config.id_contsubmit).show();
//						 $(config.divrisp_ric).html();
						$(config.id_submit).submit(function(){
								 config.before_sub();
								str2 = $(config.id_submit).serialize();
								gtw_t=config.gtw_sub;
								if(!config.gtw_sub){gtw_t=config.gtw_ric}
							$.fn.gestoredati({
								str: str2,
	                    		gtw: gtw_t,
	                    		succ: function(){
									config.succ_sub();
									$(config.id_contsubmit).hide();			
								},
								unsucc:config.unsucc_sub,
								risp:config.risp_sub,
	  							divrisp:config.divrisp_sub
							})
						return false;
						});	
						
					},
					unsucc:config.unsucc_ric,
                	risp:1,
					divrisp:config.id_contsubmit
                });
   		
		//alert(config.id_contsubmit+' '+config.risp_ric);
   
   }
   
 })(jQuery); 

 (function($) {  
   //plugin code will go here...  
   $.gestoredati = {  
     defaults: {  
       str:'',
	   gtw:'',
	   succ:function(){},
	   unsucc:function(){},
	   risp:false,
	   divrisp:''
     }  
   };
   
   $.fn.extend({
  gestoredati:function(config) {
			
    var config = $.extend({}, $.gestoredati.defaults, config);

        

	gestoredatifun(config);

    return this;
  }
});
     function gestoredatifun(config){
	 	
        var succ_invia_dati = function(msg){
            
            
            if (!config.risp) {
            
            
                if (msg.substr(0,2) == "OK") {
                    //alert(msg);
                	$("#msg").html("Operazione completata");
                    $("#id_oggetto").val(msg.substr(3));
                        config.succ();
                    
                }
                else {
                    $("#msg").html('Attenzione' + msg);
                    
                        config.unsucc();
                    
                }
            }
			else{
				if(config.risp==1){
				$(config.divrisp).html(msg);
				
                        config.succ();
                    
				}
				if(config.risp==2){
				$(config.divrisp).replaceWith(msg);

                        config.succ();

				}
				if(config.risp==3){
				$(config.divrisp).after(msg);

                        config.succ();

				}
			}
        }
        $.ajax({
            type: "POST",
            url: config.gtw,
            dataType: "html",
            data: config.str,
            beforeSend: function(){
                $("#loading_generale").show();
            },
            complete: function(){
                 $("#loading_generale").hide();
            },
            success: succ_invia_dati
        });
       
	  
    
	 }
 })(jQuery); 