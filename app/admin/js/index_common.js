$(document).ready(function(){

    var action = $("#action").val();
    var idtabella = $("#id_tabella").val();
    var idrecordsel = $("#hid_id_record").val();
    /*
     *  Modifica campi
     */

    $(".edit_rec").editable('lib/controllers/gtw_common.php', {
        submitdata : {do:"mod_voce"},
        width:"70%",
        callback : function(value, settings) {
            //location.reload();
            window.location='index.php?invia=1';
        }
    });
    $(".edit_rec_noreload").editable('lib/controllers/gtw_common.php', {
        submitdata : {do:"mod_voce"},
        width:"70%",
        callback : function(value, settings) {

        }
    });


    $(".edit_flag").live('click', function (event){
        var arrid = $(this).attr('id').split("|");
        //var act = action;
        var tipoflag = arrid[0];
        var idrecord = arrid[1];
        var tabella = arrid[2];
        var campo = arrid[3];
        //var doact = arrid[3];
        //var mode = arrid[4];
        //var tabattivo = arrid[5];
        //alert (tipoflag);
        if (tipoflag == "pri"){
            strdata = "do=setflagpri&id="+campo+"|"+idrecord+"|"+tabella;
        }

        if (tipoflag == "pub"){
            strdata = "do=setflagpub&id="+campo+"|"+idrecord+"|"+tabella;
        }

        if ($(this).is(':checked')){
            $.ajax({
                type: "POST",
                url: "lib/controllers/gtw_common.php",
                data: strdata+"&value=1",
                success: function(msg){
                    if(msg==1){

                        if (tipoflag=="pri"){
                            //window.location="index.php?act="+action+"&do=ric";
                            location.reload();
                        }
                    }else{
                        $("#msg_alert").html("Associazione Flag non Riuscita. Contatta l'Amministratore!");
                        $("#msg_alert").dialog({
                            resizable: false,
                            height:200,
                            width:400,
                            modal: true,
                            title: "Attenzione",
                            buttons: {
                                "Ok": function(){
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }

                }
            });

        }else{
            $.ajax({
                type: "POST",
                url: "lib/controllers/gtw_common.php",
                data: strdata+"&value=0",
                success: function(msg){
                    if(msg==0){
                        if (tipoflag=="pub"){
                            window.location="index.php?act="+action+"&do=ric";
                        }
                    }else{
                        $("#msg_alert").html("Dissociazione Flag non Riuscita. Contatta l'Amministratore!");
                        $("#msg_alert").dialog({
                            resizable: false,
                            height:200,
                            width:400,
                            modal: true,
                            title: "Attenzione",
                            buttons: {
                                "Ok": function(){
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }

                }
            });
        }
    });


    function clean_field(){
        $(".clean_field").val("");
        $(".clean_field").html("");
    }


    /*
     * MEssaggio dialog
     */

    if($("#messaggio").html()!=""){
        $("#messaggio").dialog({
            resizable: false,
            height:200,
            modal: true,
            title: "Attenzione",
            buttons: {
                "Ok": function(){
                    $(this).dialog("close");
                }
            }
        });

    }

    /*
     * Gestione immagini
     */
    $("a[rel^='prettyPhoto']").prettyPhoto();



});

