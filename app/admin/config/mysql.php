<?php

/*
#
# FUNZIONI MYSQL
#
*/
function connection_db()
{
    global $nome_host,$nome_db,$utente_db,$password_db;
//eseguo il collegamento al database
    $id_mysql = mysql_connect($nome_host,$utente_db,$password_db);
//eseguo la selezione del database
    mysql_select_db($nome_db,$id_mysql);
    return $id_mysql;
}
/*
Funzione per il recupero dei dati dal database
*/
function seleziona($query){
    global $nome_host,$nome_db,$utente_db,$password_db;
//eseguo il collegamento al database
    $id_mysql = mysql_connect($nome_host,$utente_db,$password_db);
//eseguo la selezione del database
    mysql_select_db($nome_db,$id_mysql);
//eseguo la query
    $risultato = mysql_query($query, $id_mysql) or die(mysql_error());
//recupero il numero di righe interessate
    $righe = mysql_num_rows($risultato);
// chiudo la connessione al database
    mysql_close($id_mysql) ;

//if ($righe > 0)	return $risultato;
//else 			return false;

    return $risultato;
}

/*
 funzione per l'inserimento dei dati nel database nel caso uso funzioni
 che richiedono la connessione al db prima di essere richiamate tipo mysql_real_escape_string().
*/
function inserisci_conn_esternal($query,$db_conn)
{

    mysql_select_db($nome_db,$db_conn);
//eseguo la query
    mysql_query($query, $db_conn) or die(mysql_error());
//recupero il numero di righe interessate
    $righe = mysql_affected_rows();
//recupero l'id dell'ultimo inserimento
    $prenotazione = mysql_insert_id($db_conn);
// chiudo la connessione al database
    mysql_close($db_conn) ;

    if ($righe > 0) return $prenotazione;
    else 			return false;
}
/*
 funzione per l'inserimento dei dati nel database 
*/
function inserisci($query)
{
    global $nome_host,$nome_db,$utente_db,$password_db;

//eseguo il collegamento al database
    $id_mysql = mysql_connect($nome_host,$utente_db,$password_db);
//eseguo la selezione del database
    mysql_select_db($nome_db,$id_mysql);
//eseguo la query
    mysql_query($query, $id_mysql) or die(mysql_error());
//recupero il numero di righe interessate
    $righe = mysql_affected_rows();
//recupero l'id dell'ultimo inserimento
    $prenotazione = mysql_insert_id($id_mysql);
// chiudo la connessione al database
    mysql_close($id_mysql) ;

    if ($righe > 0) return $prenotazione;
    else 			return false;
}

/*
 funzione per la rimozione dei dati dal database
*/
function rimuovi($query)
{
    global $nome_host,$nome_db,$utente_db,$password_db;
//eseguo il collegamento al database
    $id_mysql = mysql_connect($nome_host,$utente_db,$password_db);

//eseguo la selezione del database
    mysql_select_db($nome_db,$id_mysql);

//eseguo la query
    mysql_query($query, $id_mysql);

//recupero il numero di righe interessate
    $righe = mysql_affected_rows();

// chiudo la connessione al database
    mysql_close($id_mysql) ;

    if ($righe > 0) return true;
    else 			return false;
}
/*
funzione per la modifica dei dati dal database
*/
function aggiorna($query)
{
    global $nome_host,$nome_db,$utente_db,$password_db;
//eseguo il collegamento al database
    $id_mysql = mysql_connect($nome_host,$utente_db,$password_db);

//eseguo la selezione del database
    mysql_select_db($nome_db,$id_mysql);

//eseguo la query
    $risultato = mysql_query($query, $id_mysql);

//recupero il numero di righe interessate
    $righe = mysql_affected_rows();

// chiudo la connessione al database
    mysql_close($id_mysql) ;

    if ($righe > 0)	return $righe;
    else 			return false;
}

?>
