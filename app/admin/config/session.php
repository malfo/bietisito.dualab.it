<?php session_start();
function session_language($lingua){
    //echo "SESSIONE: ".$lingua;
	switch ($lingua){
		case "en":
			$_SESSION['s_language'] = "en";
			$_SESSION['s_estensione'] = "_en";
			break;
		case "it":
			$_SESSION['s_language'] = "it";
			$_SESSION['s_estensione'] = "";
			break;
		default:
			$_SESSION['s_language'] = "it";
			$_SESSION['s_estensione'] = "";
	}
}
?>