<?php
//definisco la varibile che contiene l'url della pagina di lavoro
define("PAGINA", $_SERVER['PHP_SELF']);
define("PAGINA_M", $_SERVER['PHP_SELF']."?".$_SERVER['argv'][0]);

//Gestione della classe del body
function body_class($sezione){

    switch ($sezione){
        case "chisiamo":
            echo "home";
            break;
        case "professionisti":
            echo "pagina";
            break;
        case "professionista":
            echo "pagina scheda";
            break;
        case "aree":
            echo "pagina aree";
            break;
        case "area":
            echo "pagina scheda";
            break;
        case "clienti":
			echo "pagina";
            break;
        case "lavora":
			echo "pagina scheda";
            break;
        case "dove":
			echo "pagina dove";
            break;
        case "contatti":
			echo "pagina contact";
            break;
        default:
    }
}
?>