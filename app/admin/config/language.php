<?php
$menu_superiore=array(
	"chisiamo"=>array("it"=>"Chi Siamo","en"=>"Who"),
	"professionisti"=>array("it"=>"I Professionisti","en"=>"I Professionisti"),
	"aree"=>array("it"=>"Aree di Competenza","en"=>"Aree di Competenza"),
	"clienti"=>array("it"=>"Clienti","en"=>"Clients"),
    "lavora"=>array("it"=>"Lavora con noi","en"=>"Job Opportunities"),
    "sedi"=>array("it"=>"Sedi","en"=>"Where"),
    "contatti"=>array("it"=>"Contatti","en"=>"Contacts"),
);
$voci_pagine=array(

    "box_contenuti_link"=>array("it"=>"Leggi","en"=>"Read"),
    "professionisti_contatti"=>array("it"=>"CONTATTI","en"=>"CONTACTS"),
    "professionisti_specializzazioni"=>array("it"=>"Specializzazioni","en"=>"Activities"),
    "home_tecnologia_titolo"=>array("it"=>"Innovazione & tecnologia italiana ","en"=>""),
    "home_contatti_titolo"=>array("it"=>"Per info e contatti clicca qui","en"=>""),
    "download_registrazione"=>array("it"=>"Per poter accedere all'area download devi effettuare la <a href='index.php?sez=formemail' style='font-size: 20px;'>registrazione</a>","en"=>"To download the documents, please <a href='index.php?sez=formemail' style='font-size: 20px;'>sign in</a>."),
    "formemail_title"=>array("it"=>"Per scaricare i documenti e per rimanere informato sulle ultime novità Firecom Automotive S.r.l., <a href='index.php?sez=formemail' style='font-size: 20px;'>registrati</a>","en"=>"To download the documents, please <a href='index.php?sez=formemail' style='font-size: 20px;'>sign in</a>."),
    "formemail_title1"=>array("it"=>"Registrati, per scaricare i documenti e per rimanere informato sulle ultime novità Firecom Automotive S.r.l.","en"=>"Sign in, to download the documents, Firecom Automotive S.r.l."),
    "formemail_nome"=>array("it"=>"Nome","en"=>"First Name"),
    "formemail_cognome"=>array("it"=>"Cognome","en"=>"Last Name"),
    "formemail_azienda"=>array("it"=>"Azienda","en"=>"Company"),
    "formemail_ruolo"=>array("it"=>"Ruolo","en"=>"Role"),
    "formemail_indirizzo"=>array("it"=>"Indirizzo","en"=>"Address"),
    "formemail_citta"=>array("it"=>"Citt&grave;","en"=>"City"),
    "formemail_provincia"=>array("it"=>"Provincia","en"=>"Location*"),
    "formemail_nazione"=>array("it"=>"Nazione","en"=>"Nation"),
    "formemail_telefono"=>array("it"=>"Telefono","en"=>"Phone Number"),
    "formemail_email"=>array("it"=>"Email","en"=>"Email"),
    "formemail_messaggio"=>array("it"=>"Messaggio","en"=>"Message"),
    "formemail_website"=>array("it"=>"Sito Web*","en"=>"Web Site*"),
    "formemail_privacy"=>array("it"=>"Acconsento al trattamento dei dati personali.","en"=>"I authorise the processing of my personal data."),
    "formemail_newsletter"=>array("it"=>"Desidero ricevere la Newsletter.","en"=>"Subscribe to our newsletter."),
    "formemail_button"=>array("it"=>"INVIA","en"=>"SEND"),
    "attivazione"=>array("it"=>"<h2>Complimenti, il tuo account è attivo. Esegui il Login per accedere a tutti i contenuti.<br>Buona Navigazione!</h2>","en"=>"<h2>Congratulations, your account is actived. Log in to download all documents.<br>Happy web surfing!</h2>"),
    "attivazione_error"=>array("it"=>"<h2>Problemi nell'attivazione, Ci scusiamo per il momentaneo disservizio.</h2>","en"=>"<h2>Sorry, activation server unavailable.</h2>"),
    "attivazione_message"=>array("it"=>"<h2>Azione non Consentita!</h2>","en"=>"<h2>Action not allowed</h2>"),
    "info_title"=>array("it"=>"<h2>Gli uffici e le aree ricerca/sviluppo e produzione si trovano nella sede Firecom Automotive di Paliano (FR)</h2>","en"=>"<h2>Firecom Automotive factory and offices are in Paliano (FR)</h2>"),
    "info_contatti"=>array("it"=>"Contatti","en"=>"Contacts"),
    "info_indirizzo"=>array("it"=>"Indirizzo","en"=>"Address"),
    "info_dealer"=>array("it"=>"Responsabile","en"=>"Sales Manager"),
    "login_registrazione"=>array("it"=>"Registrazione","en"=>"Sign In"),
    "login_recuperapassword"=>array("it"=>"Recupero Password","en"=>"Password Reminder"),

);
$tag=array(
    "title"=>array("it"=>"","en"=>""),
    "description"=>array("it"=>"","en"=>""),
    "keywords"=>array("it"=>"","en"=>""),

);
?>