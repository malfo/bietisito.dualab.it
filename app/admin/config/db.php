<?php
//date_deafault_timezone_set('Europe/Rome');
setlocale(LC_TIME, 'it_IT'); //per unix
//setlocale(LC_TIME, 'it');//per windows

$nome_host="localhost";
//DEV
/*
$nome_db="bietisit_master";
$utente_db="bietisit_adm";
$password_db="kwCRe!OehrtzoR!";
*/
//PROD
$nome_db="bieti307_master";
$utente_db="bieti307_top";
$password_db="kwCRe!OehrtzoR!";


//limite paginazione
//$num_record=2;


$tipi=array(
    "10"=>array("anagrafiche","id_anagrafica"),
    "11"=>array("anagrafiche_tipologie","id_tipologia"),
    "12"=>array("anagrafiche_conti_correnti","id_conto_corrente"),
    "13"=>array("generali_province","idprovincia"),
    "31"=>array("modulo_sedi","id_sede"),
    "40"=>array("modulo_news","id_news"),
    "401"=>array("modulo_news_tipologie","id_tipologia"),
    "402"=>array("modulo_news_immagini","id_news"),
    "42"=>array("modulo_form_lavora","id_form_lavora"),
    "43"=>array("modulo_moto_vendita","id_moto_vendita"),
    "431"=>array("modulo_moto_vendita_tipologie","id_tipologia"),
    "432"=>array("modulo_moto_ktm","id_moto_ktm"),
    "433"=>array("modulo_moto_ktm_tipologie","id_tipologia"),
    "434"=>array("modulo_moto_ktm_immagini","id_moto_ktm"),
    "435"=>array("modulo_moto_vendita","id_moto_vendita"),
    "436"=>array("modulo_moto_vendita_tipologie","id_tipologia"),
    "437"=>array("modulo_moto_vendita_immagini","id_moto_vendita"),
    "438"=>array("modulo_moto_vendita_temp","id_moto_vendita"),
    "439"=>array("modulo_moto_vendita_tipologie_temp","id_tipologia"),
    "440"=>array("modulo_moto_vendita_immagini_temp","id_moto_vendita"),
    "44"=>array("modulo_aree","id_area"),
    "47"=>array("modulo_chisiamo","id_chisiamo"),
    "45"=>array("modulo_banner","id_banner"),
    "46"=>array("modulo_clienti","id_cliente"),
    "48"=>array("modulo_media","id_media"),
    "49"=>array("modulo_media_file","id_file"),
    "491"=>array("modulo_media_tipologie","id_tipologia"),
    "403"=>array("modulo_local_web","id_local_web"),
    "404"=>array("modulo_slideshow","id_slideshow"),
    "405"=>array("modulo_download","id_download"),
    "406"=>array("modulo_download_categorie","id_download_categoria"),
    "407"=>array("modulo_download_documenti","id_download_documenti"),
    "408"=>array("modulo_contatti","id_contatto"),
    "409"=>array("modulo_offerte","id"),
    "50"=>array("gallery_categorie","id_gallery_categoria"),
    "51"=>array("gallery_sottocategorie","id_gallery_sottocategoria"),
    "52"=>array("gallery_foto","id_foto"),
    "53"=>array("gallery_video","id_video"),
    "54"=>array("gallery_file","id_gallery_file"),
    "60"=>array("generali_regioni","id_regione"),
    "61"=>array("generali_nazioni","id_nazione"),
    "62"=>array("generali_immagini","id_immagine"),
    "63"=>array("generali_citta","id_citta"),
    "64"=>array("generali_documenti","id_documento"),
    "65"=>array("generali_nazioni","sigla_nazione"),
    "500"=>array("admin_gruppi","id_gruppo"),
    "501"=>array("admin_menu","id_menu"),
    "502"=>array("admin_menu_gruppi","id"),
    "503"=>array("admin_utenti","id"),
    "504"=>array("admin_menu_gruppi","id_menu"),
    "505"=>array("admin_menu_gruppi","id_gruppo"),
    "506"=>array("admin_menu","id_menu_padre"),
    "1000"=>array("query","id_store_articoli"),
);

//require_once('mysql.php');
//require_once('mysql.class.php');
?>