<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 23/07/15
 * Time: 18:12
 */

class slideshow extends connect_db{

    public $query,$obj,$result,$model,$idtipologia,$msgErr;
    private $conta_ris,$dati,$rows;


    public function get_slideshow_internal($limit="",$debug=0){

        try{
            if ($limit>0){
                $limit = "LIMIT ".$limit;
            }

            $this->query = "SELECT\n".
                "`MS`.`id_slideshow`,\n".
                "`MS`.`titolo`,\n".
                "`MS`.`sommario`,\n".
                "`MS`.`url_esterno`,\n".
                "`MS`.`url_title`,\n".
                "`GI`.`nome_file`\n".
                "FROM\n".
                "`modulo_slideshow` AS `MS`\n".
                "JOIN `generali_immagini` AS `GI`\n".
                "ON `MS`.`id_slideshow` = `GI`.`id`\n".
                "WHERE\n".
                "MS.attivo = 1 AND MS.flg_pubblica = 1 AND GI.sezione = 404 AND GI.flg_principale = 1 AND MS.url_esterno = '' \n".
                "ORDER BY MS.data_inserimento DESC";

            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);


            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }

        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }

    public function get_slideshow($limit="",$debug=0){

        try{
            if ($limit>0){
                $limit = "LIMIT ".$limit;
            }

            $this->query = "SELECT\n".
                "`MS`.`id_slideshow`,\n".
                "`MS`.`titolo`,\n".
                "`MS`.`sommario`,\n".
                "`MS`.`url_esterno`,\n".
                "`MS`.`url_title`,\n".
                "`GI`.`nome_file`\n".
                "FROM\n".
                "`modulo_slideshow` AS `MS`\n".
                "JOIN `generali_immagini` AS `GI`\n".
                "ON `MS`.`id_slideshow` = `GI`.`id`\n".
                "WHERE\n".
                "MS.attivo = 1 AND MS.flg_pubblica = 1 AND GI.sezione = 404 AND flg_principale = 1\n".
                "ORDER BY MS.data_inserimento DESC";

            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);


            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }

        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }

    public function get_slideshow_details($id,$debug=0){

        try{
            $condition="";

            /*$this->query = "SELECT * \n".
                "FROM\n".
                "`modulo_slideshow` \n".
                "WHERE\n".
                "attivo = 1 AND flg_pubblica = 1 \n".
                "AND id_slideshow = ".$id;*/

            $this->query = "SELECT\n".
                "`MS`.*,\n".
                "`GI`.`nome_file`\n".
                "FROM\n".
                "`modulo_slideshow` AS `MS`\n".
                "JOIN `generali_immagini` AS `GI`\n".
                "ON `MS`.`id_slideshow` = `GI`.`id`\n".
                "WHERE\n".
                "MS.attivo = 1 AND MS.flg_pubblica = 1 AND GI.sezione = 404 AND GI.flg_principale = 1 \n".
                "AND MS.id_slideshow = ".$id;

            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);
            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }
        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }

}