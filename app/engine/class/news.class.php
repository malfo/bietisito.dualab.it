<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 23/07/15
 * Time: 18:12
 */

class news extends connect_db{

    public $query,$obj,$result,$model,$idtipologia,$idnews,$msgErr,$condition;
    private $conta_ris,$dati,$rows;


    public function get_news($limit="",$debug=0){

        try{
            if ($limit>0){
                $limit = "LIMIT ".$limit;
            }

            $this->query = "SELECT * \n".
                "FROM modulo_news WHERE attivo = 1 ORDER BY data_1 DESC ".$limit;
            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);


            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }

        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }

    public function get_news_details($idnews,$debug=0){

        try{
            $condition="";

            $this->query = "SELECT * \n".
                "FROM modulo_news \n".
                "WHERE id_news = ".$idnews;

            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);
            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }
        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }
}