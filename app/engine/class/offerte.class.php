<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 23/07/15
 * Time: 18:12
 */

class offerte extends connect_db{

    public $query,$obj,$result,$model,$idtipologia,$msgErr;
    private $conta_ris,$dati,$rows;


    public function get_offerte($limit="",$debug=0){

        try{
            if ($limit>0){
                $limit = "LIMIT ".$limit;
            }

            $this->query = "SELECT\n".
                "`MO`.*,\n".
                "`GI`.`nome_file`\n".
                "FROM\n".
                "`modulo_offerte` AS `MO`\n".
                "JOIN `generali_immagini` AS `GI`\n".
                "ON `MO`.`id` = `GI`.`id`\n".
                "WHERE\n".
                "MO.attivo = 1 AND MO.flg_pubblica = 1 AND GI.sezione = 409 AND GI.flg_principale = 1\n".
                "ORDER BY MO.data_inserimento DESC";

            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);


            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }

        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }

    public function get_offerta_details($id,$debug=0){

        try{
            $condition="";

            $this->query = "SELECT\n".
                "`MO`.*,\n".
                "`GI`.`nome_file`\n".
                "FROM\n".
                "`modulo_offerte` AS `MO`\n".
                "JOIN `generali_immagini` AS `GI`\n".
                "ON `MO`.`id` = `GI`.`id`\n".
                "WHERE\n".
                "MO.attivo = 1 AND MO.flg_pubblica = 1 AND GI.sezione = 409 AND GI.flg_principale = 1 \n".
                "AND MO.id = ".$id;

            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);
            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }
        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }

}