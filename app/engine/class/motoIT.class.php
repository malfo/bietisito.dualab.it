<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 23/07/15
 * Time: 18:12
 */

class moto_it extends connect_db{

    public $query,$obj,$result,$model,$idtipologia,$idmoto,$msgErr,$condition;
    private $conta_ris,$dati,$rows;


    public function get_tipologie($debug=0){

        try{
            $this->query = "SELECT * \n".
                "FROM modulo_moto_vendita_tipologie WHERE attivo = 1";
            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);
            //$this->result = mysql_fetch_assoc($this->obj);
            //var_dump($this->result);

            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }

        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }
    public function get_moto($idtipologia="0",$rand=1,$debug=0){
        try{
            $condition="";
            if($idtipologia>0){
                $condition= " AND K.id_tipologia = ".$idtipologia;
            }
            $order="";
            if($rand==1){
                $order = " ORDER BY RAND()";
            }
            $this->query = "SELECT K.id_moto_vendita, \n".
                "	KT.tipologia, \n".
                "	K.marca, \n".
                "	K.modello, \n".
                "	K.prezzo, \n".
                "	K.descrizione, \n".
                "	K.url_img \n".
                "FROM modulo_moto_vendita K INNER JOIN modulo_moto_vendita_tipologie KT ON K.id_tipologia = KT.id_tipologia WHERE K.attivo=1 ".$condition.$order;

            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);
            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }
        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;
    }

    public function get_moto_details($idmoto,$debug=0){

        try{
            $condition="";

            $this->query = "SELECT * \n".
                "FROM modulo_moto_vendita \n".
                "WHERE id_moto_vendita = ".$idmoto;

            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);
            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }
        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }

    public function get_moto_images($idmoto,$debug=0){

        try{
            $condition="";

            $this->query = "SELECT * \n".
                "FROM modulo_moto_vendita_immagini \n".
                "WHERE id_moto_vendita = ".$idmoto;

            if($debug==1){
                echo $this->query."<br>";
            }
            $this->obj = $this->seleziona($this->query);
            $conta_ris=mysql_num_rows($this->obj);

            $dati = array();
            if ($conta_ris>0){
                while ($rows = mysql_fetch_assoc($this->obj)){
                    $dati[]=$rows;
                }

            }else{
                $dati="Non esistono record";

            }
        }catch(Exception $e) {

            $dati='Message:'.$e->getMessage();

        }

        return $dati;

    }
}