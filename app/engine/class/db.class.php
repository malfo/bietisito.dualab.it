<?php
class connect_db{

    private $nome_host="localhost";
    //DEV
    /*
    private $nome_db="bietisit_master";
    private $utente_db="bietisit_adm";
    private $password_db="kwCRe!OehrtzoR!";
    */
    //PROD

    private $nome_db="bieti307_master";
    private $utente_db="bieti307_down";
    private $password_db="RFZ_D_R9TXM7aau";

    private $id_mysql;
    public $risultato;
    public $righe;
    public $idultimo;
    public $query;


    private function open_connect(){

        $this->id_mysql = mysql_connect($this->nome_host,$this->utente_db,$this->password_db);
        mysql_select_db($this->nome_db,$this->id_mysql);
        return $this->id_mysql;
    }

    private function close_connect($idmysql){
        mysql_close($idmysql);
    }

    public function seleziona($query){

        $idmysql = $this->open_connect();

        //eseguo la query
        $this->risultato = mysql_query($query, $idmysql) or die(mysql_error());
        //recupero il numero di righe interessate
        $this->righe = mysql_num_rows($this->risultato);
        // chiudo la connessione al database
        $this->close_connect($idmysql) ;

        //if ($righe > 0)	return $risultato;
        //else 			return false;

        return $this->risultato;
    }

    /*
     funzione per l'inserimento dei dati nel database
    */


    public function inserisci($query)
    {

        //eseguo il collegamento al database

        $idmysql = $this->open_connect();

        //eseguo la query
        $this->risultato = mysql_query($query, $idmysql) or die(mysql_error());
        //recupero il numero di righe interessate
        $this->righe = mysql_affected_rows();
        //recupero l'id dell'ultimo inserimento
        $this->idultimo = mysql_insert_id($idmysql);
        // chiudo la connessione al database
        $this->close_connect($idmysql) ;

        if ($this->righe > 0) return $this->idultimo;
        else 			return false;
    }

    /*
     funzione per la rimozione dei dati dal database
    */
    public function rimuovi($query)
    {
        //eseguo il collegamento al database
        $idmysql = $this->open_connect();

        //eseguo la query
        $this->risultato = mysql_query($query, $idmysql) or die(mysql_error());
        //recupero il numero di righe interessate
        $this->righe = mysql_affected_rows();
        //recupero l'id dell'ultimo inserimento
        $this->idultimo = mysql_insert_id($idmysql);
        // chiudo la connessione al database
        $this->close_connect($idmysql) ;

        if ($this->righe > 0) return $this->idultimo;
        else 			return false;
    }
    /*
    funzione per la modifica dei dati dal database
    */
    public function aggiorna($query)
    {
        //eseguo il collegamento al database
        $idmysql = $this->open_connect();

        //eseguo la query
        $this->risultato = mysql_query($query, $idmysql) or die(mysql_error());
        //recupero il numero di righe interessate
        $this->righe = mysql_affected_rows();
        //recupero l'id dell'ultimo inserimento
        $this->idultimo = mysql_insert_id($idmysql);
        // chiudo la connessione al database
        $this->close_connect($idmysql) ;

        if ($this->righe > 0) return $this->idultimo;
        else 			return false;
    }

    function set_date_time_roma(){
        $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
        return $dateTime->format("Y-m-d H:i:s");
    }
    function set_date_time_roma_it_format(){
        $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
        return $dateTime->format("d-m-Y H:i:s");
    }
    function set_date_time_roma_it_format_concat(){
        $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
        return $dateTime->format("d-m-Y_H:i:s");
    }
    function set_date_roma(){
        $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
        return $dateTime->format("Y-m-d");
    }
    function set_date_roma_it_format(){
        $dateTime = new DateTime("now", new DateTimeZone('Europe/Rome'));
        return $dateTime->format("d-m-Y");
    }
}
?>