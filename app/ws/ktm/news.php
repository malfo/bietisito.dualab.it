<?php
require_once('access.php');
header('Content-type:text/html; charset=utf-8');
//mysql_query("SET NAMES utf8");
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 17/06/15
 * Time: 15:24
 */
$html= file_get_contents("http://www.ktm.com/it/news/");

// a new dom object
$dom = new domDocument('1.0', 'utf-8');
$dom->preserveWhiteSpace = false;
$dom->loadHTML  ($html);
$title="";
$url="";
$category="";
$date="";
$text="";
$subtitle="";
$i=0;
while(is_object($article = $dom->getElementsByTagName("article")->item($i)))
{
    foreach($article->childNodes as $nodename)
    {
        /******* NEWS **********/
        //URL AND TITLE
        if($nodename->nodeName=='a')
        {
            $url = $nodename->getAttribute('href');
            foreach($nodename->childNodes as $subNodes)
            {
                //echo $subNodes->nodeName." - ".$subNodes->nodeValue."<br>";
                if ($subNodes->nodeName == "h3"){$title =  $subNodes->nodeValue; }
            }
        }
        //CATEGORY AND DATA
        if($nodename->nodeName=='header')
        {
            foreach($nodename->childNodes as $subNodes)
            {
                //echo $subNodes->nodeName." - ".$subNodes->nodeValue."<br>";
                if ($subNodes->nodeName == "p"){$category = $subNodes->nodeValue; }
                if ($subNodes->nodeName == "time"){$date = $subNodes->nodeValue; }
            }
        }
        //echo data_phptomy($date)."<br>";

    }

    /******** DETTAGLIO NEWS *********/
    //echo "http://www.ktm.com".$url;
    $html_detail= file_get_contents("http://www.ktm.com".$url);

    $dom_detail = new domDocument('1.0', 'utf-8');
    $dom_detail->preserveWhiteSpace = false;
    $dom_detail->loadHTML  ($html_detail);
    $x=0;
    $k=0;

    //DETTAGLIO

    $xpath = new DOMXpath($dom_detail);
    $classname = 'text';
    $articles = $xpath->query("//*[@class='" . $classname . "']");//$xpath->query('//nav[@class="nav-secondary"]');
    $links = array();
    $y=0;
    foreach($articles as $container) {
        if ($y==0){ $subtitle =  $container->nodeValue; }
        if ($y==1){ $text =  $container->nodeValue; }
        echo $text."<br>";
        $y++;
    }


    //CONTROLLO ESISTENZA NEWS
    $ric['titolo|equal|A1']=$title;
    $obj = lista_record($ric,array(40));
    $conta_news = mysql_num_rows($obj);


    if($conta_news==0){
        $insert['id_tipologia']=2;
        $insert['category_ktm']=$category;
        $insert['sommario']=ltrim($subtitle);
        $insert['titolo']=ltrim($title);
        $insert['descrizione']=ltrim($text);
        $insert['data_1']=$date;
        $insert['data_inserimento']=data_phptomy(set_date_roma());
        $insert['datatime_inserimento']=set_date_time_roma();
        $insert['flg_pubblica']=1;
        $insert['attivo']=1;

        $id = inserisci_record($insert,40);

        // IMMAGINI
        while(is_object($images = $dom_detail->getElementsByTagName("figure")->item($k))){
            foreach($images->childNodes as $nodename_img)
            {
                if($nodename_img->nodeName=='img')
                {
                    $url_img = $nodename_img->getAttribute('src');
                    $alt_img = $nodename_img->getAttribute('alt');


                    $insert_image['id_news']=$id;
                    $insert_image['url']=$url_img;
                    $insert_image['alt']=$alt_img;
                    $insert_image['attivo']=1;

                    $id_imm = inserisci_record($insert_image,402);

                    //echo $url_img.$alt_img."<br>";

                }
            }
            $k++;
        }

    }

    $i++;
}