<?php
//require_once('../common.php');
require_once('access.php');
header('Content-type:application/json; charset=utf-8');
// a new dom object
$url = 'https://bieti.dualab.it/ws/articoli_sito/elaborazione_json/crea_json_moto.php';
//  Initiate curl
$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$json=curl_exec($ch);
// Closing
curl_close($ch);
//var_dump(json_decode($result, true));
//exit;

$arr_json = json_decode($json);

//var_dump($arr_json);

//Elimino tutti i record
$del['id_tipologia']=3;
$nr=elimina_record_def($del,435);
$nrimg=elimina_record_def($del,437);

var_dump($nr);
foreach ($arr_json as $ris_arr){

    $arr_immatr = explode("-",$ris_arr->first_registration_date);
    $month = $arr_immatr[0];
    $year = $arr_immatr[1];

    //Array Inserimento
    $ins['id_tipologia']=3; //Ecuo / Mistermatic
    $ins['nr_annuncio']=$ris_arr->id_element;
    $ins['marca']=ltrim(rtrim($ris_arr->make));
    $ins['modello']=$ris_arr->title;
    $ins['mese']=$month;
    $ins['anno']=$year;
    $ins['prezzo']=$ris_arr->price;
    $ins['km']=$ris_arr->mileage;
    $ins['descrizione']=nl2br($ris_arr->description);
    $ins['attivo']=1;

    //Inserimento Moto
    $id_moto = inserisci_record($ins,435);
    //echo "<pre>";
    //var_dump($ins);
    //Immagini
    $i=0;
    foreach ($ris_arr->images as $images){

        if ($i==0){
            $edit['url_img'] = $images;
            modifica_record($edit,435,$id_moto);
        }

        $ins_imm['id_moto_vendita']=$id_moto;
        $ins_imm['id_tipologia']=3;
        $ins_imm['url_esterno']=$images;
        $ins_imm['attivo']=1;
        //echo "<pre>";
        //var_dump($ins_imm);
        inserisci_record($ins_imm,437);

        $i++;
    }

    $conta_art++;

}
?>