<?php
require_once('access.php');
header('Content-type:text/html; charset=utf-8');

//set the directory for the cookie using defined document root var
$fold=$_SERVER['DOCUMENT_ROOT']."/app/ws/motoit";
//echo $fold;
$dir = "/ctemp";
//echo $dir;
//build a unique path with every request to store
//the info per user with custom func.
//////$path = build_unique_path($dir);
$path = $fold.$dir;

//login form action url
$login_url="http://my.moto.it/Views/Open/Access/Login.aspx";
$post_data = "__EVENTTARGET=ctl00%24ctl00%24body%24body%24FrmLogin%24btnLogin&__EVENTARGUMENT=&__VIEWSTATE=wGVxzXjyK7Lk%2B5uDFVdtVbARaG9tjyblHwpRwLCJMjbkhwPE3bia4ANk9VTpXCLb3aEGJ43vw62tfIlmP2WdVdHDD%2FmpDAqcguruvkfAuPOTGW7zYCFe9QXD%2Bq9usdS3DEg6PTRFn0EPT3FKo5zYC4sYDpGiSS672khCIXl49l4XbBcafOYtBhg9zWXAE1ljWbgJAEjFS5W92QYW6hWKk8a%2Fj4Pta1tINlqDyp%2BpWWncY2b0UupGUGPga5SUGzO63e23WnXjDBkDY7VCduaQchGiKBs7xZqtA2LxZV46TVgSsuas5SCUAtu%2Fv87%2Bh7H82cuTSPGyPo8aER6QXpppL9Ola8IptUoAC0L5VNQTLrv9Keu3grLDAK61%2BGNYreKt%2B9ghiZUX6hXApf8PxhG5E4AQnVcWeF2rt%2B%2FivxxPcGX2n%2B5umZaAfY49aBwKPD4g04zX9HsICQKD4Gqr9E%2FkxGMY35sUrKH5ICljuQbwWXWDS0Z82teJzm8YYvI26o3aeybLQaN6W%2FZn0zewCGks5w%3D%3D&__EVENTVALIDATION=R6d7zvHCZhU82ajVCcUlA5tea4fwYS0MCTl6bDunLcjULgabVaNXPsGCgJM2JS9Qmtx9dP50IB9rKRd8b7r4pqCq2ToDq%2BRyNpBPAaPRQ3LbnhDgDzT1wbl4XRxj6UlFZ2W4caUQ%2FnfiKVl7EAahnV%2BPTfwxlHbOeFo6JfaHGpo%3D&ctl00%24ctl00%24body%24body%24FrmLogin%24txtUsrLogin=bieti%40bieti.it&ctl00%24ctl00%24body%24body%24FrmLogin%24txtUsrPassword=200893";

$cookie_file_path = $path."/cookie.txt";
$cookie_file_path_moto = $path."/cookie_moto.txt";
//Create a curl object
$ch = curl_init();

//Set the useragent
$agent = $_SERVER["HTTP_USER_AGENT"];
curl_setopt($ch, CURLOPT_USERAGENT, $agent);

//Set the URL
curl_setopt($ch, CURLOPT_URL, $login_url );

//This is a POST query
curl_setopt($ch, CURLOPT_POST, 1 );

//Set the post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

//We want the content after the query
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

//Follow Location redirects
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

/*
Set the cookie storing files
Cookie files are necessary since we are logging and session data needs to be saved
*/

curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);

//Execute the action to login
$postResult = curl_exec($ch);

$url = "http://my.moto.it/Views/Registered/Market/Used/List.aspx?mf=011&po=D&ord=d&totPages=1&itemsperpage=1000&p=0";
curl_setopt($ch, CURLOPT_URL, $url);
$html = curl_exec($ch);

$dom = new domDocument('1.0', 'utf-8');
$dom->loadHTML($html);
//echo $html;

$xpath = new DOMXpath($dom);
$classname = 'blocco-annuncio';
$articles = $xpath->query("//*[@class='" . $classname . "']");//$xpath->query('//nav[@class="nav-secondary"]');
$conta_art=0;

//Check Tabella
$ric['attivo|equal|A1']=1;
$ric['id_tipologia|equal|A1']=2;
$obj = lista_record($ric,array(435));
$conta_record = mysql_num_rows($obj);

//Elimino tutti i record di moto.it
$del['id_tipologia']=2;
elimina_record_def($del,435);
elimina_record_def($del,437);

//////elimina_record_def($del,438);
//////elimina_record_def($del,440);

$dom_annuncio = new domDocument('1.0', 'utf-8');
$conta_art=0;
foreach($articles as $link) {

    $nr_annuncio =  $link->getAttribute("data-id");
    $url_annuncio = "http://www.moto.it/market/ad/".$nr_annuncio;
    echo $url_annuncio."<br>";
    ///////Data
    $arrdata = $xpath->query("//*[@class='box-msg_dateins']");
    $data = $arrdata->item($conta_art)->nodeValue;
    //echo $data;

    $split_data = explode(" ",$data);
    $explode_data = explode("/",$split_data[1]);
    if ($split_data[1]=="Oggi"){
        $data_format = set_date_roma();
    }else{
        $data_format = "20".$explode_data[2]."-".$explode_data[1]."-".$explode_data[0];
    }

    //echo $data_format."<br>";

    ///////Brand
    $divbrand = $xpath->query("//*[@class='brand']");
    $brand = $divbrand->item($conta_art)->nodeValue;
    //echo $brand."<br>";

    ///////Model
    $divmodel = $xpath->query("//*[@class='model']");
    $model = $divmodel->item($conta_art)->nodeValue;
    //echo $model."<br>";

    ///////Prezzo
    $divprice = $xpath->query("//*[@class='price']");
    $price = $divprice->item($conta_art)->nodeValue;
    $decode_price = utf8_decode($price);
    $explode_price = explode("?",$decode_price);
    $price_str = $explode_price[0];
    $price_num = $stringa = str_replace(".", "", $price_str);
    //echo $price_num."<br>";

    ///////KM
    $divkm = $xpath->query("//*[@data-tooltip='Km percorsi']");
    $km = $divkm->item($conta_art)->nodeValue;
    $km = trim(str_replace("Km ","",$km));
    if (!is_numeric($km)){
        $km=0;
    }
    //$split_km = explode(" ",$km);
    //var_dump($split_km);
    //echo "km: ".str_replace("Km ","",$km)."<br>";


    ///////ANNO
    $divanno = $xpath->query("//*[@class='year ']");
    $anno = $divanno->item($conta_art)->nodeValue;
    $explode_anno = explode(" ",$anno);
    $anno_num = $explode_price[1];
    //echo $anno."<br>";


    //Array Inserimento
    $ins['id_tipologia']=2;
    $ins['marca']=ltrim(rtrim($brand));
    $ins['modello']=$model;
    $ins['anno']=$anno_num;
    $ins['prezzo']=$price_num;
    $ins['km']=$km;
    $ins['nr_annuncio']=$nr_annuncio;
    $ins['attivo']=0;
    $conta_imm=1;


    //Inserimento Moto
    $id_moto = inserisci_record($ins,435);
    //////$id_moto = inserisci_record($ins,438);
    $conta_art++;

}
curl_close($ch);
?>