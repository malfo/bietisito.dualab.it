<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Concessionario KTM Roma - Bi.& TI.</title>
<meta name="title" content="Concessionario KTM Roma - Bi.& TI." />
<meta name="description" content="Concessionario KTM Roma - Serietà, Competenza, Passione, Vieni a trovarci presso il nostro Store, Bi.& Ti.">
<link rel="stylesheet" href="<?php echo PATH;?>template/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo PATH;?>template/css/default.css"/>
<link rel="stylesheet" href="<?php echo PATH;?>template/css/juiceCookies.css" type="text/css" />
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo PATH;?>template/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo PATH;?>template/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo PATH;?>template/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo PATH;?>template/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo PATH;?>template/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo PATH;?>template/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo PATH;?>template/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo PATH;?>template/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo PATH;?>template/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo PATH;?>template/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo PATH;?>template/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo PATH;?>template/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo PATH;?>template/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo PATH;?>template/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo PATH;?>template/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">