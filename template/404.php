<!DOCTYPE HTML>

<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>404 Pagina non trovata</title>
		<style>
			body, html, a {
				width: 100%;
				height: 100%;
				display: block;
				margin: 0;
				padding: 0;
				border: 0;
			}
			
			body {background: #222222 url(img/404.png) center center no-repeat;}
			
		</style>
	</head>

	<body>
		<a href="<?echo "http://".$_SERVER['HTTP_HOST'];?>"></a>
	</body>

</html>