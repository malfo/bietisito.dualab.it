<?php if($sezione == "home") {?>

<span id="page-top" class="sez-ancora"></span>
	
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#page-top"><img src="<?php echo PATH;?>template/css/img/logo.png" alt="logo"></a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a class="page-scroll" href="#sez-usato">usato</a></li>
        <li><a class="page-scroll" href="#sez-chi-siamo">chi siamo</a></li>
        <li><a class="page-scroll" href="#sez-gamma">gamma</a></li>
        <li><a class="page-scroll" href="#sez-contatti">contatti</a></li>
        <li><a class="page-scroll" href="#sez-dove-siamo">dove siamo</a></li>
        <li><a class="page-scroll" href="#sez-store">store</a></li>
      </ul>
    </div>
  </div>
</nav>

<?php } else { ?>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo SITEURL?>"><img src="<?php echo PATH;?>template/css/img/logo.png" alt="logo" width="200" height="120"></a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo SITEURL?>">Torna alla home</a></li>
      </ul>
    </div>
  </div>
</nav>

<?php } ?>