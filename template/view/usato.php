	<section class="slideshow">
		<div class="container no-margin" style="min-height: 130px;"></div>
	</section>	
	
	<?php $motoObj = new moto_it(); $allMotos = $motoObj->get_moto(0,0);?>
	<section class="bianco">
		<div class="page-header arancione" id="usato"><h1 class="">Bi&Ti usato</h1></div>
		<div class="container">
			<div class="row">
				<div class="col-md-3"><?php include("parts/usato-accordion.php");?></div>
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-12"><h2>Tutto il nostro usato.</h2></div>
					</div>
					<div class="row">
							<?php foreach($allMotos as $moto) {?>
						        <div class="col-md-4">
							        <img src="<?php echo $moto["url_img"];?>" alt="<?php echo $moto["marca"]." ".$moto["modello"];?>">
							        <div>
								        <p>
									        <span><?php echo $moto["marca"]." ".$moto["modello"];?></span>
									        <span>€ <?php echo $moto["prezzo"];?></span>
									        <span><a href="<?php echo SITEURL;?>modello-usato/<?php echo $moto["marca"].$moto["modello"];?>/<?php echo $moto["id_moto_vendita"];?>">Vai alla scheda</a></span>
									    </p>
									</div>
							    </div>		
							<?php } ?>
					</div>

					<br clear="all">
					<div class="row">
						<div class="col-md-12" style="text-align: right;">
							<a href="<?php echo SITEURL?>" class="btnBack" title=""><img src="<?php echo PATH;?>template/img/btnBack.png" alt="btnBack" width="" height="" /></a>
						</div>
					</div>						
					<?php //echo $compURI["id"]; print_r($moto); ?>
				</div>	
			</div>
		</div>    
	</section>