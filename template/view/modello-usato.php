<section class="slideshow">
    <div class="container no-margin" style="min-height: 130px;"></div>
</section>

<?php $motoObj = new moto_it(); $allMotos = $motoObj->get_moto();?>
<section class="bianco">
    <?php $motoDetails = $motoObj->get_moto_details($compURI["id"]); $moto = $motoDetails[0]; $motoImages = $motoObj->get_moto_images($compURI["id"]);?>
    <div class="page-header arancione" id="usato"><h1 class="">Bi&Ti usato</h1></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3"><?php include("parts/usato-accordion.php");?></div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php echo $moto["marca"];?>: <?php echo $moto["modello"];?> - Prezzo: € <?php echo $moto["prezzo"];?></h2>

                        <div class="row">
                            <div class="col-md-10">
                                <img src="<?php echo $moto["url_img"];?>" title="<?php echo $moto["modello"];?>" alt="<?php echo $moto["modello"];?>" class="principale" id="principale">
                            </div>
                            <div class="col-md-2">
                                <?php foreach($motoImages as $motoImage){?>
                                    <img src="<?php echo $motoImage["url_esterno"];?>" title="<?php echo $moto["modello"];?>" alt="<?php echo $moto["modello"];?>" class="principale">
                                <?php }?>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <br clear="all">
                        <?php echo "<strong>KM: ".$moto["km"]."</strong>";?>
                        <br clear="all">
                        <?php //echo utf8_encode( $moto["descrizione"]);?>
                        <?php echo $moto["descrizione"];?>
                    </div>
                </div>
                <br clear="all">


                <div class="page-header" style="background: transparent;">
                    <h1>informazioni</h1>
                    <h6><i>Scrivici qui e ti risponderemo al più presto.</i></h6>
                </div>
                <div class="container" id="FloatBox">

                    <div class="row">
                        <form id="formUsato" name="formUsato" action="#" method="post">
                            <div class="col-md-6">
                                <input id="oggetto" name="oggetto" type="hidden" class="form-control" value="<?php echo $moto["marca"]." ". $moto["modello"];?>">
                                <input id="nome" name="nome" type="text" class="form-control" placeholder="Nome">
                                <input id="cognome" name="cognome" type="text" class="form-control" placeholder="Cognome">
                                <input id="email" name="email" type="email" class="form-control" placeholder="Email">
                                <input id="telefono" name="telefono" type="text" class="form-control" placeholder="Telefono">
                                <div class="col-md-12" id="risposta"></div>
                            </div>
                            <div class="col-md-6">
                                <textarea name="messaggio" id="messaggio" class="form-control" rows="3" placeholder="Messaggio"></textarea>
                                <button name="invia" type="btn" class="btn btn-default" id="invia">INVIA</button>

                            </div>
                        </form>
                    </div>


                </div>


                <br clear="all">


                <div class="row">
                    <div class="col-md-6">
                        <!-- <a href="#" id="btnForm" title=""><img src="<?php echo PATH;?>template/img/btnInfo.png" alt="btnForm" width="" height="" /></a> -->
                    </div>
                    <div class="col-md-6">
                        <a href="<?php echo SITEURL?>" class="btnBack" title="" id="btnBack"><img src="<?php echo PATH;?>template/img/btnBack.png" alt="btnBack" width="" height="" /></a>
                    </div>
                </div>
                <?php //echo $compURI["id"]; print_r($moto); ?>
            </div>
        </div>
    </div>
</section>