				<div class="col-md-4">
					<div class="page-header">
						<h1>newsletter</h1>
						<h6><i>L'iscrizione è gratuita.</i></h6>
					</div>
					<div class="container"  style="text-align: center;">
						<p>
						    Per Ricevere News,<br>
							offerte e/o promozioni<br>
							e rimanere sempre aggiornato<br>
							sulle ultime novità<br><br>
							
							<span style="text-transform: uppercase; font-weight:400; color: #ea5b0c;">Registrati Ora</span> <br>
						</p>
					
						<form id="formN" name="formN" action="#" method="post">
							<div class="form-group">
								<input type="text" name="nominativo" class="form-control" id="nominativo" placeholder="Nominativo">
								<input type="text" name="emailnewsletter" class="form-control" id="emailnewsletter" placeholder="Email">
							</div>
							<div class="form-group">
								<button name="invia" type="btn" class="btn btn-default" id="inviaN">INVIA</button>
							</div>							
							<div class="form-group">
								<div class="col-md-12" id="rispostaN"></div>
							</div>

						</form>
					</div>
				</div>
				<div class="col-md-8">
					<div class="page-header">
						<h1>contattaci</h1>
						<h6><i>Scrivici qui e ti risponderemo al più presto.</i></h6>
					</div>
					<div class="container">
						<form id="formUsato" name="formUsato" action="#" method="post">
							<div class="row">
								<div class="col-md-6">
									<input id="oggetto" name="oggetto" type="hidden" class="form-control" value="Richiesta Informazioni dal sito">
									<input id="nome" name="nome" type="text" class="form-control" placeholder="Nome">
									<input id="cognome" name="cognome" type="text" class="form-control" placeholder="Cognome">
									<input id="email" name="email" type="email" class="form-control" placeholder="Email">
									<input id="telefono" name="telefono" type="text" class="form-control" placeholder="Telefono">
									<div class="col-md-12" id="risposta"></div>
								</div>
								<div class="col-md-6">
									<textarea name="messaggio" id="messaggio" class="form-control" rows="3" placeholder="Messaggio"></textarea>
									<button name="invia" type="btn" class="btn btn-default" id="invia">INVIA</button>
								</div>
							</div>						
						</form>
					</div>
			  	</div>