
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Tutte le News</h3>
      </div>
      <div class="panel-body">
<?php foreach($allNews as $singleNews) {?>
			<div class="row">
				<div class="col-md-12">
				<?php date_default_timezone_set("Europe/Rome"); $time = strtotime(str_replace("-", "", $singleNews["data_inserimento"]));?>
				<?php echo date("j F Y", $time );?> : <br>
				<a href="<?php echo SITEURL;?>news/<?php echo $singleNews["titolo"];?>/<?php echo $singleNews["id_news"];?>">
				<?php echo $singleNews["titolo"];?></a><br><br>
				</div>
			</div>	
<?php } ?>
      </div>
    </div>