
<?php $motoKTM = new moto_ktm(); $tipologie = $motoKTM -> get_tipologie(); ?>

				<!-- Product-Navigation -->
				<div id="product-navigation">
					<ul>
						<?php foreach($tipologie as $tipologia) {?>
						<li class="menu-level1" style="z-index: <?php echo 100-$tipologia["id_tipologia"];?>">
							<div class="png-img <?php if($tipologia["id_tipologia"]==1)echo "selezionato";?>"><a href="#"><?php echo $tipologia["tipologia"];?></a></div>
							<div class="product-subnav-container <?php if($tipologia["id_tipologia"]==1)echo "aperto";?>">
								<ul class="product-subnav">
									<?php $motos = $motoKTM -> get_moto($tipologia["id_tipologia"]);?>
									<?php foreach($motos as $moto){?>
									<?php $modello = urldecode($moto["modello"]);?>
									<?php //print_r($moto);?>
									<li>
										<a title="<?php echo $modello;?>" class="headline" href="<?php echo SITEURL;?>gamma/<?php echo $modello;?>/<?php echo $moto["id_moto_ktm"];?>"><?php echo $modello;?></a>
										<a title="<?php echo $modello;?>" class="bikeimg" href="<?php echo SITEURL;?>gamma/<?php echo $modello;?>/<?php echo $moto["id_moto_ktm"];?>"><img src="<?php echo $moto['url_img'];?>" alt="<?php echo $modello;?>" title="<?php echo $modello;?>" /></a>
									</li>
									<?php }?>
								</ul>
							</div>
						</li>
					<?php } ?>	
					</ul>
					
					<a href="#" id="btnLeft"><span class="glyphicon glyphicon-chevron-left"></span></a>
					<a href="#" id="btnRight"><span class="glyphicon glyphicon-chevron-right"></span></a>
				</div>
				
				<div class="select">
					<select name="select" id="select">
						
						<option value="">Seleziona il modello</option>
						<?php foreach($tipologie as $tipologia) {?>
						<option value=""><?php echo $tipologia["tipologia"];?></option>
				
				        <?php $singleMotos = $motoKTM -> get_moto($tipologia["id_tipologia"]);?>
						<?php foreach($singleMotos as $singleMoto){?>
								<?php $modello = urldecode($singleMoto["modello"]);?>
								<option value="<?php echo SITEURL;?>gamma/<?php echo $modello;?>/<?php echo $singleMoto["id_moto_ktm"];?>"> -- <?php echo $modello;?></option>
						<?php }?>
				
						<?php } ?>
				
					</select>
				</div>	
				<!-- Product-Navigation -->

