<?php $tipologie = $motoKTM->get_tipologie(); /* print_r($tipologie); */?>

<br clear="all">    
<div class="accordion">

    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title" style="text-transform: uppercase; color: #EA5B0C;">Scegli il modello</h3>
      </div>
    </div>  
    
<?php foreach($tipologie as $tipologia) {?>
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $tipologia["tipologia"];?></h3>
      </div>
      <div class="panel-body">
        <?php $singleMotos = $motoKTM -> get_moto($tipologia["id_tipologia"]); /* print_r($singleMotos); */?>
		<?php foreach($singleMotos as $singleMoto){?>
				<div class="row">
					<div class="col-md-12">
					<?php $modello = urldecode($singleMoto["modello"]);?>
						<a title="<?php echo $modello;?>" class="" href="<?php echo SITEURL;?>gamma/<?php echo $modello;?>/<?php echo $singleMoto["id_moto_ktm"];?>">
						<img src="<?php echo $singleMoto['url_img'];?>" alt="<?php echo $modello;?>" title="<?php echo $modello;?>" style="max-width: 40%;"/></a>
					</div>
					<div class="col-md-12">	
						<a title="<?php echo $modello;?>" class="" href="<?php echo SITEURL;?>gamma/<?php echo $modello;?>/<?php echo $singleMoto["id_moto_ktm"];?>"><?php echo $modello;?></a>
					</div>
				</div>
		<?php }?>
      </div>
    </div>
<?php } ?>
</div>

<div class="select">
	<select name="select" id="select">
		
		<option value="">Seleziona il modello</option>
		<?php foreach($tipologie as $tipologia) {?>
		<option value=""><?php echo $tipologia["tipologia"];?></option>

        <?php $singleMotos = $motoKTM -> get_moto($tipologia["id_tipologia"]);?>
		<?php foreach($singleMotos as $singleMoto){?>
				<?php $modello = urldecode($singleMoto["modello"]);?>
				<option value="<?php echo SITEURL;?>gamma/<?php echo $modello;?>/<?php echo $singleMoto["id_moto_ktm"];?>"> -- <?php echo $modello;?></option>
		<?php }?>

		<?php } ?>

	</select>
</div>	 