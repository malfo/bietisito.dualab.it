
<br clear="all">   
<div class="accordion">
	
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title" style="text-transform: uppercase; color: #EA5B0C;">Tutte le News</h3>
      </div>
    </div>  
    
<?php $currentDate = ""; $cont = 0; ?>
<?php foreach($allNews as $singleNews) {?>
			<?php if($cont == 0){ ?>
		    <div class="panel">
		    	<div class="panel-heading">
					<h3 class="panel-title"><?php date_default_timezone_set("Europe/Rome"); $time = strtotime(str_replace("-", "", $singleNews["data_1"])); echo date("j F Y", $time );?></h3>
		    	</div>
				<div class="panel-body">			
					<div class="row">
						<div class="col-md-12">
							<a title="<?php echo $singleNews["titolo"];?>" href="<?php echo SITEURL;?>news/<?php echo $singleNews["titolo"];?>/<?php echo $singleNews["id_news"];?>"><?php echo $singleNews["titolo"];?></a>			
						</div>
					</div>	
			<?php } ?>

			<?php if($cont > 0 && $currentDate==$singleNews["data_1"]){ ?>
					<div class="row">
						<div class="col-md-12">
							<a title="<?php echo $singleNews["titolo"];?>" href="<?php echo SITEURL;?>news/<?php echo $singleNews["titolo"];?>/<?php echo $singleNews["id_news"];?>"><?php echo $singleNews["titolo"];?></a>			
						</div>
					</div>
			
			<?php } ?>
			
			<?php if($cont > 0 && $currentDate!=$singleNews["data_1"]){ ?>	
				</div>
			</div>		
		    <div class="panel">
		    	<div class="panel-heading">
					<h3 class="panel-title"><?php date_default_timezone_set("Europe/Rome"); $time = strtotime(str_replace("-", "", $singleNews["data_1"])); echo date("j F Y", $time );?></h3>
		    	</div>
				<div class="panel-body">			
					<div class="row">
						<div class="col-md-12">
							<a title="<?php echo $singleNews["titolo"];?>" href="<?php echo SITEURL;?>news/<?php echo $singleNews["titolo"];?>/<?php echo $singleNews["id_news"];?>"><?php echo $singleNews["titolo"];?></a>			
						</div>
					</div>
			<?php } ?>
		<?php $cont++; $currentDate = $singleNews["data_1"];?>	
<?php } ?>

				</div>
			</div>
</div>

<div class="select">
	<select name="select" id="select">
		
		<option value="">Seleziona la news</option>
		<?php foreach($allNews as $singleNews) {?>
				<option value="<?php echo SITEURL;?>news/<?php echo $singleNews["titolo"];?>/<?php echo $singleNews["id_news"];?>"><?php echo $singleNews["titolo"];?></option>
		<?php }?>

	</select>
</div>	