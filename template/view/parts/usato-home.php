<?php $motoObj = new moto_it(); $allMotos = $motoObj->get_moto(0,1); /* print_r($allMotos); */?>
	<div class="row">
	<?php for($i = 0; $i<4; $i++) {?>
		<?php $moto = $allMotos[$i];?>
        <div class="col-md-3">
	        <img src="<?php echo PATH.$moto["url_img"];?>" alt="<?php echo $moto["marca"]." ".$moto["modello"];?>">
	        <div>
		        <p>
			        <span><?php echo $moto["marca"]." ".$moto["modello"];?></span>
			        <span>€ <?php echo $moto["prezzo"];?></span>
			        <span><a href="<?php echo SITEURL;?>modello-usato/<?php echo $moto["marca"].$moto["modello"];?>/<?php echo $moto["id_moto_vendita"];?>">Vai alla scheda</a></span>
			    </p>
			</div>
	    </div>		
	<?php } ?>
    </div> 