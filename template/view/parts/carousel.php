<?php $cont = 0; $objSlideKTM = new slideshow(); $slideKTM = $objSlideKTM->get_slideshow(); print_r($slideKTM); ?>

<?php /* echo count($slideKTM); */ ?>

<?php if (count($slideKTM) > 0) {?>

<div id="myCarousel" class="carousel slide" data-ride="carousel">

	<ol class="carousel-indicators">
		<?php foreach($slideKTM as $slide) {?>
			<li class="<?if ($cont == 0) echo "active";?>" data-target="#myCarousel" data-slide-to="<?php echo $cont; $cont++;?>"></li>
		<?php } ?>
	</ol>

	<div class="carousel-inner" role="listbox">

		<?php $cont=0; ?>
		<?php foreach($slideKTM as $slide) {?>
			<div class="item <?if ($cont == 0) echo "active"; $cont++; ?>" style="background-image: url(images/<?php echo $slide["nome_file"];?>);">
				<div class="container">
					<div class="carousel-caption">
						<h1><?php echo stripslashes($slide["titolo"]);?></h1>
						<p><?php echo stripcslashes($slide["sommario"]);?></p>
						<p><a class="btn btn-lg btn-primary" href="<?php echo $slide["url_esterno"];?>" role="button" target="_blank"><?php echo $slide["url_title"];?></a></p>
					</div>
				</div>
			</div>
		<?php } ?>

	</div>

	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>

</div>
<?php } else {?>


<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li class="active" data-target="#myCarousel" data-slide-to="0" class=""></li>
	</ol>
	<div class="carousel-inner" role="listbox">
		<div class="item active" style="background-image: url('<?php echo PATH;?>template/img/SlideHomePageMX_02.jpg');">
			<div class="container">
				<div class="carousel-caption">
					<!--<h1>Enduro</h1>-->
					<!--<p>Descrizione della slide ed eventuale testo...</p>-->
					<p><a class="page-scroll btn btn-lg btn-primary" href="#sez-contatti" role="button">Chiedi info</a></p>
				</div>
			</div>
		</div>
	</div>
	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>


<?php } ?>