	<section class="slideshow">
		<div class="container no-margin" style="min-height: 130px;"></div>
	</section>	

	<?php $newsObj = new news(); $allNews = $newsObj->get_news();?>
	<section class="bianco">
		<?php $newsDetails = $newsObj->get_news_details($compURI["id"]); $news = $newsDetails[0];?>
		<div class="page-header arancione"><h1 class="">NEWS</h1></div>
		<div class="container">
			<br clear="all">	
			<div class="row">
				<div class="col-md-3"><?php include("parts/news-accordion.php");?></div>
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-12">
						<em><?php date_default_timezone_set("Europe/Rome");$time = strtotime(str_replace("-", "", $news['data_1'])); echo date("j F Y", $time );?></em><br>
						<h4><?php echo urldecode($news['titolo']);?></h4>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px; font-style: italic;">
							
							<?php echo urldecode($news['sommario']);?>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<?php echo urldecode($news['descrizione']);?>
						</div>
					</div>
					<br clear="all">
					<div class="row">
						<div class="col-md-12" style="text-align: right;">
							<a href="<?php echo SITEURL?>" class="btnBack" title=""><img src="<?php echo PATH;?>template/img/btnBack.png" alt="btnBack" width="" height="" /></a>
						</div>
					</div>					
					<?php //print_r($moto);?>
				</div>	
			</div>
		</div>    
	</section>

