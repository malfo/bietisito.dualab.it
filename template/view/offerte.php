<section class="slideshow">
    <div class="container no-margin" style="min-height: 130px;"></div>
</section>

<?php $offerteObj = new offerte(); $allOfferte = $offerteObj->get_offerte(0,0);?>
<section class="bianco">
    <?php $offertaDetails = $offerteObj->get_offerta_details($compURI["id"]); $offerta = $offertaDetails[0];?>
    <?php /* print_r($offerta);  */?>
    <div class="page-header arancione" id="usato"><h1 class="">Bi&Ti offerte</h1></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3"><?php include("parts/offerte-accordion.php");?></div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php echo $offerta["titolo"];?>: <?php echo $offerta["sommario"];?></h2> 


                        <div class="row">
                            <div class="col-md-12">
                                <img src="<?php echo PATH."images/".$offerta["nome_file"];?>" title="<?php echo $offerta["titolo"];?>" alt="<?php echo $offerta["titolo"];?>" class="principale" id="principale2" width="100%">
                            </div>
                        </div>
						
						Prezzo Pieno: € <span style="text-decoration: line-through"><?php echo $offerta["prezzo"];?></span> <br>
                        Prezzo Scontato: € <?php echo $offerta["prezzo_scontato"];?>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <br clear="all">
                        <?php echo "<strong>DESCRIZIONE</strong>";?>
                        <br clear="all">
                        <?php //echo utf8_encode( $offerta["descrizione"]);?>
                        <?php echo $offerta["descrizione"];?>
                    </div>
                </div>
                <br clear="all">


                <div class="page-header" style="background: transparent;">
                    <h1>informazioni</h1>
                    <h6><i>Scrivici qui e ti risponderemo al più presto.</i></h6>
                </div>
                <div class="container" id="FloatBox">

                    <div class="row">
                        <form id="formUsato" name="formUsato" action="#" method="post">
                            <div class="col-md-6">
                                <input id="oggetto" name="oggetto" type="hidden" class="form-control" value="<?php echo $offerta["titolo"];?>">
                                <input id="nome" name="nome" type="text" class="form-control" placeholder="Nome">
                                <input id="cognome" name="cognome" type="text" class="form-control" placeholder="Cognome">
                                <input id="email" name="email" type="email" class="form-control" placeholder="Email">
                                <input id="telefono" name="telefono" type="text" class="form-control" placeholder="Telefono">
                                <div class="col-md-12" id="risposta"></div>
                            </div>
                            <div class="col-md-6">
                                <textarea name="messaggio" id="messaggio" class="form-control" rows="3" placeholder="Messaggio"></textarea>
                                <button name="invia" type="btn" class="btn btn-default" id="invia">INVIA</button>

                            </div>
                        </form>
                    </div>


                </div>


                <br clear="all">


                <div class="row">
                    <div class="col-md-6">
                        <!-- <a href="#" id="btnForm" title=""><img src="<?php echo PATH;?>template/img/btnInfo.png" alt="btnForm" width="" height="" /></a> -->
                    </div>
                    <div class="col-md-6">
                        <a href="<?php echo SITEURL?>" class="btnBack" title="" id="btnBack"><img src="<?php echo PATH;?>template/img/btnBack.png" alt="btnBack" width="" height="" /></a>
                    </div>
                </div>
                <?php //echo $compURI["id"]; print_r($offerta); ?>
            </div>
        </div>
    </div>
</section>