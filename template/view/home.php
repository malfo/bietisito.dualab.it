	<section class="slideshow">
		<div class="container no-margin">
			<?php include("parts/carousel.php");?>
		</div>
	</section>	
	
	<section class="news">
		<div class="container">
			<?php include("parts/news-home.php");?>
		</div>
	</section>
	
	<span id="sez-usato" class="sez-ancora"></span>
	
	<section class="bianco">	
		<div class="page-header arancione" id="usato"><h1 class="">il nostro usato</h1><a href="<?php echo SITEURL?>usato/" class="" title=""> - Guarda tutto - </a></div>
		<div class="container usato">
			<?php include("parts/usato-home.php");?>	
		</div>
	</section>
	
	<span id="sez-chi-siamo" class="sez-ancora"></span>
	
	<section class="grigio">
		<div class="page-header" id="chi-siamo">
			<h1>Bi&Ti è passione che diventa professione</h1>
			<h6><i>L’esperienza si fa mestiere al servizio del cliente, appassionato, esperto o neofita.</i></h6>
		</div>
					
		<div class="container">

			<div class="row">
			    <div class="col-md-6">
                    <p>
                        Bi e Ti è soprattutto off road: nata nel 1986, la struttura è diventata in pochi anni punto di riferimento delle ruote tassellate della Capital-e. Dal 2002, Bi&Ti è concessionaria Ktm, marchio che è sintesi tra la filosofia dell'off road e l'adrenalina dell'asfalto, discipline distanti pochi metri ma diametralmente opposte.
                        Eppure, le soluzioni trovate per l'una possono essere applicate all'altra e viceversa, in una sinergia di prodotto e competenze che punta all'eccellenza, sia in gara che che nell'uso amatoriale della moto: prestazioni, affidabilità e durata del veicolo. Una moto deve essere sempre come il giorno in cui viene consegnata: efficiente, performante, e bella!
                    </p>
                    <p>
                        Purtroppo, le moto ogni tanto si rompono o vanno aggiornate. Per questo, Bi&Ti è magazzino ricambi, approdo sicuro per motociclisti e professionisti della meccanica. Senza dimenticare la vocazione iniziale: la riparazione e lo sviluppo delle moto di serie. Con 150 metri quadri di officina e personale altamente specializzato, Bi&Ti offre un servizio a tutto tondo: dal cambio gomme ai tagliandi, dall'assistenza ai prodotti stock fino all'elaborazione della moto per l'enduro estremo, il motorally, il supermotard o la domenica con gli amici, la gara più difficile.
                    </p>
                    <p>
                        La moto, però, non è solo un veicolo o un giocattolo. E' anche lifestyle. Ecco perché Bi&Ti dedica molta attenzione e cura alla selezione dei propri prodotti di abbigliamento e accessoristica. Oltre al catalogo Powerwear by Ktm, nel salone sono disponibili in pronta consegna capi di abbigliamento e accessori di prima qualità, perché ogni terreno richiede l'attrezzatura giusta.
                    </p>
                    <p>
                        Bi e Ti è tutto questo, ma Bi e Ti non è solo un nome sulla porta di un capannone. Quando entrate, non trovate Bi e Ti, ma Stefano, Federico e i nostri ragazzi. Persone, motociclisti come voi, che possono capire quel che provate dentro un'officina a quando vi sedete sulla moto dei vostri sogni. Anche noi abbiamo moto e sogni. E passione, come voi: per questo, abbiamo fondato un'associazione sportiva e abbiamo messo una macchina del caffè in concessionaria. Non è solo per noi. E per noi e voi, perché siamo tutti motociclisti per passione, prima che per professione.
                    </p>
				</div>
			    <div class="col-md-6"><img src="<?php echo PATH;?>template/img/NewsHome.jpg" alt="moto1" width="100%"></div>
			</div>	
		</div>
	</section>
	
	<span id="sez-gamma" class="sez-ancora"></span>

	<section class="bianco">
		<div class="page-header bianco" id="gamma">
			<h1>tutta la gamma ktm</h1>
			<h6><i>Clicca sul nome del modello e scopri la gamma.</i></h6>
		</div>
	
		<div class="container">
			<div class="row">
				<?php include("parts/gamma-home.php");?>
			</div>	
		</div>   
	</section>   
	
	<span id="sez-store" class="sez-ancora"></span>
	
	<section class="arancione">    
		<div class="page-header arancione">
			<h1>powerparts</h1>
			<h6><i>Centinaia di offerte a portata di click.</i></h6>
		</div>	 
		   
		<div class="container">
			<div class="row offerte">
				<?php include("parts/offerte-home.php");?>
			</div>	
		</div>    
	</section>
	
	<span id="sez-contatti" class="sez-ancora"></span>  
	    
	<section class="grigioChiaro">    
		<div class="container" id="contatti">
			<div class="row">
				<?php include("parts/contatti.php");?>
		  	</div>
		</div>      
	</section>
	
	<span id="sez-dove-siamo" class="sez-ancora"></span>
	
	<section class="bianco">
		<div class="container" id="dove-siamo">
			<div class="page-header bianco">
				<h1>dove siamo</h1>
				<h6><i>E' facile raggiungerci segui le indicazioni.</i></h6>
			</div>
			<div class="row">
				<div class="col-md-4" style="text-align: center;"> 
					<address>
					<span style="text-transform: uppercase; font-weight:400;">BI & TI srl</span><br>
					<span style="color: black">Via Lippo Vanni 00133 ROMA</span><br><br>
					
					<span style="text-transform: uppercase; font-weight:400;">Orari apertura</span><br>
					<span style="color: black">Lun. 15:00 - 19:00<br>Mar. - Ven. 09:00 - 13:00 / 15:00 - 19:00<br>Sab. 09:00 - 13:00</span><br><br>
					
					<span style="text-transform: uppercase; font-weight:400;">Email</span><br>
					<span style="color: black">info@bieti.it</span><br><br>

					<span style="text-transform: uppercase; font-weight:400;">Officina</span><br>
					<span style="color: black">06-45476153</span><br><br>

					<span style="text-transform: uppercase; font-weight:400;">Negozio</span><br>
					<span style="color: black">06-2022041</span><br>
					
					
 
					</address>
				</div>
				<div class="col-md-8"> 
					<iframe scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.it/maps?hl=it&amp;q=via+lippo+vanni+roma&amp;ie=UTF8&amp;hq=&amp;hnear=Via+Lippo+Vanni,+Roma,+Lazio&amp;ll=41.85979,12.599601&amp;spn=0.014303,0.033023&amp;t=m&amp;z=14&amp;output=embed" frameborder="0" height="320" width="100%"></iframe>	
				</div>
			</div>
		</div>    
	</section>
