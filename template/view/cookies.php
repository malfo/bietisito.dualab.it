	<section class="slideshow">
		<div class="container no-margin" style="min-height: 130px;"></div>
	</section>	
	<section class="bianco">
		<div class="page-header arancione" id="usato"><h1 class="">Bi&Ti cookies</h1></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12"><h2>Cosa sono i cookies</h2></div>
					</div>
					<div class="row">
						<div class="col-md-12">

<p>
I cookie sono dei file di testo che i siti inviano ai terminali degli utenti, dove vengono memorizzati per poi essere reinviati agli stessi siti in occasione delle visite successive.</p>
<p><strong>Gestione dei Cookie</strong><br>
Ai visitatori del sito potranno essere inviati, da parte dei nostri sistemi informatici, i cosiddetti cookies.<br>
Ogni utente potrà gestire, modificare o cancellare i cookie usati per la navigazione, modificando le impostazioni del proprio browser.</p>
<p><strong>Cookie Tecnici</strong><br>
I cookie tecnici sono quelli che permettono l’autenticazione sul nostro sito e aiutano a monitorare le sessioni e memorizzare le specifiche informazioni trasmesse degli utenti nella visualizzazione di una pagina web. Fondamentali per una navigazione veloce, i cookie tecnici, facilitano le procedure per l’autenticazione online e per effettuare eventuali acquisti direttamente dal nostro sito.<br>
Vengono utilizzati diversi cookie tecnici come quelli di sessione, utilizzati esclusivamente durante la navigazione, e i cookie persistenti (o di memorizzazione), che vengono salvati nel browser fino alla loro scadenza previa cancellazione da parte dell’utente.<br>
Il nostro sito utilizza diversi cookie tecnici come quelli di sessione, quelli persistenti usati per memorizzare le scelte fatte dai singoli visitatori e i cookie analytics usati per monitorare gli utenti durante la loro navigazione nel nostro sito web.</p>
<p><strong>Cookie di terze parti</strong><br>
I cookie di terze parti sono quei cookie ospitati sul nostro sito ma provenienti da altri siti web come ad esempio i cookie analitici e di profilazione come Google.<br>
Questi cookie vengono impostati direttamente dai proprietari, pertanto si elencano di seguito le rispettive policy:<br>
Google https://www.google.com/intl/it/policies/privacy/</p>

                        </div>
							
					</div>

					<br clear="all">
					<div class="row">
						<div class="col-md-12" style="text-align: right;">
							<a href="<?php echo SITEURL?>" class="btnBack" title=""><img src="<?php echo PATH;?>template/img/btnBack.png" alt="btnBack" width="" height="" /></a>
						</div>
					</div>						
					<?php //echo $compURI["id"]; print_r($moto); ?>
				</div>	
			</div>
		</div>    
	</section>