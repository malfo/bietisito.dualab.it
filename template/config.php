<?php
	
	/* Creo il path per l'inclusione del template */
	$arrayRequestURI = explode("/", $_SERVER["REQUEST_URI"]);
	$relativePath = "";
	if(count($arrayRequestURI)>2) {
		for($i=2; $i<count($arrayRequestURI);$i++){
			$relativePath .= "../";
		}
	}
	define("PATH", $relativePath);
	
	/* Creo il site url per il template */
	define("SITEURL",  "http://".$_SERVER['HTTP_HOST']."/");
	

	/* Recupero le parti che compongono l'url */
	$compURI = array();
	$compURI["sezione"] = $arrayRequestURI[1];
/*
	$compURI["nomeProdotto"] = $arrayRequestURI[2];
	$compURI["idProdotto"] = $arrayRequestURI[3];
*/
	$compURI["titolo"] = $arrayRequestURI[2];
	$compURI["id"] = $arrayRequestURI[3];
	$compURI["lingua"] = "it";

	
	/* Imposto le classi CSS delle pagine e controllo la presenza della pagina sezione */	
	$bodyClass = "onepage";
	$sezione = "home";



	if ($compURI["sezione"]!="") { 
		$sezione = $compURI["sezione"];
		$bodyClass = $compURI["sezione"];

		if ($bodyClass == "offerte") {
			$bodyClass = "modello-usato";
		}

		if (!file_exists("template/view/".$compURI["sezione"].".php")) {
			header("location: ".SITEURL."template/404.php");
			exit();
		}
	} 

	/* Includo le funzioni generiche del template */
	include("generiche.php");
		
?>