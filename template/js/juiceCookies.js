$(document).ready(function($) {
	
	var jC = getCookie("jC");
	if(jC==""){
		var testo = "Questo sito utilizza i cookies, interagendo con esso si acconsente al loro utilizzo.";
		$("body").append('<div class="cookiesDiv">'+testo+' <a href="#" id="ok">Accetta</a> <a href="#" id="leggi">Leggi</a></div><br clear="both">');
		$("#ok").click(function(){
			setCookie("jC", "ok", 31);
			$(".cookiesDiv").slideUp();
		});
		$("#leggi").click(function(){
			window.location = linkCookies;
			return false;
		});
		
	}

});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}