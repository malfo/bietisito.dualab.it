var productNavigation=0;
var sectionNavigation=0;
var tempo;

$(document).ready(function() {


	
	if($("body").hasClass("onepage")){
		
		$(window).bind('resize', function(e) {
		    window.resizeEvt;
		    $(window).resize(function() {
		        clearTimeout(window.resizeEvt);
		        window.resizeEvt = setTimeout(function() {
		            location.reload();
		        }, 250);
		    });
		});		

		var maxH = 0;
		$(".usato .row .col-md-3 div").each(function(){
			if($(this).height() > maxH) maxH = $(this).height();
			/*console.log(maxH);*/
		});
		
		$(".usato .row .col-md-3, .usato .row .col-md-3 div").css("height", maxH);	
		
		var maxH = 0;
		$(".offerte .row .col-md-3 div").each(function(){
			if($(this).height() > maxH) maxH = $(this).height();
			/*console.log(maxH);*/
		});

		$(".offerte .row .col-md-3, .offerte .row .col-md-3 div").css("height", maxH);

		iniNews();
		$("#product-navigation .menu-level1 .png-img a, #btnLeft, #btnRight").click(function(){ return false; })	
		var MW = 0;
		$(".menu-level1 .png-img").each(function(){
			$(this).attr("style", "margin-left: "+MW+"px");
			MW += $(this).width();
		})
		$(".menu-level1 > .png-img > a").click(function(){
			$(".menu-level1 .png-img").removeClass("selezionato");
			$(this).closest(".menu-level1").find(".png-img").addClass("selezionato");
			$("#product-navigation ul .menu-level1 .product-subnav-container").removeClass("aperto").hide();
			$(this).closest(".menu-level1").find(".product-subnav-container").addClass("aperto").show();
			$(this).closest(".menu-level1").find(".product-subnav-container ul").css({ marginLeft: 0 });
			iniFabSliderWidth();
		});
		
		iniFabSlider();
		
		$("#select").on('change', function () {
	        var url = $(this).val();
	        /*console.log(url);*/
	        if (url!="") { window.location = url; }
	        return false;
    	});
	}
	
	if($("body").hasClass("gamma")){
		$(".panel .panel-body").slideUp();
		$(".panel .panel-title").click(function(){
			if (!$(this).closest(".panel").find(".panel-body").is(':visible')){	
				$(".panel .panel-body").slideUp();
				$(".panel").removeClass("backGray");
			}
			$(this).closest(".panel").addClass("backGray").find(".panel-body").slideDown("slow", function(){
				if(($(".navbar").offset().top + $(".navbar").height())>$(this).offset().top){
					$('html, body').stop().animate({ scrollTop: ($(this).offset().top - $(".navbar").height() - 60) }, 1500, 'easeInOutExpo');
				}
			});
		});
		$(".feature-content img").each(function(){
			$(this).hide();
		});	
		if(om){
			$(".gamma .col-md-3 .panel .panel-body a[title='"+om+"']").closest(".panel").addClass("backGray").find(".panel-body").slideDown();
		}
		
		$("#select").on('change', function () {
	        var url = $(this).val();
	        if (url!="") { window.location = url; }
	        return false;
    	});	
	}
	
	if($("body").hasClass("news")){
		$(".panel .panel-body").slideUp();
		$(".panel .panel-title").click(function(){
			if (!$(this).closest(".panel").find(".panel-body").is(':visible')){	
				$(".panel .panel-body").slideUp();
				$(".panel").removeClass("backGray");
			}
			$(this).closest(".panel").addClass("backGray").find(".panel-body").slideDown("slow", function(){
				if(($(".navbar").offset().top + $(".navbar").height())>$(this).offset().top){
					$('html, body').stop().animate({ scrollTop: ($(this).offset().top - $(".navbar").height() - 60) }, 1500, 'easeInOutExpo');
				}
			});
		});	
		if(om){
			$(".news .col-md-3 .panel .panel-body a[title='"+om+"']").closest(".panel").addClass("backGray").find(".panel-body").slideDown();
		}	
		
		$("#select").on('change', function () {
	        var url = $(this).val();
	        /*console.log(url);*/
	        if (url!="") { window.location = url; }
	        return false;
    	});	
    	
	}

	if($("body").hasClass("modello-usato")){
		
		$(window).bind('resize', function(e) {
		    window.resizeEvt;
		    $(window).resize(function() {
		        clearTimeout(window.resizeEvt);
		        window.resizeEvt = setTimeout(function() {
		            location.reload();
		        }, 250);
		    });
		});	
		
		
		$(".panel .panel-body").slideUp();
		$(".panel .panel-title").click(function(){
			if (!$(this).closest(".panel").find(".panel-body").is(':visible')){	
				$(".panel .panel-body").slideUp();
				$(".panel").removeClass("backGray");
			}
			$(this).closest(".panel").addClass("backGray").find(".panel-body").slideDown("slow", function(){
				if(($(".navbar").offset().top + $(".navbar").height())>$(this).offset().top){
					$('html, body').stop().animate({ scrollTop: ($(this).offset().top - $(".navbar").height() - 60) }, 1500, 'easeInOutExpo');
				}
			});
		});
		if(om){
			$(".modello-usato .col-md-3 .panel .panel-body ."+om+"").css("color", "black");
			$(".modello-usato .col-md-3 .panel .panel-body ."+om+"").closest(".panel").addClass("backGray").find(".panel-body").slideDown();
		}
		
		$("#select").on('change', function () {
	        var url = $(this).val();
	        /*console.log(url);*/
	        if (url!="") { window.location = url; }
	        return false;
    	});	
    	
    	$(".modello-usato .col-md-2 .principale").click(function(){
	    	if($(window).width() >= 992) {
		    	var valore = $(this).attr("src");
		    	$("#principale").fadeOut(500, function( ){ $(this).attr("src", valore).fadeIn();});
	    	}
    	});
    	
/*
    	$("#btnForm").click(function(event){
	    	event.preventDefault();
			$('#FloatBox').css('height', $(window).height());
			$('#FloatBox').css('padding-top', "15%");
			$('#FloatBox').fadeOut().fadeIn("slow");	
			return false;    	
    	});
    	
    	$('#FloatBox #btnClose').click(function(){ 
	    	$("#FloatBox").fadeOut("slow", function(){ $(this).hide();});
    	});
*/
    		
	}

	if($("body").hasClass("usato")){
		var maxH = 0;
		/*console.log(maxH);*/
		$(".row .col-md-9 .row .col-md-4 div").each(function(){
			if($(this).height() > maxH) maxH = $(this).height();
			/*console.log(maxH);*/
		});
		
		$(".row .col-md-9 .row .col-md-4, .row .col-md-9 .row .col-md-4 div").css("height", maxH);	
		
		$(".panel .panel-body").slideUp();
		$(".panel .panel-title").click(function(){
			if (!$(this).closest(".panel").find(".panel-body").is(':visible')){	
				$(".panel .panel-body").slideUp();
				$(".panel").removeClass("backGray");
			}
			$(this).closest(".panel").addClass("backGray").find(".panel-body").slideDown("slow", function(){
				if(($(".navbar").offset().top + $(".navbar").height())>$(this).offset().top){
					$('html, body').stop().animate({ scrollTop: ($(this).offset().top - $(".navbar").height() - 60) }, 1500, 'easeInOutExpo');
				}
			});
		});
		
		$(".usato .col-md-3 .panel:last-child").addClass("backGray").find(".panel-body").slideDown();
		
		$("#select").on('change', function () {
	        var url = $(this).val();
	        /*console.log(url);*/
	        if (url!="") { window.location = url; }
	        return false;
    	});	
	}
			
});


function iniFabSlider(){
	iniFabSliderWidth();
	$("#btnRight").click(function(){
		if ($(".menu-level1 .aperto ul").is(":animated")) { return false; }
		var ML = parseInt($(".menu-level1 .aperto ul").css("margin-left"));
		if((sectionNavigation+ML)>productNavigation)
			$(".menu-level1 .aperto ul").animate({ marginLeft: '-=150px' }, { duration: 500, start: function(){}, complete: function(){} });
	});
	$("#btnLeft").click(function(){
		if ($(".menu-level1 .aperto ul").is(":animated")) { return false; }
		var ML = parseInt($(".menu-level1 .aperto ul").css("margin-left"));
		if(ML<0)
			$(".menu-level1 .aperto ul").animate({ marginLeft: '+=150px' }, { duration: 500, start: function(){}, complete: function(){} });
	});
	$('#btnRight').dblclick(function(e){ e.stopPropagation(); e.preventDefault(); });
	$('#btnLeft').dblclick(function(e){ e.stopPropagation(); e.preventDefault(); });
}

function iniFabSliderWidth(){
	productNavigation = $("#product-navigation").width();
	sectionNavigation = $(".menu-level1 .product-subnav-container .product-subnav li :visible").length/3*150 ;
}

function iniNews(){
	$(".news .container .dl-horizontal").hide();
	mostraNews();
	tempo = setInterval(function(){mostraNews();}, 6000);
/*
	$(".news .container").hover( 
		function() { clearInterval(tempo); tempo = 0; }, 
		function() { tempo = setInterval(function(){mostraNews();}, 6000); }
	);
*/
}

function mostraNews(){
	$(".news .container .dl-horizontal:first-child").fadeIn("slow").delay(4500).appendTo( ".news .container" ).fadeOut("slow");
}