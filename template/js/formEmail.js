$(document).ready(function(){ 
	$("#risposta").html("&nbsp;");	
	$("#invia").click(function(){
		$("#risposta").addClass("loading");
		$("#risposta").html("&nbsp;");
		var url = "http://www.bieti.it/app/engine/controller/email.php?azione=send";
		var data = $("#formUsato").serialize();
		var errore = 0;
		if(errore == 0) {
			
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function(data){ risposta(data); },
				dataType: "json"
			});
		}
		return false;
	});
	$("#reset").click(function(){ pulisciForm(); });
	iniFocus();
});


/* ESEGUE I CONTROLLI E L'INVIO DEI DATI DEL FORM */

function risposta(data){
	//console.log("risposta.");
	//console.log(data);
	$("#risposta").removeClass("loading");
	if (data["errore"]) {
		var arrayErrori = data["risposta"];
		arrayErrori.forEach(function(entry){
			if(entry) {
				var arrayCampi = entry["error"].split("|");
				$("#"+arrayCampi[0]).addClass("errore");
				$("#"+arrayCampi[0]).val("");
				$("#"+arrayCampi[0]).attr("placeholder",arrayCampi[1]);
			}
		});
	} else {
		pulisciForm();
		$("#risposta").html(data["risposta"]);
	}
}

function pulisciForm(){
	$("#formUsato input, #formUsato textarea").each(function(){
		$(this).val("");
	});
}

/* INIZIALIZZA TUTTI GLI OGGETTI CHE INTERAGISCONO ATTRAVERSO IL FOCUS */
function iniFocus(){
	//console.log("iniFocus");
	$("#formUsato input, #formUsato textarea").focusin(function(){
		$(this).removeClass("errore");
	});
}
