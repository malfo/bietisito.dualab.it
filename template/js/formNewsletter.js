$(document).ready(function(){ 
	$("#rispostaN").html("&nbsp;");	
	$("#inviaN").click(function(){
		$("#rispostaN").addClass("loading");
		$("#rispostaN").html("&nbsp;");
		var url = "http://www.bieti.it/app/engine/controller/newsletter.php?azione=setnewsletter";
		var data = $("#formN").serialize();
		//console.log(data);
		var errore = 0;
		if(errore == 0) {
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function(data) { rispostaN(data); },
 				error: function(error) { erroreN(error); },
				dataType: "json"
			});
		}
		return false;
	});
	iniFocusN();
});


/* ESEGUE I CONTROLLI E L'INVIO DEI DATI DEL FORM */

function rispostaN(data){
	//console.log("Data: ");
	//console.log(data);
	$("#rispostaN").removeClass("loading");
	if (data["errore"]) {
		var arrayErrori = data["risposta"];
		//console.log(typeof arrayErrori);
		arrayErrori.forEach(function(entry){
			//console.log("Entry: ");
			//console.log(entry);
			if(entry) {
				var arrayCampi = entry["error"].split("|");
				$("#"+arrayCampi[0]).addClass("errore");
				$("#"+arrayCampi[0]).val("");
				$("#"+arrayCampi[0]).attr("placeholder",arrayCampi[1]);
			}
		});
	} else {
		pulisciForm();
		
		$("#rispostaN").html(data["risposta"]);
	}
}

function erroreN(error){
	//console.log("Errore: " );
	//console.log(error);
	$("#rispostaN").removeClass("loading");
}

function pulisciForm(){
	$("#formN input, #formN textarea").each(function(){
		$(this).val("");
		$(this).attr("placeholder","Email");
		$(this).removeClass("errore");
	});
}

/* INIZIALIZZA TUTTI GLI OGGETTI CHE INTERAGISCONO ATTRAVERSO IL FOCUS */
function iniFocusN(){
	$("#formN input, #formN textarea").focusin(function(){
		$(this).removeClass("errore");
	});
}
