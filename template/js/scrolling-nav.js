//jQuery to collapse the navbar on scroll
//console.log("Include scroll");

$(window).scroll(function() {
/*
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
*/
$(".navbar-fixed-top").addClass("top-nav-collapse");

});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll, a.navbar-brand').bind('click', function(event) {
        var $anchor = $(this);
        //console.log($anchor);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top//  -120
        }, 1500, 'easeInOutExpo');
        if($(".navbar-header button").is(":visible")){$(".navbar-toggle").trigger( "click" );}
        
        event.preventDefault();
    });
});
