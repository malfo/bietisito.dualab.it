<?php include("app/engine/common.class.php");?>
<?php include("template/config.php");?>

<!DOCTYPE html>

<html lang="it">

	<head>
	
		<?php include("template/head.php");?>
		
	</head>

	<body class="<? echo $bodyClass; ?>" data-spy="scroll" data-target=".navbar-fixed-top">
		
		<?php include("template/header.php");?>
		
		<?php include("template/view/".$sezione.".php");?>
		
		<?php include("template/footer.php");?>
		
	</body>

</html>